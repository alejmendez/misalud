<?php
$menu['facturacion'] = [
	[
		'nombre' 	=> 'Facturacion',
		'direccion' => '#Facturacion',
		'icono' 	=> 'fa fa-clipboard',
		'menu' 		=> [
			[
				'nombre' 	=> 'Inicio',
				'direccion' => 'facturacion',
				'icono' 	=> 'fa fa-building'
			],
			[
				'nombre' 	=> 'Afacturar',
				'direccion' => 'afacturar',
				'icono' 	=> 'fa fa-cogs'
			],
			[
				'nombre' 	=> 'Libro',
				'direccion' => 'libro',
				'icono' 	=> 'fa fa-book'
			],
			[
				'nombre' 	=> 'Cierre',
				'direccion' => 'cierre',
				'icono' 	=> 'fa fa-times-circle'
			]
		]
	]
];