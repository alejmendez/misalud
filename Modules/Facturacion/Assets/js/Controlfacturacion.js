var aplicacion, $form, tabla;
$(function() {
    veficar(verificar);
    aplicacion = new app("formulario", {
        'antes': function(accion) {

        },

        'limpiar': function() {}

    });


    $("#guardar2").on('click', function() {


        var options2 = {
            url: $url + 'guardar',
            type: 'POST',
            data: $("#formulario").serialize(),
            success: function(r) {
                aviso(r);
                resul_carga(r.id);

            },
            complete: function(x, e, o) {
                ajaxComplete(x, e, o);

            }
        };

        $('#formulario').ajaxSubmit(options2);
    });

});

function veficar(veficar) {

    if (veficar == 1) {
        $('#guardar2').prop('disabled', true);
        $('#alert').show(1000);

    } else {

        $('#guardar2').prop('disabled', false);
    }
}

function resul_carga(id) {
    $.ajax({
        url: $url + 'resulcarga',
        data: 'id=' + id,
        type: 'POST',
        success: function(r) {
            $('#total').text(number_format(r.total, 2));
            $('#registros').text(r.movimientos);
            $('#guardar2').prop('disabled', true);
            $('#resumen').show(1000);
        }
    });
}