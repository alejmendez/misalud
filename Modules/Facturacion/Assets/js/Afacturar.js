$(function() {

    inicio(1);
    inicio(2);
    inicio(3);

    $('#txt-cbo').prop('disabled', false);
    $('#txt-pto').prop('disabled', false);
    $('#txt-tigre').prop('disabled', false);

    $('#agrupar-cbo').on('click', function() {
        operacion(1, '#agrupar-cbo', 'agrupar');
    });

    $('#agrupar-pto').on('click', function() {
        operacion(2, '#agrupar-pto', 'agrupar');
    });

    $('#agrupar-tigre').on('click', function() {
        operacion(3, '#agrupar-tigre', 'agrupar');
    });

    $('#enun-cbo').on('click', function() {
        operacion(1, '#enun-cbo', 'enum');
    });

    $('#enun-pto').on('click', function() {
        operacion(2, '#enun-pto', 'enum');
    });

    $('#enun-tigre').on('click', function() {
        operacion(3, '#enun-tigre', 'enum');
    });
});

function redirect(r) {

    if (r == 0) {
        window.location.href = mover;
    }
}

function inicio(id) {
    $.ajax({
        url: $url + 'buscar',
        data: {
            'id': id
        },
        success: function(r) {
            //console.log(r.afacturar['correlativo']);
            switch (id) {
                case 1:
                    $('#correlativo-cbo').text(r.correlativo);
                    $('#totalmovil-cbo').text(r.movimientos['movimientos']);
                    $('#totalmonto-cbo').text(r.movimientos['monto']);
                    $('#totalgrupo-cbo').text(r.afacturar['afacturar']);
                    $('#totalagrupmonto-cbo').text(r.afacturar['monto']);
                    if (r.afacturar['correlativo'] == 0) {
                        //$('#txt-cbo').prop('disabled', true);
                    } else {
                        //$('#txt-cbo').prop('disabled', false);
                    }
                    break;
                case 2:
                    $('#correlativo-pto').text(r.correlativo);
                    $('#totalmovil-pto').text(r.movimientos['movimientos']);
                    $('#totalmonto-pto').text(r.movimientos['monto']);
                    $('#totalgrupo-pto').text(r.afacturar['afacturar']);
                    $('#totalagrupmonto-pto').text(r.afacturar['monto']);
                    if (r.afacturar['correlativo'] == 0) {
                        //$('#txt-pto').prop('disabled', true);
                    } else {
                        //$('#txt-pto').prop('disabled', false);
                    }
                    break;
                case 3:
                    $('#correlativo-tigre').text(r.correlativo);
                    $('#totalmovil-tigre').text(r.movimientos['movimientos']);
                    $('#totalmonto-tigre').text(r.movimientos['monto']);
                    $('#totalgrupo-tigre').text(r.afacturar['afacturar']);
                    $('#totalagrupmonto-tigre').text(r.afacturar['monto']);
                    if (r.afacturar['correlativo'] == 0) {
                        //$('#txt-tigre').prop('disabled', true);
                    } else {
                        //$('#txt-tigre').prop('disabled', false);
                    }
                    break;
            }
        }
    });

}

function operacion(sucursal, botton, opera) {

    var l = Ladda.create($(botton).get(0));
    l.start();

    if (opera == 'agrupar') {
        $rutaope = $url + 'agrupar';
    } else {
        $rutaope = $url + 'correlativo';
    }

    $.ajax({
        url: $rutaope,
        data: {
            'id': sucursal
        },
        complete: function(x, e, o) {
            ajaxComplete(x, e, o);
            inicio(1);
            inicio(2);
            inicio(3);
            l.stop();
        }
    });
}