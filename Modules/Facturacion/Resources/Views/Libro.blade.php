@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
	
	@include('base::partials.ubicacion', ['ubicacion' => ['Libro']])

@endsection

@section('content')
	<div class="row">
		<div class="col-lg-6" id="pendientes">
			<div class="panel panel-success">
				<div class="panel-heading">
					<h3 class="panel-title">Carga Facturacion Manual</h3>
				</div>
				<div class="panel-body">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<h4>Cargar Facturacion Manual <small>Excel (.xls, .xlsx, .ods)</small></h4>
						<hr>
						<center>
							{!! Form::open(['id' => 'carga', 'name' => 'carga', 'method' => 'POST']) !!}
								<input id="upload" name="subir" type="file"/>

								<button id="subir" type="button" class="btn btn-primary mt-ladda-btn ladda-button" data-style="expand-right">
									<span class="ladda-label">
										<i class="icon-arrow-right"></i> Carga archivo Excel
									</span>
								</button>
							{!! Form::close() !!}
						</center>
					</div>
				</div>
			</div>
		</div>

		<div class="col-lg-6" id="pendientes">
			<div class="panel panel-success">
				<div class="panel-heading">
					<h3 class="panel-title">Generar Libro</h3>
				</div>
				<div class="panel-body">
					
					<h4>Generar libro de ventas</h4>
					<hr>
					<center>	
						<a href="{{ url('libro/export') }}" class="btn btn-primary">Generar</a>
					</center>
				</div>
			</div>
		</div>
	</div>
@endsection