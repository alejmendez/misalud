@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')

	@include('base::partials.ubicacion', ['ubicacion' => ['Afacturar']])
	
@endsection

@section('content')
	<div class="row">
		<div class="col-lg-4" id="cbo">
			<div class="panel panel-success">
				<div class="panel-heading">
					<h3 class="panel-title">Ciudad Bolivar</h3>
				</div>
				<div class="panel-body">
					<h4>Correlativo: <span id="correlativo-cbo"></span></h4>
					<hr>					
					<h4>Total Movimientos: <span id="totalmovil-cbo"></span></h4>
					<h4>Total en bolivares: <span id="totalmonto-cbo"></span></h4>
					<hr>
					<h4>Total agrupados: <span id="totalgrupo-cbo"></span></h4>
					<h4>Total en bolivares agrupados: <span id="totalagrupmonto-cbo"></span></h4>
					<hr>
					<center> 	
						<button id="agrupar-cbo" type="button" class="btn btn-success mt-ladda-btn ladda-button" data-style="expand-right">
							<span class="ladda-label">
								<i class="icon-arrow-right">Agrupar</i> 
							</span>
						</button>
						<button id="enun-cbo" type="button" class="btn btn-success mt-ladda-btn ladda-button" data-style="expand-right">
							<span class="ladda-label">
								<i class="icon-arrow-right">Enumerar Facturas</i> 
							</span>
						</button>
						<hr>
						{!! Form::open(['url'=> 'afacturar/facturas', 'method' => 'POST', 'target' => '_blank']) !!}
							<input type="hidden" name="id" value="1">
							<button id="txt-cbo"  type="submit" class="btn btn-success"  >Generar Txt</button>	
					
						{!! Form::close() !!}
					</center>
				</div>
			</div>
		</div>
		<div class="col-lg-4" id="pto">
			<div class="panel panel-success">
				<div class="panel-heading">
					<h3 class="panel-title">Puerto Ordaz</h3>
				</div>
				<div class="panel-body">
					<h4>Correlativo: <span id="correlativo-pto"></span></h4>
					<hr>					
					<h4>Total Movimientos: <span id="totalmovil-pto"></span></h4>
					<h4>Total en bolivares: <span id="totalmonto-pto"></span></h4>
					<hr>
					<h4>Total agrupados: <span id="totalgrupo-pto"></span></h4>
					<h4>Total en bolivares agrupados: <span id="totalagrupmonto-pto"></span></h4>
					<hr>
					<center> 	
						<button id="agrupar-pto" type="button" class="btn btn-success mt-ladda-btn ladda-button" data-style="expand-right">
							<span class="ladda-label">
								<i class="icon-arrow-right">Agrupar</i> 
							</span>
						</button>
						<button id="enun-pto" type="button" class="btn btn-success mt-ladda-btn ladda-button" data-style="expand-right">
							<span class="ladda-label">
								<i class="icon-arrow-right">Enumerar Facturas</i> 
							</span>
						</button>
						<hr>
						{!! Form::open(['url'=> 'afacturar/facturas', 'method' => 'POST', 'target' => '_blank']) !!}
							<input type="hidden" name="id" value="2">
							<button id="txt-pto"  type="submit" class="btn btn-success"  >Generar Txt</button>	
					
						{!! Form::close() !!}
					</center>
				</div>
			</div>
		</div>
		<div class="col-lg-4" id="tigre">
			<div class="panel panel-success">
				<div class="panel-heading">
					<h3 class="panel-title">El Tigre</h3>
				</div>
				<div class="panel-body">
					<h4>Correlativo: <span id="correlativo-tigre"></span></h4>
					<hr>					
					<h4>Total Movimientos: <span id="totalmovil-tigre"></span></h4>
					<h4>Total en bolivares: <span id="totalmonto-tigre"></span></h4>
					<hr>
					<h4>Total agrupados: <span id="totalgrupo-tigre"></span></h4>
					<h4>Total en bolivares agrupados: <span id="totalagrupmonto-tigre"></span></h4>
					<hr>
					<center> 	
						<button id="agrupar-tigre" type="button" class="btn btn-success mt-ladda-btn ladda-button" data-style="expand-right">
							<span class="ladda-label">
								<i class="icon-arrow-right">Agrupar</i> 
							</span>
						</button>
						<button id="enun-tigre" type="button" class="btn btn-success mt-ladda-btn ladda-button" data-style="expand-right">
							<span class="ladda-label">
								<i class="icon-arrow-right">Enumerar Facturas</i> 
							</span>
						</button>
						<hr>
						{!! Form::open(['url'=> 'afacturar/facturas', 'method' => 'POST', 'target' => '_blank']) !!}
						
							<input type="hidden" name="id" value="3">
							<button id="txt-tigre"  type="submit" class="btn btn-success">Generar Txt</button>	

						{!! Form::close() !!}
					</center>
				</div>
			</div>
		</div>
	</div>
@endsection