<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Libro extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('libro', function (Blueprint $table) {
			$table->increments('id');	
			$table->date('fecha');
			$table->integer('desde')->unsigned();
			$table->integer('hasta')->unsigned();
			$table->decimal('total', 10, 2);
			$table->integer('sucursal_id')->unsigned()->nullable();
			$table->integer('controlfacturacion_id')->unsigned();
            
			$table->foreign('controlfacturacion_id')
				->references('id')->on('controlfacturacion')
				->onDelete('cascade')->onUpdate('cascade');

			$table->foreign('sucursal_id')
				->references('id')->on('sucursal')
				->onDelete('cascade')->onUpdate('cascade');	
			$table->timestamps();
			//$table->softDeletes();
		});	

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('libro');
    }
}
