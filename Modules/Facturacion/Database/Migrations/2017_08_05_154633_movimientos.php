<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Movimientos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movimientos', function (Blueprint $table) {
			$table->increments('id');
			$table->string('ci');
			$table->date('fecha');
			$table->decimal('monto', 10, 2);
			$table->integer('sucursal_id')->unsigned();

			$table->integer('controlfacturacion_id')->unsigned();
    
            $table->foreign('sucursal_id')
				->references('id')->on('sucursal')
				->onDelete('cascade')->onUpdate('cascade');	
			
			$table->foreign('controlfacturacion_id')
				->references('id')->on('controlfacturacion')
				->onDelete('cascade')->onUpdate('cascade');
			$table->timestamps();
			//$table->softDeletes();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movimientos');
    }
}
