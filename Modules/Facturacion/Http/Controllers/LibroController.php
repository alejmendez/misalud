<?php

namespace Modules\Facturacion\Http\Controllers;
use Excel;
use Carbon\Carbon;
//Controlador Padre
use Modules\Facturacion\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;

//Request
use Modules\Facturacion\Http\Requests\LibroRequest;

//Modelos
use Modules\Facturacion\Model\Libro;
use Modules\Facturacion\Model\Controlfacturacion;

class LibroController extends Controller {
	protected $titulo = 'Libro';

	public $librerias = [
		'ladda'
	];

	public $js  = ['Libro'];
	public $css = ['Libro'];

	protected $id_control;

	public function __construct()
	{
		parent::__construct();

		$rs = controlfacturacion::where('estatus', '=', 1)->first();
		$this->id_control = $rs['id'];
	}

	public function index() {
		return $this->view('facturacion::Libro');
	}

	public function carga(Request $request)
	{
		set_time_limit(0);
		DB::beginTransaction();
		try {
					
			$mimes = [
			'text/csv',
			'application/vnd.ms-excel',
			'application/vnd.oasis.opendocument.spreadsheet',
			'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
			];
 
			$ruta = public_path('archivos/');
			$archivo = $request->file('subir');

			$mime = $archivo->getClientMimeType();

			/*if (!in_array($mime, $mimes)) {
				return "Error de Validación, la extension el archivo cargado debe ser: xls,xlsx,csv";
			}*/
			
			do {
				$nombre_archivo = str_random(20) . '.' . $archivo->getClientOriginalExtension();
			} while (is_file($ruta . $nombre_archivo));
				
			$archivo->move($ruta, $nombre_archivo);

			chmod($ruta . $nombre_archivo, 0777);

			$excel = Excel::selectSheetsByIndex(0)->load($ruta . $nombre_archivo)->get();

			$sucursal=5;
			$id = $this->id_control;

			libro::where('controlfacturacion_id', '=', $id)->delete();

	 		foreach ($excel as $key => $value) {
					
				$total = floatval(str_replace(',', '.', $value->total));
				$fecha2= Carbon::parse($value->fecha)->format("Y-m-d");	

				libro::create([
					'fecha'					=>	$fecha2,
					'desde'					=>	intval($value->desde),
					'hasta'					=>	intval($value->hasta),
					'total'					=>	$total,
					'sucursal_id'			=>	$sucursal,
					'controlfacturacion_id' =>	$id
				]);
			}	
		} catch (Exception $e) {
			DB::rollback();
			return $e->errorInfo[2];
		}

		DB::commit();
		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function export()
	{

		$movimientos = DB::table('afacturar')
		->select('afacturar.fecha',DB::raw('min(correlativo) as desde'), DB::raw('max(correlativo) as hasta'),
			DB::raw('sum(afacturar.total) as total'), 'afacturar.sucursal_id')
		->groupBy('afacturar.fecha','afacturar.sucursal_id')
		->orderBy('afacturar.fecha', 'asc')
		->orderBy('afacturar.sucursal_id', 'asc')
		->get();

		$id = $this->id_control;
		foreach ($movimientos as $value) {
				
			$total = floatval(str_replace(',', '.', $value->total));

			$libro = libro::firstOrNew([
				'fecha'					=>	$value->fecha,
				'sucursal_id'			=>	$value->sucursal_id,
				'controlfacturacion_id' =>	$id,
			]);

			$libro->desde 			= $value->desde;
			$libro->hasta 			= $value->hasta;
			$libro->total 			= $total;

			$libro->save();
		}	

	
		Excel::create('Libro Excel', function($excel) {

            $excel->sheet('Libro', function($sheet) {
						
				$libro = libro::select('libro.fecha','libro.desde','libro.hasta','libro.total', 'sucursal.abreviatura')
					->join('sucursal', 'sucursal.id', '=', 'libro.sucursal_id')
					->orderBy('libro.fecha', 'asc')
					->orderBy('libro.sucursal_id', 'asc')
					->orderBy('libro.desde', 'asc')
					->get()->toArray();
				
				foreach ($libro as $value) {
					$cantidad = floatval(str_replace(',', '.', $value['total']));
					$datos[]=[
						'dia' 		 => $value['fecha'],	
						'sucursal'	 => $value['abreviatura'],
						'Desde'	     => $value['desde'],
						'sucursal_2' => $value['abreviatura'],
						'hasta'		 => $value['hasta'],
						'total'		 => $cantidad
					];
				}	
	            
				$sheet->fromArray($datos);

            });

        })->export('xls');	
	}
	
}