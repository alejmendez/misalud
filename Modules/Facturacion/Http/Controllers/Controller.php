<?php namespace Modules\Facturacion\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController {
	public $app = 'admin';
	protected $patch_js = [
		'public/js',
		'public/plugins',
		'Modules/Facturacion/Assets/js',
	];

	protected $patch_css = [
		'public/css',
		'public/plugins',
		'Modules/Facturacion/Assets/css',
	];
}