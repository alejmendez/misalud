<?php

namespace Modules\Facturacion\Http\Controllers;

//Controlador Padre
use Modules\Facturacion\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;

//Request
use Modules\Facturacion\Http\Requests\ControlfacturacionRequest;

//Modelos
use Modules\Facturacion\Model\Controlfacturacion;
use Modules\Facturacion\Model\Movimientos;
use Modules\Cobranza\Model\ContratosFacturar;

class ControlfacturacionController extends Controller {
	protected $titulo = 'Nueva Facturacion';
	
	public $librerias = [
		'ladda',
	];

	public $js=[
		'Controlfacturacion'
	];
	public $css=[
		'Controlfacturacion'
	];
	public $meses = [		
		1 =>'Enero',
		'Febrero', 
		'Marzo',
		'Abril',
		'Mayo',				 
		'Junio',
		'Julio',
		'Agosto',
		'Septiembre',
		'Octubre',
		'Noviembre',
		'Diciembre',
	];

	public function index() {
		return $this->view('facturacion::Controlfacturacion');
	}

	/* public function verificar()
	{
	 	$cantidad =Controlfacturacion::select('estatus')->where('estatus', '=', '1')->count();

	 	if($cantidad > 0){
	 		return 1; //activo
	 	}

	 	return 0; //no activo
	} */

	public  function resulcarga(Request $request)
	{

	 $query = Movimientos::select(DB::raw('SUM(monto) as total'),DB::raw('count(controlfacturacion_id) as movimientos'))
			->where('controlfacturacion_id', '=', $request->id)
			->first();

			Controlfacturacion::find($request->id)->update([
				'total_registros' =>  $query->movimientos,
				'total_bolivares' =>  $query->total
			]);

			 return (array) $query->toArray();
	}

	public function Guardar(Request $request){

		DB::beginTransaction();
		try {
			
			//dd($request->all());
			//estoy en Controller principal "verificar()"
			if($this->verificar() == 1){
				return ['s' => 'n', 'msj' => 'Aviso: No se puede crear  un nuevo proceso de facturacion, ya que existe uno en curso'];
			}

			$estatus=1;
			//$fecha2= Carbon::parse($request->fecha_update_client)->format("Y-m-d");
			$rs = Controlfacturacion::create([
				'fecha_inicio'			=> date('Y-m-d'),
				'mes'					=> $request->mes,
				'ano'					=> $request->ano,
				'estatus'				=> $estatus,
			]);
			
			$id = $rs['id']; 
			/* ["ci","fecha","monto","sucursal_id","controlfacturacion_id"] */	
			$contratos_facturar = ContratosFacturar::select([
				'contratos_facturar.contratos_id',
				'cobros.id as cobro_id',
				'personas.dni',
				'cobros.total_pagado',
				'cobros.fecha_pagado',
				'contratos.sucursal_id'
			])
			->leftJoin('cobros', 'cobros.contratos_id', '=', 'contratos_facturar.contratos_id')
			->leftJoin('personas', 'personas.id', '=', 'cobros.personas_id')
			->leftJoin('contratos', 'contratos.id', '=', 'contratos_facturar.contratos_id')
			->where(DB::RAW('EXTRACT(MONTH FROM contratos_facturar.fecha_facturar)'), $request->mes)
			->where(DB::RAW('EXTRACT(YEAR FROM contratos_facturar.fecha_facturar)'), $request->ano)
			->where('cobros.completo', true)
			->where('contratos.cobrando', true);

			$movimientos = [];
			
			foreach ($contratos_facturar->get() as $key => $datos) {

				$sucursal_id = $datos->sucursal_id;

				if($datos->sucursal_id == 4){
					$sucursal_id = 1;
				}

				if($datos->total_pagado ==''){
					continue;
				}

				Movimientos::create([
						"ci"					=> $datos->dni,
						"fecha"					=> $datos->fecha_pagado,
						"monto"					=> $datos->total_pagado,
						"sucursal_id"			=> $sucursal_id,
						"controlfacturacion_id" => $id
					]);
				}

		} catch (Exception $e) {
			DB::rollback();
			return $e->errorInfo[2];
		}

		DB::commit();
		return ['s' => 's', 'msj' => trans('controller.incluir'), 'id' => $id];
	}

}