<?php

namespace Modules\Facturacion\Http\Controllers;

//Controlador Padre
use Modules\Facturacion\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Session;
use Carbon\Carbon;
//Request
use Modules\Facturacion\Http\Requests\AfacturarRequest;

//Modelos
use Modules\Facturacion\Model\Afacturar;
use Modules\Facturacion\Model\Movimientos;
use Modules\Facturacion\Model\Controlfacturacion;
use Modules\Empresa\Model\Sucursal;

class AfacturarController extends Controller {
	protected $titulo = 'Afacturar';

	public $librerias = [
		'ladda'
	];
	public $js=[
		'Afacturar'
	];

	public $meses = [		
		1 => 'Enero',
		'Febrero', 
		'Marzo',
		'Abril',
		'Mayo',				 
		'Junio',
		'Julio',
		'Agosto',
		'Septiembre',
		'Octubre',
		'Noviembre',
		'Diciembre',
	];

	public function index() {
			
		return $this->view('facturacion::Afacturar',[
			//estoy en controller principal 
			'control' => $this->verificar()
		]);
	}

	public function Buscar(Request $request){

		$id = $request->id;

		$rs = sucursal::find($id);
		$correlativo= $rs->correlativo;

		$movimientos = Movimientos::select([
			DB::RAW('SUM(movimientos.monto) as monto'),
			DB::RAW('COUNT(movimientos.id) as movimientos')
		])->where('sucursal_id',$id);

		if($id == 4){
			$movimientos->where('sucursal_id',4);
		}


		$afacturar = Afacturar::select(DB::RAW('SUM(afacturar.total) as monto'),DB::RAW('COUNT(afacturar.id) as afacturar'), DB::RAW('COUNT(afacturar.correlativo) as correlativo'))
			->where('sucursal_id',$id)->first();
	
		$datos=[
			'correlativo' => $correlativo,
			'movimientos' => $movimientos->first(),
			'afacturar'   => $afacturar
			];
			return $datos;
	}

	public function agrupar(Request $request){
	
		$id=$request->id;

		DB::beginTransaction();
			try {
				$rs = afacturar::select(DB::RAW('COUNT(afacturar.id) as id'))->where('sucursal_id', '=', $id)->first();	
					if($rs->id > 0){
						afacturar::where('sucursal_id', '=', $id)->delete();
					
					}
						DB::select('select * from agrupar(:id)', ['id' => $id]);				 
				}
			 catch (Exception $e) {
				DB::rollback();
				return $e->errorInfo[2];
			}
			DB::commit();
			return ['s' => 's', 'msj' => 'Operacion exitosa'];
	}

	public function correlativo(Request $request)
	{
		$id = $request->id;
	
	 	DB::beginTransaction();
		try {
				$rs = sucursal::find($id);
				$actual  =  $rs['correlativo'];

				$facturas=afacturar::select('id','fecha')->where('sucursal_id', '=', $id)->orderBy('fecha', 'asc')->get()->toArray();

				foreach ($facturas as $factura) {
					afacturar::find($factura['id'])->update([
						'correlativo' =>  $actual = $actual + 1,
					]);
				}

				sucursal::find($id)->update([
					'correlativo' =>  $actual,
				]);
			}
		 catch (Exception $e) {
			DB::rollback();
			return $e->errorInfo[2];
		}
		DB::commit();
		return ['s' => 's', 'msj' => 'Operacion exitosa'];
	}

	public function facturas(Request $request)
	{
		mb_internal_encoding('UTF-8');
		$id =$request->id;
	 	$cantidad = Controlfacturacion::select('mes')
			->where('estatus', '=', '1')
		->first();
		$mese = $this->meses[$cantidad->mes];

		$movimientos = Afacturar::select( 
			'tipo_persona.nombre as nacionalidad',
			'afacturar.ci',
			'personas.nombres',
			'personas_direccion.direccion',
			'personas_direccion.estados_id',
			//'personas.estado',
			//'personas.ciudad',
			'afacturar.total',
			'afacturar.fecha',
			'afacturar.correlativo'
		)
		->leftJoin('personas', 'personas.dni', '=', 'afacturar.ci')
		->leftJoin('personas_direccion', 'personas_direccion.personas_id', '=', 'personas.id')
		->leftJoin('tipo_persona', 'tipo_persona.id', '=', 'personas.tipo_persona_id')
		//->leftJoin('sucursal', 'personas.sucursal_id', '=', 'sucursal.id')
		->where('afacturar.sucursal_id', '=', $id)
		->orderBy('correlativo', 'asc')
		->get();

	
		switch ($id) {
			case 1:
				$con= "";
				$sucur= 'bolivar';  
				break;
			case 2:
				$con="SERIE B";
				$sucur= 'Puerto-ordaz';  
				break;
			case 3:
				$con="SERIE C";
				$sucur= 'Tigre'; 
				break;
			}
		header("Content-type: text/plain");
		header("Content-Disposition: attachment; filename=1-".$sucur."-".$mese.".txt");
		header("Pragma: no-cache");
		header("Expires: 0");

		foreach ($movimientos as $movimiento) {
			
			switch ($id) {
				case 1:
					echo "\n";
					echo "\n";
					echo "\n";
					echo "\n";
					echo "\n";
					echo "\n";
					if($movimiento->estados_id != 6){
						$movimiento->direccion = "Ciudad Bolivar";
					}
					
					break;
				case 2:
					echo "\n";
					echo "\n";
					echo "SUCURSAL: CALLE CUCHIVERO TORRE \n";
					echo "CONTINENTAL NIVEL MEZZANINA LOCAL 15\n";
					echo "PTO ORDAZ - ESTADO BOLIVAR\n";
					echo "\n";
					break;
				case 3:
					echo "\n";
					echo "\n";
					echo "CENTRO EMPRESARIAL IRPINIA PISO \n";
					echo "AVENIDA WINSTON CHURCHILL\n";
					echo "EL TIGRE, ESTADO ANZOATEGUI\n";
					echo "\n";
					break;
			}

			echo '         CONTRIBUYENTE FORMAL';
			echo "\n";
			echo '    FECHA             '.$con.' NRO. FACTURA';
			echo "\n  ";
			echo date('d/m/Y', strtotime($movimiento->fecha));
			echo "                ";
			echo $movimiento->correlativo;
			echo "\n";
			echo "CLIENTE: ".strtoupper($movimiento->nombre);
			echo "\n";
			echo "R.I.F: ".strtoupper($movimiento->nacionalidad).'-'.$movimiento->ci;
			echo "\n";
			echo "DIRECCION: ".strtoupper(substr($movimiento->direccion, 0, 28));
			echo "\n";
			echo substr(strtoupper($movimiento->direccion), 28, 39);
			
			echo "\n";
			echo "CIUDAD: ";
			switch ($id) {
				case 1:
					echo "BOLIVAR";
					break;
				case 2:
					echo "GUAYANA";
					break;
				case 3:
					echo "EL TIGRE";
					break;
			}
			echo "\n";
			echo str_repeat("-", 39);
			echo "\n";
			echo str_pad("CONCEPTO", 39, " ", STR_PAD_BOTH);
			echo "\n";
			echo str_repeat("-", 39);
			echo "\n";
			echo "\n";
			echo "PAGO DE CONSULTAS MEDICAS Y ODONTOLOGI-\n";
			echo "CAS CORRESPONDIENTES AL MES DE\n";
			echo "$mese DEL ".date('Y') ." (E)\n";
			echo "\n";
			echo str_repeat("-", 39);
			echo "\n";
			echo "SUB-TOTAL: ";
			echo str_pad(strval($movimiento->total), 28, " ", STR_PAD_LEFT);
			echo "\n";
			echo "EXENTO   :";
			echo str_pad(strval($movimiento->total), 29, " ", STR_PAD_LEFT);
			echo "\n";
			echo "I.V.A.   :                         0.00";
			echo "\n";
			echo "TOTAL    :";
			echo str_pad(strval($movimiento->total), 29, " ", STR_PAD_LEFT);
			echo "\n";
			echo "\n";
			echo "\n";
			echo "\n";
			echo "\n";
		};	
			
	}

}