<?php namespace Modules\Facturacion\Http\Controllers;

//Controlador Padre
use Modules\Facturacion\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;

//Request
use Modules\Facturacion\Http\Requests\ControlfacturacionRequest;

//Modelos
use Modules\Facturacion\Model\Controlfacturacion;
use Modules\Facturacion\Model\Movimientos;
use Modules\Cobranza\Model\ContratosFacturar;

class CierreFacturacionController extends Controller {
	
	protected $titulo = 'Cierre';
	
	protected $id_control;

	public $js=[
	 'cierre'
	];

	public function __construct()
	{
		parent::__construct();
		$rs = controlfacturacion::where('estatus', '=', 1)->first();
		$this->id_control = $rs['id'];
	}

	public function index()
	{
		return $this->view('facturacion::cierre',[
			'control' => $this->verificar(),//estoy en el controlador principal
		]);
	}

	/* public function verificar()
	{
	 	$cantidad = DB::table('controlfacturacion')
	 			->select('estatus')
	 			->where('estatus', '=', '1')
				->count();

	 	if($cantidad > 0){
	 		return 1; //activo
	 	}
	 	return 0; //no activo
	} */

	public function cierre(){	

	DB::beginTransaction();
		try {

			$total_facturado = DB::table('afacturar')
 			->select(DB::raw('sum(total) as total'))
			->first();	
				$rs = controlfacturacion::where('estatus', '=', 1)->first();
			Controlfacturacion::where('id', $rs->id)->update([
				"fecha_final"=> date('Y-m-d'),
				"total_facturado" => $total_facturado->total,
				"estatus" => 0
			]);

			DB::select('select * from historial()');		

			DB::table('libro')->truncate();
			DB::table('movimientos')->truncate();
			DB::table('afacturar')->truncate();
			//DB::table('controlfacturacion')->truncate(); 
	
		}
		catch (Exception $e) {
			DB::rollback();
			return $e->errorInfo[2];
		}
		DB::commit();
		return ['s' => 's', 'msj' => 'Operacion exitosa'];
	}
}
