<?php

	Route::group(['prefix' => 'facturacion'], function() {
		Route::get('/', 					'ControlfacturacionController@index');
		Route::post('guardar', 		'ControlfacturacionController@guardar');
		Route::post('resulcarga', 'ControlfacturacionController@resulcarga');
	});
	
	Route::group(['prefix' => 'afacturar'], function() {
		Route::get('/', 			'AfacturarController@index');
		Route::get('buscar', 		'AfacturarController@buscar');
		Route::get('agrupar', 		'AfacturarController@agrupar');
		Route::get('correlativo', 	'AfacturarController@correlativo');
		Route::post('facturas', 	'AfacturarController@facturas');
	});

	Route::group(['prefix' => 'libro'], function() {
		Route::get('/', 	'LibroController@index');
		Route::post('carga', 'LibroController@carga');
		Route::get('export', 'LibroController@export');
	});
	
	Route::group(['prefix' => 'cierre'], function() {
		Route::get('/', 	 'CierreFacturacionController@index');
		Route::get('cierre', 'CierreFacturacionController@cierre');
	});

