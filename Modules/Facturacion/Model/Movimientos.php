<?php
namespace Modules\Facturacion\Model;

use Illuminate\Database\Eloquent\Model;

class Movimientos extends Model
{
	protected $table = 'movimientos';
    protected $fillable = ["ci","fecha","monto","sucursal_id","controlfacturacion_id"];
   

    public function __construct(array $attributes = array())
	{
		parent::__construct($attributes);
		
	}
}