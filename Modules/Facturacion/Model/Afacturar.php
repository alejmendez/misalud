<?php
namespace Modules\Facturacion\Model;

use Illuminate\Database\Eloquent\Model;

class Afacturar extends Model
{
	protected $table = 'afacturar';
    protected $fillable = ["ci","fecha","total","correlativo","sucursal_id","controlfacturacion_id"];
    
    public function __construct(array $attributes = array())
	{
		parent::__construct($attributes);
		
	}
}