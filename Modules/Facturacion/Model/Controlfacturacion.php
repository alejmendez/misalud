<?php
namespace Modules\Facturacion\Model;

use Modules\Base\Model\Modelo;

class Controlfacturacion extends modelo
{
	protected $table = 'controlfacturacion';
    protected $fillable = ["fecha_inicio","fecha_final","total_registros","total_bolivares","total_facturado","mes","ano","estatus"];
   



    public function __construct(array $attributes = array())
	{
		parent::__construct($attributes);
		
	}
}