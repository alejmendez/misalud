<?php
namespace Modules\Facturacion\Model;

use Illuminate\Database\Eloquent\Model;

class Libro extends Model
{
	protected $table = 'libro';
    protected $fillable = ["fecha","desde","hasta","total","sucursal_id","controlfacturacion_id"];
    protected $campos = [
    'fecha' => [
        'type' => 'date',
        'label' => 'Fecha',
        'placeholder' => 'Fecha del Libro',
        'required' => true
    ],
    'desde' => [
        'type' => 'number',
        'label' => 'Desde',
        'placeholder' => 'Desde del Libro',
        'required' => true
    ],
    'hasta' => [
        'type' => 'number',
        'label' => 'Hasta',
        'placeholder' => 'Hasta del Libro',
        'required' => true
    ],
    'total' => [
        'type' => 'text',
        'label' => 'Total',
        'placeholder' => 'Total del Libro',
        'required' => true
    ],
    'sucursal_id' => [
        'type' => 'number',
        'label' => 'Sucursal',
        'placeholder' => 'Sucursal del Libro',
        'required' => true
    ],
    'controlfacturacion_id' => [
        'type' => 'number',
        'label' => 'Controlfacturacion',
        'placeholder' => 'Controlfacturacion del Libro',
        'required' => true
    ]
];

    public function __construct(array $attributes = array())
	{
		parent::__construct($attributes);
		
	}
}