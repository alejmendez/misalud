CREATE FUNCTION agrupar(_id integer)
  RETURNS void AS
  $BODY$
      BEGIN
      
     INSERT INTO afacturar(ci, fecha, total, sucursal_id, controlfacturacion_id, created_at, updated_at)

      select movimientos.ci, MAX(movimientos.fecha) as fecha, SUM(movimientos.monto) as total, movimientos.sucursal_id, movimientos.controlfacturacion_id, now() as created_at ,now() as updated_at
      from movimientos 
      where movimientos.sucursal_id = _id
      group by movimientos.ci, movimientos.controlfacturacion_id, movimientos.sucursal_id;
          END;
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;


 -------------------------------------------------------------------------------------------------------------------------

  CREATE FUNCTION historial()
  RETURNS void AS
  $BODY$
  BEGIN
      
  INSERT INTO
  movimientos_historial(ci, fecha, monto, sucursal_id, controlfacturacion_id, created_at, updated_at)

  select  movimientos.ci, 
  movimientos.fecha, 
  movimientos.monto, 
  movimientos.sucursal_id, 
  movimientos.controlfacturacion_id, 
  movimientos.created_at, 
  movimientos.updated_at 
  from    movimientos;



  INSERT INTO
      afacturar_historial(ci, fecha, total, correlativo, sucursal_id, controlfacturacion_id, created_at, updated_at)
    
  select  afacturar.ci, 
    afacturar.fecha, 
    afacturar.total, 
    afacturar.correlativo, 
    afacturar.sucursal_id, 
    afacturar.controlfacturacion_id, 
    afacturar.created_at, 
    afacturar.updated_at 
    
  from  afacturar;


  INSERT INTO
   libro_historial(fecha, desde, hasta, total, sucursal_id, controlfacturacion_id, created_at, updated_at)
    
  select libro.fecha, 
    libro.desde, 
    libro.hasta, 
    libro.total, 
    libro.sucursal_id, 
    libro.controlfacturacion_id, 
    libro.created_at, 
    libro.updated_at 
    
  from    libro;


      END;
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;