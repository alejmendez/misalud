<?php

namespace Modules\Cobranza\Http\Requests;

use App\Http\Requests\Request;

class ContratosRequest extends Request {
    protected $reglasArr = [
		'inicio'        			    => ['required', 'date_format:"d/m/Y"'], 
		'sucursal_id'         			=> ['required', 'integer'], 
		'vendedor_id'         			=> ['required', 'integer'], 
		'planilla' 	          			=> ['required', 'min:3', 'max:255'], 
		'dni' 	   	          			=> ['required', 'integer'], 
		'goza' 	   	      	  			=> ['required'], 
		'planes_id'     	 		 	=> ['required'], 
		'Num_benefe'   	 	 		    => ['required'], 
		'frecuencia'   	  	  			=> ['required'], 
		'tipo_pago'   	 	  			=> ['required'], 
		
		'n_recibo'   	  	  			=> ['required'],
		'fecha_decontado'     			=> ['required', 'date_format:"d/m/Y"'], 
		'pago'   	  	  	  			=> ['required'], 
		 
		'personas_bancos_id'  			=> ['required'], 
		'fecha_incial'        			=> ['required', 'date_format:"d/m/Y"'], 
		'frecuencia_pagos_id' 			=> ['required'], 
		

		'nombres_beneficiario.*' 		=> ['required'], 
		'sexo_beneficiario.*'     		=> ['required'], 
		'nacimiento_beneficiario.*' 	=> ['required', 'date_format:"d/m/Y"'], 
		'parentescos_beneficioario.*'   => ['required']	
	];

	public function rules() {
		$rules = parent::rules();


		switch ($this->method()){
			case 'PUT':
			case 'POST':
			case 'GET':
			case 'PATCH':
				if ($this->input('tipo_pago') == 1) {
					
					unset($rules['personas_bancos_id']);
					unset($rules['fecha_incial']);
					unset($rules['frecuencia_pagos_id']);
				
				}elseif($this->input('tipo_pago') == 2){

					unset($rules['n_recibo']);
					unset($rules['fecha_decontado']);
					unset($rules['pago']);

				}

				if(auth()->user()->super === 'n'){
					unset($rules['vendedor_id']);
				}


				break;
		}

		return $rules;
	}

}