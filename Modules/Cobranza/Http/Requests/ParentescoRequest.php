<?php

namespace Modules\Cobranza\Http\Requests;

use App\Http\Requests\Request;

class ParentescoRequest extends Request {
    protected $reglasArr = [
		'nombre' => ['required', 'min:3', 'max:100']
	];
}