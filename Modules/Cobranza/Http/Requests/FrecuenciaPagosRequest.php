<?php

namespace Modules\Cobranza\Http\Requests;

use App\Http\Requests\Request;

class FrecuenciaPagosRequest extends Request {
    protected $reglasArr = [
		'frecuencia' => ['required'], 
		'dias' => ['required']
	];
}