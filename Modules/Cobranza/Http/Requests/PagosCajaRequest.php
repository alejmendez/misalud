<?php

namespace Modules\Cobranza\Http\Requests;

use App\Http\Requests\Request;

class PagosCajaRequest extends Request {
    protected $reglasArr = [

		'sucursal_id'  => ['required', 'integer'], 
		'recibo'  => ['required'], 
			
	];

}