<?php

namespace Modules\Cobranza\Http\Controllers;

//Controlador Padre
use Modules\Cobranza\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use Modules\Cobranza\Http\Requests\ContratoTipoRequest;

//Modelos
use Modules\Cobranza\Model\ContratoTipo;

class ContratoTipoController extends Controller
{
    protected $titulo = 'Contrato Tipo';

    public $js = [
        'ContratoTipo'
    ];
    
    public $css = [
        'ContratoTipo'
    ];

    public $librerias = [
        'datatables',
         
    ];

    public function index()
    {
        return $this->view('cobranza::ContratoTipo', [
            'ContratoTipo' => new ContratoTipo()
        ]);
    }

    public function nuevo()
    {
        $ContratoTipo = new ContratoTipo();
        return $this->view('cobranza::ContratoTipo', [
            'layouts' => 'base::layouts.popup',
            'ContratoTipo' => $ContratoTipo
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $ContratoTipo = ContratoTipo::find($id);
        return $this->view('cobranza::ContratoTipo', [
            'layouts' => 'base::layouts.popup',
            'ContratoTipo' => $ContratoTipo
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $ContratoTipo = ContratoTipo::withTrashed()->find($id);
        } else {
            $ContratoTipo = ContratoTipo::find($id);
        }

        if ($ContratoTipo) {
            return array_merge($ContratoTipo->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(ContratoTipoRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $ContratoTipo = $id == 0 ? new ContratoTipo() : ContratoTipo::find($id);

            $ContratoTipo->fill($request->all());
            $ContratoTipo->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $ContratoTipo->id,
            'texto' => $ContratoTipo->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            ContratoTipo::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            ContratoTipo::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            ContratoTipo::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = ContratoTipo::select([
            'id', 'nombre', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}