<?php

namespace Modules\Cobranza\Http\Controllers;

//Controlador Padre
use Modules\Cobranza\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;
use Carbon\Carbon;
//Request
use Modules\Cobranza\Http\Requests\ContratoTipoRequest;
use Modules\Cobranza\Http\Requests\PagosCajaRequest;
use Session;
//Modelos

use Modules\Empresa\Model\Sucursal;
use Modules\Cobranza\Model\Contratos;
use Modules\Cobranza\Model\Cobros;
use Modules\Cobranza\Model\FrecuenciaPagos;
use Modules\Cobranza\Model\ContratosFacturar;

class PagosCajasController extends Controller
{
    protected $titulo = 'Pago Caja';
    
    public $autenticar = false;

    public $js = [
        'Pagos'
    ];

    public $css = [
        ''
    ];

    public $librerias = [
       'bootstrap-select',
       'bootstrap-touchspin',
       'bootstrap-switch' 
         
    ];

    public function index(Request $resquet, $id = 0)
    {
        $contrato = Contratos::find($id);
      
        $cuotas_pendientes =  $contrato->cuotas - $contrato->cuotas_pagadas; 
        $inicial = 'n';
        
        if($contrato->fecha_incial ===  $contrato->fecha_pago ){
             $inicial = 's';
        }
        
        return $this->view('cobranza::popup-pagocaja', [
            'layouts' => 'base::layouts.popup',
            'contrato' => $contrato,
            'cuotas_pendientes'=> intval($cuotas_pendientes),
            'inicial' => $inicial   
        ]);
    }

    public function sucursales()
    {
      return Sucursal::where('empresa_id', Session::get('empresa'))->where('id','!=',5)->pluck('nombre', 'id');
    }

    public function debe(Request $resquet)
    {
       return   Cobros::where('contratos_id', $resquet->id)->where('completo', false)->get();
    }

    public function guardar(PagosCajaRequest $resquet){
        DB::beginTransaction();
        try{
            $num = 0;
            $total = 0;
            $date = Carbon::now();
            $contrato = Contratos::find(intval($resquet->id));
            
            //pagar la inicial
            if($resquet->inicial == "s"){
               
                $concepto = 'Pago de Inicial  del Contrato ID:' . $contrato->id . ' Tipo de pago: ' .$resquet->tipo_pago;
        
                Cobros::create([
                    'contratos_id'      => $resquet->id,
                    'sucursal_id'       => $resquet->sucursal_id,
                    'personas_id'       => $contrato->titular,
                    'total_cobrar'      => $contrato->inicial,
                    'concepto'          => $concepto,
                    'fecha_pagado'      => date('Y-m-d'),
                    'total_pagado'      => $contrato->inicial,
                    'num_recibo'        => $resquet->recibo,
                    'completo'          => 1,
                    'tipo_pago'         => $resquet->tipo_pago,
                ]);
                    
                $frecuencia = FrecuenciaPagos::find($contrato->frecuencia_pagos_id);
                
                if($frecuencia->frecuencia == 'm' ){
                    $date->day = $frecuencia->dias;
                }else{
                    $_dias = explode(',',  $frecuencia->dias);
                    $date->day = $_dias[0];
                }

                $date->addMonth();

                Contratos::where('id', $resquet['id'])->update([
                    'primer_cobro' => date('Y-m-d'),
                    'fecha_pago'   => $date->format('Y-m-d'),
                    'total_pagado' => $contrato->inicial
                ]);
            }

            if($resquet->cuotas > 0){
        
                $cuota =  $contrato->total_contrato  - $contrato->inicial;
                $cuota =  $cuota / $contrato->cuotas;

                $concepto = 'Pago de cuota  del Contrato ID:' . $contrato->id . ' Tipo de pago: ' .$resquet->tipo_pago;  
                for ($i=1; $i <= intval($resquet->cuotas); $i++) { 

                    Cobros::create([
                        'contratos_id' => $resquet->id,
                        'sucursal_id'  => $resquet->sucursal_id,
                        'personas_id'  => $contrato->titular,
                        'total_cobrar' => round($cuota,2),
                        'concepto'     => $concepto,
                        'fecha_pagado' => date('Y-m-d'),
                        'total_pagado' => round($cuota,2),
                        'num_recibo'   => $resquet->recibo,
                        'completo'     => 1,
                    ]);

                    $total = $total +  $cuota;
                }

                $total_pagado = $contrato->total_pagado + $total;
                $total_cuotas = $contrato->cuotas_pagadas + $resquet->cuotas;

                $frecuencia = FrecuenciaPagos::find($contrato->frecuencia_pagos_id);
                
                if($total_cuotas == $contrato->cuotas ){
                    $fecha = null;

                    Contratos::where('id', $resquet->id)->update([
                        'fecha_pago'     => null,
                        'total_pagado'   => $contrato->total_contrato,
                        'cuotas_pagadas' => $resquet->cuotas,
                        'cobrando'       => true,
                    ]);

                    ContratosFacturar::create([
                        "fecha_facturar" => date('Y-m-d'),
                        "contratos_id"   => $resquet->id
                    ]);
                }else{ 
                    if($frecuencia->frecuencia == 'm' ){
                        $date->day = $frecuencia->dias;
                        $date->addMonths($resquet->cuota);
                    }else{
                        //proceso por quincena

                        $contrato = Contratos::find(intval($resquet->id));
                        $date = Carbon::createFromFormat('d/m/Y', $contrato->fecha_pago);
                                
                        $_dias = explode(',',  $frecuencia->dias);
                        if ($resquet->cuotas == 1) {
                            if ($date->day < $_dias[0]) {
                                $date->day = $_dias[0];
                            } elseif($date->day >= $_dias[1]) {
                                $date->day = $_dias[0];
                                $date->addMonth();
                            } else {
                                $date->day = $_dias[1];
                            }
                        } else {
                            $date->day = $_dias[$resquet->cuota % 2 == 0 && $date->day > $_dias[0] ? 1 : 0];
                            $date->addMonths(floor($resquet->cuota / 2) + 1);
                        }       
                    }

                    Contratos::find($resquet['id'])->update([
                        'fecha_pago'     => $date->format('Y-m-d'),
                        'total_pagado'   => $total_pagado,
                        'cuotas_pagadas' => $total_cuotas,
                    ]);
                }
            }
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        
        DB::commit();
        return ['s' => 's', 'msj' => "Pago registrado"];
    }
}