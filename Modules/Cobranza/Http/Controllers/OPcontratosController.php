<?php

namespace Modules\Cobranza\Http\Controllers;

//Controlador Padre
use Modules\Cobranza\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;
use Session;
use Carbon\Carbon;
//Request
use Modules\Cobranza\Http\Requests\ContratosRequest;

//Modelos
use Modules\Cobranza\Model\Contratos;
use Modules\Cobranza\Model\ContratosTemp;
use Modules\Cobranza\Model\ContratosDetalles;


use Modules\Cobranza\Model\SolicitudBeneficiariosTemp;
use Modules\Cobranza\Model\BeneficiariosTemp;
use Modules\Cobranza\Model\ContratosFacturar;
use Modules\Cobranza\Model\Cobros;

use Modules\Base\Model\Solicitudes;
use Modules\Cobranza\Model\Beneficiarios;
use Modules\Base\Model\TipoPersona;

use Modules\Base\Model\Personas;
use Modules\Base\Model\PersonasDetalles;

use Modules\Base\Model\PersonasBancos;
use Modules\Ventas\Model\Plan;
use Modules\Ventas\Model\PlanDetalles;
use Modules\Cobranza\Model\FrecuenciaPagos;
use Modules\Cobranza\Model\Parentesco;
use Auth;

class OPcontratosController extends Controller
{
    public $autenticar = false;
    
    protected $titulo = '';
    public $js = [
    ];
    
    public $css = [
    ];

    public $librerias = [
    ];

    public function renovacion(Request $request, $id=0, $adonde = 0)
    {
        
        //adonde: 0 = analista, 1= vendedor, dice de adonde esta accediendo
        $this->librerias = [

            'jquery-ui',
            'jquery-ui-timepicker',
            'bootstrap-wizard',
            'maskMoney'
        ];

        $this->js=[
            'renovacion',
            'jquery.validate.min.js',
            'additional-methods.min.js',
            //'bootstrap-wizard/jquery.bootstrap.wizard.min.js'
            'select2.full.min.js'
        ];

        $this->css=[
            'select2.min.css',
            'select2-bootstrap.min.css'
        ];


        $num_beneficiarios = Contratos::select('beneficiarios','titular')->where('id', $id)->first();

        $cuentas = PersonasBancos::select('personas_bancos.id', 'personas_bancos.cuenta','bancos.nombre' )
            ->leftJoin('bancos', 'bancos.id','=','personas_bancos.bancos_id')
            ->where('personas_bancos.personas_id', $num_beneficiarios->titular)->get();


        return $this->view('cobranza::popup-renovacion', [
            'layouts'       => 'base::layouts.popup-consulta',
            'contratos_id'  => $id,
            'beneficiarios' => $num_beneficiarios->beneficiarios,
            'cuentas'       => $cuentas,
            'adonde'        => $adonde // es de adonde viene el formularo 
        ]);
    } 

    public function beneficiarios(Request $request, $id=0, $adonde = 0)
    {
        
        //adonde: 0 = analista, 1= vendedor, dice de adonde esta accediendo
        $this->librerias = [

            'jquery-ui',
            'jquery-ui-timepicker',
            'bootstrap-wizard',
            'template',
            'maskMoney'
        ];

        $this->js=[
            'popup-beneficiarios',
            'jquery.validate.min.js',
            'additional-methods.min.js',
            //'bootstrap-wizard/jquery.bootstrap.wizard.min.js'
            'select2.full.min.js'
        ];

        $this->css=[
            'select2.min.css',
            'select2-bootstrap.min.css'
        ];

        $num_beneficiarios = Contratos::select('beneficiarios','titular')->where('id', $id)->first();

        return $this->view('cobranza::popup-beneficiarios', [
            'layouts'       => 'base::layouts.popup-consulta',
            'contratos_id'  => $id,
            'beneficiarios' => $num_beneficiarios->beneficiarios,
            'id'       => $id,
            'adonde'   => $adonde // es de adonde viene el formularo 
        ]);
    } 
    public function beneficiariosconfirmacion(Request $request, $id=0, $adonde = 0)
    {
        
        //adonde: 0 = analista, 1= vendedor, dice de adonde esta accediendo
        
        $this->librerias = [
            'bootstrap-sweetalert'
        ];
        $this->js=[
            'beneficiariosconfirmacion'
             
        ];

        $this->css=[
            ''
        ];

   
        $solicitud = Solicitudes::find($id);

        //informacion del contrato actual
        $Contratos = $this->contratoactual($solicitud->indece);
           
        $persona = Personas::select('*')->where('id',$Contratos->titular)->first();

    
        $beneficiario = 0;
        $plan = 'Personalizado';

        if($Contratos->plan_detalles_id  != ''){

            $plan_detalles = PlanDetalles::where('id',$Contratos->plan_detalles_id )->first();
            $plan          = Plan::where('id',$plan_detalles->plan_id )->first();
            $beneficiario  = $plan_detalles->beneficiarios;
            $plan          = $plan->nombre;
        }else{
           $beneficiario =  Beneficiarios::where('contratos_id', $solicitud->indece )->where('estatus',0)->count(); 
        }

        $_beneficiarios = Beneficiarios::select(
            'personas.dni',
            'tipo_persona.nombre',
            'personas.nombres',
            'parentesco.nombre as parentesco',
            'personas_detalles.sexo',
            'personas_detalles.fecha_nacimiento'
        )
        ->leftJoin('personas', 'personas.id','=','beneficiarios.personas_id')
        ->leftJoin('personas_detalles', 'personas_detalles.personas_id','=','personas.id')
        ->leftJoin('tipo_persona', 'tipo_persona.id','=','personas.tipo_persona_id')
        ->leftJoin('parentesco', 'parentesco.id','=','beneficiarios.parentesco_id')
        ->where('contratos_id', $solicitud->indece )->where('estatus',0)->get();
    
        $contratos_detalles = ContratosDetalles::select(
            'personas.dni',
            'tipo_persona.nombre',
            'personas.nombres',
            'contratos_detalles.operacion',
            'contratos_detalles.planilla',
            'contratos_detalles.created_at'

        )
        ->leftJoin('personas', 'personas.id','=','contratos_detalles.personas_id')
        ->leftJoin('tipo_persona', 'tipo_persona.id','=','personas.tipo_persona_id')
        ->orderBy( 'contratos_detalles.id', 'desc')
        ->where('contratos_id',$solicitud->indece )->get();

        
        $solicitud_temp = SolicitudBeneficiariosTemp::where('solicitud_id', $solicitud->id)->first();

        /*
           "planilla"      => $request->planilla,
           "contratos_id"  => $request->id,
           "solicitud_id"  => $solicitud['id'],
           "fecha"         => $request->inicio,
           "operacion"     => $request->opera,
           "planes_id"     => $planes
        */

        $beneficiarios_temp = BeneficiariosTemp::select([
            "beneficiarios_temp.dni",
            "beneficiarios_temp.nombre",
            'parentesco.nombre as parentesco',
            "beneficiarios_temp.sexo",
            "beneficiarios_temp.solicitud_beneficiarios_temp_id",
            "beneficiarios_temp.parentesco_id",
            "beneficiarios_temp.fecha_nacimiento"
            ])
            ->leftJoin('parentesco', 'parentesco.id','=','beneficiarios_temp.parentesco_id')
            ->where('solicitud_beneficiarios_temp_id', $solicitud_temp->id)->get();

        $operacion='';

        $inf_plan= $plan;

        if($Contratos->tipo_pago == 1){
            $t = "De contado";
        }else{
            $t = "Credito";

        }
        $inf_tipo= $t;
        
        $inf_frecuencia     = $Contratos->frecuencia == 'm' ? 'Mensual'.' : '. $Contratos->dias : 'Quincenal' .' : '. $Contratos->dias;
        $inf_cuotas2        = $Contratos->total_contrato / $Contratos->cuotas;
        $inf_cuotas         = $Contratos->cuotas;
        $inf_total          = $Contratos->total_contrato;
        $inf_total_pagado   = $Contratos->total_pagado;
        
        $num_b = $beneficiarios_temp->count();
        
        switch ($solicitud_temp->operacion) {
            case 0:
                //cambio
                $operacion = 'Cambio de Beneficiario';
              
                break;
            case 1:
                //rediccion
                $operacion = 'Reduccion de Beneficiarios';
                $_plan_detalles2 = PlanDetalles::where('id', $Contratos->plan_detalles_id)->first();

                $_plan_detalles = PlanDetalles::where('plan_id',$_plan_detalles2->plan_id)
                    ->where('beneficiarios', $num_b)
                    ->first();

                    $inf_cuotas2 = $_plan_detalles->total / $Contratos->cuotas;

                    $inf_total   = $_plan_detalles->total;


            
                break;
            case 2:
                $operacion = 'Inclusion de Beneficiarios';
                $_plan = Plan::where('id',$solicitud_temp->planes_id )->first();
                $inf_plan = $_plan->nombre;


             

                $_plan_detalles = PlanDetalles::where('plan_id',$solicitud_temp->planes_id)
                ->where('beneficiarios', $num_b)
                ->first();

                $inf_cuotas2 = $_plan_detalles->total / $Contratos->cuotas;

                $inf_total   = $_plan_detalles->total;

                break; 
            default:
                # code...
                break;
        }


        return $this->view('cobranza::beneficiariosconfirmacion', [
            'layouts'           => 'base::layouts.popup-consulta',
            'Contrato'          => $Contratos,
            'Persona'           => $persona,
            'plan'              => $plan,
            'beneficiario'      => $beneficiario,
            'info_beneficiarios'=> $_beneficiarios,
            'beneficiarios_temp'=> $beneficiarios_temp,
            'contratos_detalles'=> $contratos_detalles,
            'tipo'              => $operacion,
            'solicitud_id'      => $solicitud->id,
            'inf_plan'          => $inf_plan,
            'inf_tipo'          => $inf_tipo,
            'inf_frecuencia'    => $inf_frecuencia,
            'inf_cuotas2'       => $inf_cuotas2,
            'inf_cuotas'        => $inf_cuotas,
            'inf_total'         => $inf_total,
            'inf_total_pagado'  => $inf_total_pagado
        ]);
    }

    public function confirmacionbene(Request $request){

       DB::beginTransaction();
        try{
            $datos = [];

            $solicitud = Solicitudes::find($request->id);



            $contrato  = Contratos::find($solicitud->indece);

            $solicitud_temp = SolicitudBeneficiariosTemp::where('solicitud_id', $solicitud->id)->first();

        /*
           "planilla"      => $request->planilla,
           "contratos_id"  => $request->id,
           "solicitud_id"  => $solicitud['id'],
           "fecha"         => $request->inicio,
           "operacion"     => $request->opera,
           "planes_id"     => $planes
        */

            $beneficiarios_temp = BeneficiariosTemp::select([
                "beneficiarios_temp.dni",
                "beneficiarios_temp.nombre",
               
                "beneficiarios_temp.sexo",
                "beneficiarios_temp.solicitud_beneficiarios_temp_id",
                "beneficiarios_temp.parentesco_id",
                "beneficiarios_temp.fecha_nacimiento"
                ])
                ->where('solicitud_beneficiarios_temp_id', $solicitud_temp->id)->get();

            $this->guardarbeneficiarios2($beneficiarios_temp, $contrato->id);
            $num_b = $beneficiarios_temp->count();

            switch ($solicitud_temp->operacion) {
                case 0:
                    //cambio
                    $operacion = 'Confirmacion de Cambio de Beneficiario';
                    
                    break;
                case 1:
                    //reduccion
                    $operacion = 'Confirmacion deReduccion de Beneficiarios';
                    $_plan_detalles2 = PlanDetalles::where('id', $contrato->plan_detalles_id)->first();

                    $_plan_detalles = PlanDetalles::where('plan_id',$_plan_detalles2->plan_id)
                        ->where('beneficiarios', $num_b)
                        ->first();
                
                        $datos['plan_detalles_id']  = $_plan_detalles->id;
                        $datos['total_contrato']    = $_plan_detalles->total;
                        $datos['beneficiarios']     = $contrato->beneficiarios - 1 ;
                
                    break;
                case 2:
                    $operacion = 'Confirmacion de Inclusion de Beneficiarios';
                    $_plan = Plan::where('id',$solicitud_temp->planes_id )->first();
                    
                    $_plan_detalles = PlanDetalles::where('plan_id',$solicitud_temp->planes_id)
                    ->where('beneficiarios', $num_b)
                    ->first();

                    $datos['plan_detalles_id']  = $_plan_detalles->id;
                    $datos['total_contrato']    = $_plan_detalles->total;
                     $datos['beneficiarios']     = $contrato->beneficiarios + 1 ;
                    break; 
                default:
                    # code...
                    break;
            }

            $datos['planilla'] = $solicitud_temp->planilla;

            $contrato->update($datos);


            ContratosDetalles::create([
               "contratos_id" => $contrato->id,
               "personas_id"  => auth()->user()->personas_id,
               "operacion"    => $operacion,
               "planilla"     => $solicitud_temp->planilla
            ]); 


            Solicitudes::where('id', $request->id)->delete();


            SolicitudBeneficiariosTemp::where('solicitud_id', $solicitud->id)->delete();


            BeneficiariosTemp::where('solicitud_beneficiarios_temp_id', $solicitud_temp->id)->delete();


        } catch(QueryException $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];

    
    }
   
    public function opanular(Request $request, $id = 0, $porque = 0)
    {     
        $this->js=[
            'opconsulta'
        ];

        $Contratos = Contratos::find($request->id);

        ContratosFacturar::create([
            "fecha_facturar" => date('Y-m-d'),
            "contratos_id"   => $Contratos->id
        ]);
        $Contratos->fill([
            'anulado'=> 1
        ]);
        $Contratos->save();

    
        ContratosDetalles::create([
           "contratos_id" => $Contratos->id,
           "personas_id"  => auth()->user()->personas_id,
           "operacion"    => "Anulacion de Contrato Por: ".$request->porque,
           "planilla"     => $Contratos->planilla
        ]);

        if(!$Contratos){
             return ['s' => 'n', 'msj' => 'Error']; 
        }

        return ['s' => 's', 'msj' => trans('controller.incluir')]; 
  
    }

    public function renovacionGuardar(Request $request, $id = 0)
    {   
        DB::beginTransaction();
        try{
    
             $datos= $request->all();
             $contrato = Contratos::find($request->id);
            if($request->adonde == 0 ){
                //Crear decripcion para el contrato detalle
                $descripcion = "Resumen Contrato anterior, planilla: ". $contrato->planilla . ", fecha de contrato: ".$contrato->inicio. ", fecha vencimiento: ".  $contrato->vencimiento. ", total del contrato: ".$contrato->total_contrato.", total pagado: ". $contrato->total_pagado;
                
                ContratosDetalles::create([
                    "contratos_id" => $request->id,
                    "personas_id"  => auth()->user()->personas_id,
                    "operacion"    => $descripcion,
                    "planilla"     => 's/n'
                ]);


                $date =Carbon::createFromFormat('d/m/Y', $datos['inicio'])->addYear();

                $datos['vencimiento'] = $date;

                $plan_detalles = PlanDetalles::where('plan_id',$datos['planes_id'])->where('beneficiarios', $request['Num_benefe'])->first();

                $datos['plan_detalles_id']  = $plan_detalles->id;
                $datos['total_contrato']    = $plan_detalles->total;
                $datos['inicial']           = $plan_detalles->inicial;    

                $pago = 0;

                if($request['tipo_pago'] == 1 ){

                    $pago = $plan_detalles->contado;
                    $datos['total_contrato'] = $plan_detalles->contado; 
                  
                    $datos['cuotas'] = 1;
                    $datos['inicial'] = '0';
                    $datos['personas_bancos_id'] = null;
                    $datos['frecuencia_pagos_id'] = null;
                    $datos['primer_cobro'] = $request['fecha_decontado'];
                    $datos['fecha_incial'] = null;
                    $datos['cobrando'] = true;
                    $datos['fecha_incial'] = null;
                    $datos['fecha_pago']   = null;

                   
                    $fecha_facturar = date('Y-m-d');
                    $_tipo_pago = [
                        0 => 'Por Tarjeta',
                            'Transferencia',
                            'efectivo'
                    ];

                    Cobros::create([
                        "contratos_id"  => $contrato->id,
                        "sucursal_id"   => $contrato->sucursal_id,
                        "personas_id"   => $contrato->titular,
                        "total_cobrar"  => $pago,
                        "concepto"      => 'Pago de Contrato de Contado por: '.$_tipo_pago[$datos['pago']],
                        "cuota"         => 1,
                        "fecha_pagado"  => $request->fecha_decontado,
                        "total_pagado"  => $pago,
                        "num_recibo"    => $datos['n_recibo'],
                        "tipo_pago"     => 'caja',
                        "completo"      => true
                    ]);

                }else{

                    $plan = Plan::where('id',$request['planes_id'] )->first();
                    if($request['frecuencia'] == 'm'){
                        $cuotas = 1 * $plan->meses_pagar;
                    }else{
                        $cuotas = 2 * $plan->meses_pagar; 
                    }

                    $datos['fecha_facturacion'] = null;
                    $datos['cuotas'] = $cuotas;
                    $datos['primer_cobro'] = null;
                    $datos['cobrando'] = false;
                    $datos['fecha_pago'] =  $datos['fecha_incial'];
                    
                    $date =  Carbon::createFromFormat('d/m/Y', $datos['inicio']);
                    $fecha_facturar = $date->subDay(1)->format('Y-m-d');
                }

                $datos['total_pagado']  = $pago; 
                
                $datos['estatus_contrato_id']  = 1; 

                $datos['renovaciones']  = $contrato->renovaciones + 1;
            
                $datos['cuotas_pagadas']  = null;  
                $datos['anulado']  = 0;  

                ContratosFacturar::create([
                    "fecha_facturar" =>  $fecha_facturar,
                    "contratos_id"   => $contrato->id
                ]);
               
                $contrato->fill($datos);
                $contrato->save();

                ContratosDetalles::create([
                    "contratos_id" => $contrato->id,
                    "personas_id"  => auth()->user()->personas_id,
                    "operacion"    => 'Renovacion de Contrato',
                    "planilla"     => $contrato->planilla
                ]); 


            }else{
                //Solicitud
                $datos['contrato_id'] = $request['id'];
                $date =Carbon::createFromFormat('d/m/Y', $datos['inicio'])->addYear();
                $datos['vencimiento'] = $date;
                $plan_detalles = PlanDetalles::where('plan_id',$datos['planes_id'])->where('beneficiarios', $request['Num_benefe'])->first();

                $datos['plan_detalles_id']  = $plan_detalles->id;
                $datos['total_contrato']    = $plan_detalles->total;
                $datos['inicial']           = $plan_detalles->inicial; 
                $datos['beneficiarios']     = $request['Num_benefe']; 

                $pago = 0;

                if($request['tipo_pago'] == 1 ){
                    $pago = $plan_detalles->contado;
                    $datos['total_contrato'] = $plan_detalles->contado; 
                    $datos['cuotas'] = 1;
                    $datos['inicial'] = '0';
                    $datos['personas_bancos_id'] = null;
                    $datos['frecuencia_pagos_id'] = null;
                    $datos['primer_cobro'] = $request['fecha_decontado'];
                    $datos['fecha_incial'] = null;
                    //registrar cobro

                     $fecha_facturar = date('Y-m-d');
                    $_tipo_pago = [
                        0 => 'Por Tarjeta',
                            'Transferencia',
                            'efectivo'
                    ];

                    Cobros::create([
                        "contratos_id"  => $contrato->id,
                        "sucursal_id"   => $contrato->sucursal_id,
                        "personas_id"   => $contrato->titular,
                        "total_cobrar"  => $pago,
                        "concepto"      => 'Pago de Contrato de Contado por: '.$_tipo_pago[$datos['pago']],
                        "cuota"         => 1,
                        "fecha_pagado"  => $request->fecha_decontado,
                        "total_pagado"  => $pago,
                        "num_recibo"    => $datos['n_recibo'],
                        "tipo_pago"     => 'caja',
                        "completo"      => true
                    ]);

                }else{
                    $datos['fecha_decontado']= null;
                    $plan = Plan::where('id',$request['planes_id'] )->first();
                    
                    if($request['frecuencia'] == 'm'){
                        $cuotas = 1 * $plan->meses_pagar;
                    }else{
                        $cuotas = 2 * $plan->meses_pagar; 
                    }
                    
                    $datos['cuotas'] = $cuotas;
                    $datos['primer_cobro'] = null;
                     
                    $date =  Carbon::createFromFormat('d/m/Y', $datos['inicio']);
                   
                    $fecha_facturar = $date->subDay(1)->format('Y-m-d');
                }

                $datos['total_pagado']  = $pago; 
                unset($datos['n_recibo']);
                unset($datos['pago']);
                unset($datos['fecha_decontado']);
                $datos['cuotas_pagadas']  = null; 
                /*
                    Tipo solicitud 
                        1 = contrato nuevo
                        2 = renovacion contrato
                        3 = agregar beneficiarios
                        4 = modificacion de informacion personal

                    Solicitante 
                        id_persona quien solicita.
                    aquien 
                        1-contrato
                        2-persona
                    
                    indice 
                        persona_id o contrato_id
                */
                ContratosFacturar::create([
                    "fecha_facturar" =>  $fecha_facturar,
                    "contratos_id"   => $contrato->id
                ]);

                
                $solicitud = Solicitudes::create([
                    "tipo_solicitud" => 2,
                    "solicitante"    => auth()->user()->personas->id,
                    "aquien"         => 1,
                    "indece"         => $datos['contrato_id']
                ]); 

                $datos['solicitud_id'] = $solicitud['id']; 

                ContratosTemp::create($datos);

                ContratosDetalles::create([
                    "contratos_id" => $datos['contrato_id'],
                    "personas_id"  => auth()->user()->personas_id,
                    "operacion"    => 'Solicitud de Renovacion de Contrato',
                    "planilla"     => $datos['planilla']
                ]); 
            }
        } catch(QueryException $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }


    public function beneficiariosguardar(Request $request){

       DB::beginTransaction();
        try{
            $datos = [];
            $contrato = Contratos::find($request->id);

            if($request->adonde == 0 ){
                
                
                $datos['planilla'] = $request->planilla;
                $this->guardarbeneficiarios($request->all(), $request->id);
                
                switch ($request->opera) {
                    case 0:
                        //cambio
                        $operacion = 'Cambio de Beneficiario';

                        break;
                    case 1:
                        //rediccion
                        $operacion = 'Reduccion de Beneficiarios';

                        $_plan_detalles = PlanDetalles::where('id', $contrato->plan_detalles_id)->first();

                        $plan_detalles = PlanDetalles::where('plan_id', $_plan_detalles->plan_id)
                        ->where('beneficiarios', $request->Num_benefe)
                        ->first();

                            if($contrato->tipo_pago = 2)// credidto
                            {
                                $datos['total_contrato']   =  $plan_detalles->total;
                                $datos['plan_detalles_id'] =  $plan_detalles->id;
                                $datos['beneficiarios']    =  $request->Num_benefe;
                            }

                        break;
                    case 2:
                        $operacion = 'Inclusion de Beneficiarios';

                        if($contrato->tipo_pago = 2)// credidto
                        {
                            
                            $plan_detalles = PlanDetalles::where('plan_id', $request->planes_id)
                                ->where('beneficiarios', $request->Num_benefe)
                                ->first();

                            $datos['total_contrato']   =  $plan_detalles->total;
                            $datos['plan_detalles_id'] =  $plan_detalles->id;
                            $datos['beneficiarios']    =  $request->Num_benefe;

                        }

                        break; 
                    default:
                        # code...
                        break;
                }

                 $contrato->update($datos);

            }else{

                switch ($request->opera) {
                    case 0:
                        //cambio
                        $operacion = 'Solicitud de Cambio de Beneficiario';
                        $planes =  null;


                        break;
                    case 1:
                        //rediccion
                        $operacion = 'Solicitud de Reduccion de Beneficiarios';
                        $planes =  null;

                        break;
                    case 2:
                        $operacion = 'Solicitud de  Inclusion de Beneficiarios';
                        $planes =  $request->planes_id;

                        
                        break; 
                    default:
                        # code...
                        break;
                }

                /*
                Tipo solicitud 
                    1 = contrato nuevo
                    2 = renovacion contrato
                    3 = agregar beneficiarios
                    4 = modificacion de informacion personal
                Solicitante 
                    id_persona quien solicita.
                aquien 
                    1-contrato
                    2-persona
                indice 
                    persona_id o contrato_id
                */

                $solicitud = Solicitudes::create([
                   "tipo_solicitud" => 3,
                   "solicitante"    => auth()->user()->personas->id,
                   "aquien"         => 1,
                   "indece"         => $request->id
                ]);

                $solicitud_beneficiario = SolicitudBeneficiariosTemp::create([
                   "planilla"      => $request->planilla,
                   "contratos_id"  => $request->id,
                   "solicitud_id"  => $solicitud['id'],
                   "fecha"         => $request->inicio,
                   "operacion"     => $request->opera,
                   "planes_id"     => $planes
                ]);
                
                $dni = Personas::select('dni')->where('id', $contrato->titular)->first();

                $this->beneficiarios_temp($request->all(),$dni->dni,$solicitud_beneficiario['id']);
              
            }
                
            ContratosDetalles::create([
               "contratos_id" => $request->id,
               "personas_id"  => auth()->user()->personas_id,
               "operacion"    => $operacion,
               "planilla"     => $request->planilla
            ]); 


        } catch(QueryException $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            's'     => 's',
            'msj'   => trans('controller.incluir'),
            "donde" => $request->adonde
        ];

    }

    public function parentescos(){
        return Parentesco::all();
    }
    public function frecuencias(Request $request){
        return FrecuenciaPagos::where('frecuencia', $request->id)->get();
    }

    public function reenviarsolicitud(Request $request)
    {
         DB::beginTransaction();
        try {

            $Contratos = Contratos::where('id',$request->id )->first();

            $solicitud = Solicitudes::create([
               "tipo_solicitud" => 2,
               "solicitante"    => auth()->user()->personas->id,
               "aquien"         => 1,
               "indece"         => $request->id
            ]); 

            ContratosDetalles::create([
               "contratos_id" => $Contratos->id,
               "personas_id"  => auth()->user()->personas->id,
               "operacion"    => "Reenvio  de contrato",
               "planilla"     => $Contratos->planilla
            ]);

            $Contratos->update([
                'estatus_contrato_id'=> 2
            ]);            

        } catch (Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }

        DB::commit();

        return ['s' => 's']; 
    }

    public function beneficiarios_temp($request,$titular_dni,$id){
        DB::beginTransaction();
        try { 

            $datos = [];

            foreach ($request['dni_beneficiario'] as $_id => $beneficiario) {
               
                if($request['dni_beneficiario'][$_id] === ""){

                    $persona = Personas::where('dni', 'LIKE',$titular_dni.'%')->count();
                    $result = $persona +  1;
                    $dni = $request['dni'] . "-" . $result;

                }else{

                    $dni = $request['dni_beneficiario'][$_id];
                }

                $datos= [
                   "dni"                                => $dni,
                   "nombre"                             => $request['nombres_beneficiario'][$_id],
                   "sexo"                               => $request['sexo_beneficiario'][$_id],
                   "solicitud_beneficiarios_temp_id"    => $id,
                   "parentesco_id"                      => $request['parentescos_beneficioario'][$_id],
                   "fecha_nacimiento"                   => $request['nacimiento_beneficiario'][$_id]
                ];
                
                BeneficiariosTemp::create($datos);   
            }
   

        } catch (Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }

        DB::commit();

        return ['s' => 's'];
    }
    
    public function beneficiario(Request $request){

        $Beneficiarios = Beneficiarios::select(
            'personas.nombres',
            'personas.dni',
            'beneficiarios.parentesco_id',
            'personas_detalles.fecha_nacimiento',
            'personas_detalles.sexo'
            )
        ->leftjoin('personas','personas.id','=','beneficiarios.personas_id')
        ->leftjoin('personas_detalles','personas_detalles.personas_id','=','beneficiarios.personas_id')
        ->where('beneficiarios.contratos_id',$request->id)->get();
        
        
        foreach( $Beneficiarios as $beneficiario ){
            $beneficiario->fecha_nacimiento = Carbon::parse($beneficiario->fecha_nacimiento)->format('d/m/Y');
        }
        return ['datos' =>$Beneficiarios->toArray()];
    }

    public function plan(Request $request){
       
        $op = $request->op;  
        $cuentas =  "";

        $Contratos = Contratos::select([

            "contratos.frecuencia_pagos_id",
            "contratos.tipo_pago",
            "contratos.cobrando",
            "contratos.titular",
            "contratos.cobrando",
            "contratos.total_contrato",
            "contratos.cuotas",
            "contratos.total_pagado",
            "contratos.inicial",
            "contratos.anulado",
            "contratos.plan_detalles_id",
            "plan.id as plan_id"
        ])
        ->leftjoin('plan_detalles', 'plan_detalles.id',"=", "contratos.plan_detalles_id")
        ->leftjoin('plan', 'plan.id',"=", "plan_detalles.plan_id")
        ->where('contratos.id', $request->id_contrato)
        ->first();


        $Contratos->total_pagado2 = number_format($Contratos->total_pagado, 2, ',', '.');
        $Contratos->inicial2 = number_format($Contratos->inicial, 2, ',', '.');
        $Contratos->total_contrato2 = number_format($Contratos->total_contrato, 2, ',', '.');

        $plan_detalles = PlanDetalles::where('id', $Contratos->plan_detalles_id)->first();

        $plan_detalles->contado2 = number_format($plan_detalles->contado, 2, ',', '.');
        $plan_detalles->inicial2 = number_format($plan_detalles->inicial, 2, ',', '.');
        $plan_detalles->total2 = number_format($plan_detalles->total, 2, ',', '.');

        $frecuencia = FrecuenciaPagos::where('id', $Contratos->frecuencia_pagos_id)->first();

        $plan_old ='';
        $plan_detalles_new ="";
        $planes="Personalizado";

        if(!is_null($Contratos->plan_id)){
            $planes = Plan::where('id', $Contratos->plan_id)->first();
        }   
            
        if($op == 2){
            $plan_old  = $planes;
            $fecha = Carbon::createFromFormat('d/m/Y',$request->dato)->format("Y-m-d");
            
            $planes = Plan::where('desde','<=',  $fecha)
                ->where('hasta','>=',  $fecha)
                ->get();

            $cuentas = PersonasBancos::select('personas_bancos.id', 'personas_bancos.cuenta','bancos.nombre' )
                ->leftJoin('bancos', 'bancos.id','=','personas_bancos.bancos_id')
                ->where('personas_bancos.personas_id', $Contratos->titular)
                ->get();
        }

        if($op == 1){
            $plan_old  = $planes;
            $plan_detalles_new = PlanDetalles::where('plan_id', $plan_old->id)
                ->where('beneficiarios', $request->beneficiarios)
                ->first();
             $plan_detalles_new->contado2 = number_format($plan_detalles_new->contado, 2, ',', '.');
            $plan_detalles_new->inicial2 = number_format($plan_detalles_new->inicial, 2, ',', '.');
            $plan_detalles_new->total2 = number_format($plan_detalles_new->total, 2, ',', '.');
        }  

        $salida = [
            's'                 => 's', 
            'planes'            => $planes,
            'contrato'          => $Contratos,
            'plan_detalles'     => $plan_detalles,
            'cuentas'           => $cuentas,
            'frecuencia'        => $frecuencia,
            'plan_old'          => $plan_old,
            'plan_detalles_new' => $plan_detalles_new
        ];
        
        return $salida;
    }

    public function infoplan(Request $request){


        $plan = Plan::where('id',$request->plan_id )->where('empresa_id',Session::get('empresa'))->get();

        $plan_detalles = PlanDetalles::where('plan_id',$request->plan_id )->where('beneficiarios', $request->beneficiarios)->first();

        $plan_detalles->contado2 = number_format($plan_detalles->contado, 2, ',', '.');
        $plan_detalles->inicial2 = number_format($plan_detalles->inicial, 2, ',', '.');
        $plan_detalles->total2 = number_format($plan_detalles->total, 2, ',', '.');

        $frecuencia = FrecuenciaPagos::where('frecuencia', $request->frecuencia)->get();

        return['plan' => $plan, 'plan_detalles'=> $plan_detalles, 'frecuencia' => $frecuencia]; 

    }

    public function guardarbeneficiarios($request, $id){
        DB::beginTransaction();
        try { 

            Beneficiarios::where('contratos_id', $id)->delete();
            $datos = [];

            foreach ($request['dni_beneficiario'] as $_id => $beneficiario) {
               
                if($request['dni_beneficiario'][$_id] === ""){


                    $contrato = Contratos::select('personas.dni')->where('contratos.id', $id)
                    ->leftjoin('personas', 'personas.id', '=', 'contratos.titular')->first();


                    $persona = Personas::where('dni', 'LIKE', $contrato->dni.'%')->count();
                    $result = $persona +  1;
                    $dni = $contrato->dni . "-" . $result;

                }else{

                    $dni = $request['dni_beneficiario'][$_id];
                }

                $persona = Personas::where('dni', $dni)->first();

                if(count($persona) == 0){
                   $persona = Personas::create([
                       "tipo_persona_id" => 1,
                       "dni"             => $dni,
                       "nombres"         => $request['nombres_beneficiario'][$_id]
                    ]);

                    PersonasDetalles::create([
                       "personas_id"      => $persona->id,
                       "sexo"             => $request['sexo_beneficiario'][$_id],
                       "fecha_nacimiento" => $request['nacimiento_beneficiario'][$_id]
                    ]);
                }


                $datos= [
                    "contratos_id"  => $id,
                    "personas_id"   => $persona->id,
                    "parentesco_id" => $request['parentescos_beneficioario'][$_id],
                    "estatus"       => 0
                ];
                
                Beneficiarios::create($datos);   
            }
   

        } catch (Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }

        DB::commit();

        return ['s' => 's'];
    }

    public function guardarbeneficiarios2($request, $id){
        DB::beginTransaction();
        try { 

            Beneficiarios::where('contratos_id', $id)->delete();
            $datos = [];

            foreach ($request as $beneficiario) {
               
                $persona = Personas::where('dni', $beneficiario->dni)->first();

                if(count($persona) == 0){
                   $persona = Personas::create([
                       "tipo_persona_id" => 1,
                       "dni"             => $beneficiario->dni,
                       "nombres"         => $beneficiario->nombre
                    ]);

                    PersonasDetalles::create([
                       "personas_id"      => $persona->id,
                       "sexo"             => $beneficiario->sexo,
                       "fecha_nacimiento" => $beneficiario->fecha_nacimiento
                    ]);
                }


                $datos= [
                    "contratos_id"  => $id,
                    "personas_id"   => $persona->id,
                    "parentesco_id" => $beneficiario->parentesco_id,
                    "estatus"       => 0
                ];
                
                Beneficiarios::create($datos);   
            }
   

        } catch (Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }

        DB::commit();

        return ['s' => 's'];
    }

    public function contratoactual($id){

        $Contratos = Contratos::select(
            'contratos.id',
            'contratos.planilla',
            'contratos.titular',
            'contratos.vencimiento',
            'contratos.inicio',
            'contratos.cargado',
            'contratos.primer_cobro',
            'contratos.fecha_incial',
            'contratos.inicial',
            'contratos.renovaciones',
            'contratos.tipo_pago',
            'contratos.cuotas',
            'contratos.plan_detalles_id',
            'frecuencia_pagos.frecuencia',
            'frecuencia_pagos.dias',
            'contratos.total_pagado',
            'contratos.total_contrato',
            'contratos.anulado',
            'personas_bancos.cuenta',
            'bancos.nombre as banco',
            'estatus_contrato.nombre as estatus_contrato',
            'sucursal.nombre as sucursal',
            'personas.nombres as vendedor' 

        )
        ->leftJoin('vendedores', 'vendedores.id','=', 'contratos.vendedor_id')
        ->leftJoin('personas', 'personas.id','=','vendedores.personas_id')
        ->leftJoin('personas_bancos', 'personas_bancos.id','=', 'contratos.personas_bancos_id')
        ->leftJoin('bancos', 'bancos.id','=', 'personas_bancos.bancos_id')
        ->leftJoin('frecuencia_pagos', 'frecuencia_pagos.id','=', 'contratos.frecuencia_pagos_id')
        ->leftJoin('estatus_contrato','estatus_contrato.id', '=', 'contratos.estatus_contrato_id')      
        ->leftJoin('sucursal','sucursal.id', '=', 'contratos.sucursal_id')        
        ->where('contratos.id', $id)->first();

        $Contratos->vencimiento  =  Carbon::parse($Contratos->vencimiento)->format('d/m/Y');
        
        if($Contratos->anulado){
            $Contratos->estatus_contrato = 'Contrato Anulado';
        }


        return $Contratos;
    }

}