<?php

namespace Modules\Cobranza\Http\Controllers;

//Controlador Padre
use Modules\Cobranza\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;
use Session;
use Carbon\Carbon;
//Request
use Modules\Cobranza\Http\Requests\ContratosRequest;

//Modelos
use Modules\Base\Model\TipoPersona;
use Modules\Base\Model\Personas;
use Modules\Base\Model\PersonasBancos;
use Modules\Base\Model\PersonasDetalles;
use Modules\Base\Model\Solicitudes;
use Modules\Base\Model\PersonasDireccion;
use Modules\Base\Model\PersonasTelefono;
use Modules\Cobranza\Model\Cobros;
use Modules\Cobranza\Model\Contratos;
use Modules\Cobranza\Model\Beneficiarios;
use Modules\Cobranza\Model\Parentesco;
use Modules\Cobranza\Model\FrecuenciaPagos;
use Modules\Cobranza\Model\ContratosDetalles;
use Modules\Cobranza\Model\ContratosFacturar;
use Modules\Empresa\Model\Sucursal;
use Modules\Ventas\Model\VendedoresSucusal;
use Modules\Ventas\Model\Vendedores;
use Modules\Ventas\Model\Plan;
use Modules\Ventas\Model\PlanDetalles;
use Auth;

class AnalistaController extends Controller
{
    protected $titulo = '';

    public $js = [
    ];
    
    public $css = [
    ];

    public $numero = [
            '0',
            '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9',
            '10'
    ];

    public $librerias = [
        'datatables',
        'maskedinput',
        'alphanum',
        'template',
        'jquery-ui',
        'jquery-ui-timepicker',
        'bootstrap-sweetalert',
        'maskMoney'

        
    ];

    public $contrato_id;

    public function index()
    {
        
        $this->js=[
              'Contratosanalista',
              'jquery.validate.min.js',
              'additional-methods.min.js',
              'moment.min.js',
              //'bootstrap-wizard/jquery.bootstrap.wizard.min.js'
              'select2.full.min.js'
        ];

        $this->css=[
           'select2.min.css',
           'select2-bootstrap.min.css',
           'Contratoanalista'
        ];


        $tipo = "nuevo";

        $this->titulo= "Contratos";

        return $this->view('cobranza::Contratoanalista',[
            'Personas' => new Personas(),
            'tipo'=>$tipo
        ]);
    }
    
    public function refinaciamiento(Request $request, $id = 0 ){
        $this->titulo = 'Refinanciamiento de contrato';
        $this->js=[
             '',     
        ];

        return $this->view('cobranza::popup-refinanciamiento', [
            'layouts'          => 'base::layouts.popup',
            'contrato_id'      => $id
        ]);
    }

    public function refinaciamientoguardar(Request $request)
    {
        DB::beginTransaction();
        try{
           
            Cobros::where('contratos_id', $request->id)->where('completo', false)->delete();

            $Contratos = Contratos::find($request->id);
            
            $datos = [];
            $datos['fecha_pago'] = $request->fecha;
            if($Contratos->estatus_contrato_id == 6){
                 $datos['estatus_contrato_id'] = 1;
            }
            if($Contratos->estatus_contrato_id == 6){
                 $datos['estatus_contrato_id'] = 1;
            }
            
            $Contratos->fill($datos);
            $Contratos->save();

            ContratosDetalles::create([
               "contratos_id" => $Contratos->id,
               "personas_id"  => auth()->user()->personas->id,
               "operacion"    => "Refinanciamiento de Contrato por el analista: " .auth()->user()->personas->nombres,
               "planilla"     => 'S/n'
            ]);

        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    
    }
    public function cobros(Request $request, $id= 0)
    {
       
        $cobros= '';

        $this->js=[
             'cobros',
             
        ];

        $this->css=[
           ''
        ];

        $this->contrato_id = $id;

        return $this->view('cobranza::popup-cobros', [
            'layouts'          => 'base::layouts.popup-consulta',
            'cobros'           => $cobros,
            'contrato_id'      => $id
        ]);
    }


    public function historial(Request $request, $id=0)
    {
        $contratos_detalles = ContratosDetalles::select(
            'personas.dni',
            'tipo_persona.nombre',
            'personas.nombres',
            'contratos_detalles.operacion',
            'contratos_detalles.planilla',
            'contratos_detalles.created_at'

        )
        ->leftJoin('personas', 'personas.id','=','contratos_detalles.personas_id')
        ->leftJoin('tipo_persona', 'tipo_persona.id','=','personas.tipo_persona_id')
        ->orderBy( 'contratos_detalles.id', 'desc')
        ->where('contratos_id', $id )->get();

        return $this->view('cobranza::popup-historial', [
            'layouts'              => 'base::layouts.popup-consulta',
            'contratos_detalles'   => $contratos_detalles
        ]);
    }
  
    public function cobrostable(Request $request, $id = 0)
    {
       
        $sql = Cobros::select(
            'id',
            'total_cobrar',
            'created_at',
            'fecha_pagado',
            'total_pagado',
            'tipo_pago',
            'num_recibo',
            'concepto'
        )->where('contratos_id', $id);

      
       

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }

    public function buscar(Request $request, $id = 0)
    {
               
        $Contratos = Contratos::select([
           
            'contratos.id',
            'contratos.planilla',
            'contratos.titular',
            'contratos.renovaciones',
            'contratos.beneficiarios as Num_benefe',
            'personas.dni',
            'personas.nombres',
            'personas.tipo_persona_id',
            "contratos.vendedor_id",
            "contratos.sucursal_id",
            //DB::raw('DATE_FORMAT(contratos.inicio, "%d/%m/%Y") as fecha'),

            "contratos.inicio as fecha",
            "contratos.plan_detalles_id",
            "frecuencia_pagos.frecuencia",
            "contratos.frecuencia_pagos_id",
            "contratos.tipo_pago",
            "contratos.cobrando",
            "contratos.total_contrato",
            "contratos.cuotas",
            "contratos.personas_bancos_id",
            "contratos.inicial",
            "contratos.fecha_incial as fecha2",
            "contratos.anulado",
            "estatus_contrato.nombre as estatus"
        ])
        ->leftjoin('frecuencia_pagos', 'frecuencia_pagos.id','=','contratos.frecuencia_pagos_id')
        ->leftjoin('estatus_contrato', 'estatus_contrato.id','=','contratos.estatus_contrato_id')
        ->leftjoin('personas', 'personas.id','=','contratos.titular')
        ->where('contratos.id', $id)
        ->first();
        $cobros = '';

        if($Contratos->tipo_pago ==  1){
            $cobros = Cobros::where('contratos_id',  $id)->first();
        }
        
        $Contratos->fecha = Carbon::parse($Contratos->fecha)->format('d/m/Y');

        if($Contratos->fecha2 != ''){
            $Contratos->fecha2 = Carbon::parse($Contratos->fecha2)->format('d/m/Y');
        }

        $vendedor_id = VendedoresSucusal::select([
            'vendedores_sucusal.vendedor_id as id',
            'personas.nombres as nombre'
        ])
        ->leftjoin('vendedores', 'vendedores.id', '=', 'vendedores_sucusal.vendedor_id')
        ->leftjoin('personas', 'personas.id', '=', 'vendedores.personas_id')
        ->where(
            'vendedores_sucusal.sucursal_id', $Contratos->sucursal_id
        )
       ->pluck('nombre','id')
       ->put('_', $Contratos->vendedor_id);
       

        $Num_benefe =  Beneficiarios::where('contratos_id',$id)->where('personas_id', $Contratos->titular);

        if($Contratos->plan_detalles_id == ''){
           $PlanDetalles = '';
        }else{
            $PlanDetalles = PlanDetalles::select(
                'plan_detalles.plan_id',
                'plan_detalles.beneficiarios',
                'plan_detalles.porsertaje_descuento',
                'plan_detalles.contado',
                'plan_detalles.inicial',
                'plan_detalles.total',
                'plan.meses_pagar'
            )
            ->leftjoin('plan','plan.id','plan_detalles.plan_id')
            ->where('plan_detalles.id',$Contratos->plan_detalles_id)->first();
            
            $plan = Plan::where('id', $PlanDetalles->plan_id)
            ->pluck('nombre','id')
            ->put('_', $PlanDetalles->plan_id);  

        }
       
    
       if ($Contratos) {
            return array_merge($Contratos->toArray(), [
                'vendedor_id' => $vendedor_id,
                'goza'        => $Num_benefe->count(),
                'planDetalles'=> $PlanDetalles,
                'planes_id'   => $plan,
                'cobros'      => $cobros,
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);

        }

        return trans('controller.nobuscar');
    }

    public function guardar(ContratosRequest $request, $id = 0)
    {

        DB::beginTransaction();
        try{

            $Contratos = $id == 0 ? new Contratos() : Contratos::find($id);
            
            $datos = $request->all();

            $persona = Personas::where('dni', $datos['dni'])->first();

            $datos['titular'] = $persona->id;

            $datos['empresa_id'] = Session::get('empresa');
            $datos['cargado'] = date('Y-m-d');

            $date =Carbon::createFromFormat('d/m/Y', $datos['inicio'])->addYear();

            $datos['vencimiento'] = $date;

            $plan_detalles = PlanDetalles::where('plan_id',$datos['planes_id'])->where('beneficiarios', $request->Num_benefe)->first();

            $datos['plan_detalles_id']  = $plan_detalles->id;
            $datos['total_contrato']    = $plan_detalles->total;
            $datos['inicial']           = $plan_detalles->inicial;
            $datos['beneficiarios']     = $request->Num_benefe;

            $pago = 0;

            $vendedor =  $datos['vendedor_id'];
            
            if($request->tipo_pago == 1 ){
                $pago = $plan_detalles->contado;
                $datos['cuotas'] = 1;
                $datos['inicial'] = '0';
                $datos['personas_bancos_id'] = null;
                $datos['frecuencia_pagos_id'] = null;
                $datos['fecha_pago'] = null;
                $datos['cobrando']  = 1;

            }else{
                $plan = Plan::where('id',$request->planes_id )->first();
                if($request->frecuencia == 'm'){
                    $cuotas = 1 * $plan->meses_pagar ;
                }else{
                    $cuotas = 2 * $plan->meses_pagar; 
                }

                $datos['cuotas'] = $cuotas;
                $datos['fecha_pago'] =  $datos['fecha_incial'];
            }

            $datos['total_pagado']  = $pago; 
            $datos['estatus_contrato_id']  = 1;  
            $Contratos->fill($datos);
            $Contratos->save();
            
            $_tipo_pago = [
               0 => 'Por Tarjeta',
                'Transferencia',
                'efectivo'
            ];
            
            if($id == 0 ){
                if($request->tipo_pago == 1 ){
                    ContratosFacturar::create([
                        "fecha_facturar" => date('Y-m-d'),
                        "contratos_id"   => $Contratos->id
                    ]);

                    Cobros::create([
                        "contratos_id"  => $Contratos->id,
                        "sucursal_id"   => $datos['sucursal_id'],
                        "personas_id"   => $persona->id,
                        "total_cobrar"  => $pago,
                        "concepto"      => 'Pago de Contrato de Contado por: '.$_tipo_pago[$datos['pago']],
                        "cuota"         => 1,
                        "fecha_pagado"  => $request->fecha_decontado,
                        "total_pagado"  => $pago,
                        "num_recibo"    => $datos['n_recibo'],
                        "tipo_pago"     => 'caja',
                        "completo"      => true
                    ]);
                }
            }
            
            $_nombres = auth()->user()->personas->nombres;

            ContratosDetalles::create([
               "contratos_id" => $Contratos->id,
               "personas_id"  => auth()->user()->personas->id,
               "operacion"    => $id == 0 ? "Carga de Nuevo contrato Por Analista: " .$_nombres : "Contratos Modificado Por Analista: ".$_nombres,
               "planilla"     => $request->planilla
            ]);
            
            $this->beneficiarios($request->all(), $Contratos->id);

             /*
                Tipo solicitud 
                    1 = contrato nuevo
                    2 = renovacion contrato
                    3 = agregar beneficiarios
                    4 = modificacion de informacion personal

                Solicitante 
                    id_persona quien solicita.
                aquien 
                    1-contrato
                    2-persona
                
                indice 
                    persona_id o contrato_id

                Solicitudes::create([
                   "tipo_solicitud" => 1,
                   "solicitante"    => auth()->user()->personas->id,
                   "aquien"         => 1,
                   "indece"         => $Contratos->id
                ]);
            */

        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $Contratos->id,
            'texto' => $Contratos->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }
    
    public function beneficiarios($request, $id){
        DB::beginTransaction();
        try { 

            $datos = [];

            Beneficiarios::where('contratos_id', $id)->delete();
            
            foreach ($request['dni_beneficiario'] as $_id => $beneficiario) {
               
                if($request['dni_beneficiario'][$_id] === ""){

                    $persona = Personas::where('dni', 'LIKE',$request['dni'].'%')->count();
                    $result = $persona +  1;
                    $dni = $request['dni'] . "-" . $result;

                }else{
                    $dni = $request['dni_beneficiario'][$_id];
                }

                $persona = Personas::where('dni', $dni)->first();

                if(count($persona) == 0){
                   $persona = Personas::create([
                       "tipo_persona_id" => 1,
                       "dni"             => $dni,
                       "nombres"         => $request['nombres_beneficiario'][$_id]
                    ]);

                    PersonasDetalles::create([
                       "personas_id"      => $persona->id,
                       "sexo"             => $request['sexo_beneficiario'][$_id],
                       "fecha_nacimiento" => $request['nacimiento_beneficiario'][$_id]
                    ]);
                }


                $datos= [
                    "contratos_id"  => $id,
                    "personas_id"   => $persona->id,
                    "parentesco_id" => $request['parentescos_beneficioario'][$_id],
                    "estatus"       => 0
                ];
                
                Beneficiarios::create($datos);   
            }
   

        } catch (Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }

        DB::commit();

        return ['s' => 's', 'msj' => trans('controller.incluir')];
    }

    public function beneficiario(Request $request){

        $Beneficiarios = Beneficiarios::select(
            'personas.nombres','personas.dni',
            'beneficiarios.parentesco_id',
            'personas_detalles.fecha_nacimiento as fecha_nacimiento',
            'personas_detalles.sexo'
        )
        ->leftjoin('personas','personas.id','=','beneficiarios.personas_id')
        ->leftjoin('personas_detalles','personas_detalles.personas_id','=','beneficiarios.personas_id')
        ->where('beneficiarios.contratos_id',$request->id)->get();
    
        $datos = [];

        foreach($Beneficiarios as $Beneficiario){

            $datos[] = [
                'nombres' => $Beneficiario->nombres,
                'dni'=>$Beneficiario->dni,
                'parentesco_id'=>$Beneficiario->parentesco_id,
                'fecha_nacimiento'=> Carbon::parse($Beneficiario->fecha_nacimiento)->format('d/m/Y'),
                'sexo'=>$Beneficiario->sexo
            ];

        }
    
        return ['datos' =>$datos];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Contratos::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Contratos::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Contratos::select(
            'contratos.id as id',
            'contratos.planilla as planilla',
            'personas.nombres', 
            'personas.dni' 

        )
        
        ->join('personas', 'personas.id','=','contratos.titular');

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }

    public function vendedores(Request $request){

        $sql = VendedoresSucusal::join('vendedores', 'vendedores.id','=', 'vendedores_sucusal.vendedor_id')
            ->join('personas', 'personas.id','=','vendedores.personas_id')
            ->where('vendedores_sucusal.sucursal_id', $request->id)
            ->where('vendedores.estatus', 0)//estatus 0 = activo 
            ->pluck('personas.nombres','vendedores.id')
            ->toArray();

        $salida = ['s' => 'n' , 'msj'=> 'el sucursal no Contiene Vendedores Activos'];
        
        if($sql){
            $salida = ['s' => 's' , 'msj'=> 'Vendedores encontrados', 'vendedor_id'=> $sql];
        }               
        
        return $salida;
    } 

    public function sucursales()
    {

        /*
            $sqls = VendedoresSucusal::select('vendedores_sucusal.sucursal_id')
            ->join('vendedores', 'vendedores.id','=', 'vendedores_sucusal.vendedor_id')
            ->where('vendedores.estatus', 0)//estatus 0 = activo 
            ->where('vendedores.personas_id', auth()->user()->id)
            ->get();

            $sucursales= [];

            foreach ($sqls as $key => $sql) {
                 $sucursales[] = $sql->sucursal_id;
            }
        */
        /*if(auth()->user()->super === "s"){
        }*/
        
        return Sucursal::where('empresa_id', Session::get('empresa'))->pluck('nombre', 'id');
       // return Sucursal::whereIn('id', $sucursales)->pluck('nombre', 'id');
    }
   
    public function validar(Request $request){

        $persona = Personas::where('dni',$request->dni)->get();
        
        if($persona->count() == 0){
            return [
            's' => 'n'
            ];
        }   
        
        $_persona = $persona->toArray();
        $detalles = PersonasDetalles::where('personas_id',$_persona[0]['id'])->first();

        if(!$detalles){
            return [
                's'   => 'n'
            ];
        }
        
        $fecha_n =Carbon::createFromFormat('d/m/Y', $detalles->fecha_nacimiento);
        $date = Carbon::parse($fecha_n)->age;
        
        $detalles->nombre = $_persona[0]['nombres'];
        $detalles->dni = $_persona[0]['dni'];
        $detalles->edad = $date;
        $detalles->parentescos = 33;
        

        
        $cuentas = PersonasBancos::select('personas_bancos.id', 'personas_bancos.cuenta','bancos.nombre' )
            ->leftJoin('bancos', 'bancos.id','=','personas_bancos.bancos_id')
            ->where('personas_bancos.personas_id', $_persona[0]['id'])->get()->toArray();
            
        return [
            'persona'   => $_persona[0],
            'cuentas'   => $cuentas,
            'detalles'  => $detalles,
            's'         => 's', 
            'msj' => trans('controller.incluir')
        ];
    }


    public function parentescos(){
        return Parentesco::all();
    }
    
    public function tipo_persona(){
        return TipoPersona::pluck('nombre', 'id');
    }

    public function planes(Request $request){
       
        
        $fecha = Carbon::createFromFormat('d/m/Y',$request->dato)->format("Y-m-d");
        $planes = Plan::where('desde','<=',  $fecha)
        ->where('hasta','>=',  $fecha)->get();


        $salida = ['s' => 'n' , 'msj'=> 'NO Hay planes activos'];
        
        if(count($planes) > 0 ){
            $salida = ['s' => 's', 'planes_id'=> $planes];
        }               
        
        return $salida;
    }

    public function infoplan(Request $request){


        $plan = Plan::where('id',$request->plan_id )->where('empresa_id',Session::get('empresa'))->get();

        $plan_detalles = PlanDetalles::where('plan_id',$request->plan_id )->where('beneficiarios', $request->beneficiarios)->first();

        $plan_detalles->contado2 = number_format($plan_detalles->contado, 2, ',', '.');
        $plan_detalles->inicial2 = number_format($plan_detalles->inicial, 2, ',', '.');
        $plan_detalles->total2 = number_format($plan_detalles->total, 2, ',', '.');
       

     /*   "id" => 11
      "plan_id" => 3
      "beneficiarios" => 1
      "porsertaje_descuento" => "10.00"
      "contado" => "9000.00"
      "inicial" => "1000.00"
      "total" => "10000.00"
      "deleted_at" => null*/
        $frecuencia = FrecuenciaPagos::where('frecuencia', $request->frecuencia)->get();

      return['plan' => $plan, 'plan_detalles'=> $plan_detalles, 'frecuencia' => $frecuencia]; 

    }


}