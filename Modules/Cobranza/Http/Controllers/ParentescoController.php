<?php

namespace Modules\Cobranza\Http\Controllers;

//Controlador Padre
use Modules\Cobranza\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use Modules\Cobranza\Http\Requests\ParentescoRequest;

//Modelos
use Modules\Cobranza\Model\Parentesco;

class ParentescoController extends Controller
{
    protected $titulo = 'Parentesco';

    public $js = [
        'Parentesco'
    ];
    
    public $css = [
        'Parentesco'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('cobranza::Parentesco', [
            'Parentesco' => new Parentesco()
        ]);
    }

    public function nuevo()
    {
        $Parentesco = new Parentesco();
        return $this->view('cobranza::Parentesco', [
            'layouts' => 'base::layouts.popup',
            'Parentesco' => $Parentesco
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Parentesco = Parentesco::find($id);
        return $this->view('cobranza::Parentesco', [
            'layouts' => 'base::layouts.popup',
            'Parentesco' => $Parentesco
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Parentesco = Parentesco::withTrashed()->find($id);
        } else {
            $Parentesco = Parentesco::find($id);
        }

        if ($Parentesco) {
            return array_merge($Parentesco->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(ParentescoRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $Parentesco = $id == 0 ? new Parentesco() : Parentesco::find($id);

            $Parentesco->fill($request->all());
            $Parentesco->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $Parentesco->id,
            'texto' => $Parentesco->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Parentesco::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Parentesco::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Parentesco::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Parentesco::select([
            'id', 'nombre', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}