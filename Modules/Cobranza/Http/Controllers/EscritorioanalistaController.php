<?php

namespace Modules\Cobranza\Http\Controllers;

use Modules\Cobranza\Http\Controllers\Controller;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;

use Modules\Base\Model\TipoPersona;
use Modules\Base\Model\Personas;
use Modules\Base\Model\PersonasBancos;
use Modules\Base\Model\PersonasDetalles;
use Modules\Base\Model\Solicitudes;

use Modules\Cobranza\Model\Contratos;
use Modules\Cobranza\Model\Beneficiarios;
use Modules\Cobranza\Model\Parentesco;
use Modules\Cobranza\Model\FrecuenciaPagos;
use Modules\Cobranza\Model\ContratosDetalles;
use Modules\Empresa\Model\Sucursal;
use Modules\Ventas\Model\VendedoresSucusal;
use Modules\Ventas\Model\Vendedores;
use Modules\Ventas\Model\Plan;
use Modules\Ventas\Model\PlanDetalles;
use Auth;
use Carbon\Carbon;

class EscritorioanalistaController extends Controller {


	protected $titulo = 'Escritorio Analista';

	public $librerias = [
        'datatables',
        'bootstrap-sweetalert'
    ];

	public $js = [
        'escritorioanalista'
    ];
    
    public $css = [
         'escritorioanalista'
    ];

	public function index(){

			return $this->view('cobranza::EscritorioAnalista');
	}

	public function solicitudes()
    {
    	$tipo_solicitud=[
    		1=>'contrato nuevo',
    		'renovacion contrato',
    		'agregar  o eliminnar beneficiarios',
    		'modificacion de informacion personal'
    	];
        
	    /*
		    Tipo solicitud 
		        1 = contrato nuevo
		        2 = renovacion contrato
		        3 = agregar beneficiarios
		        4 = modificacion de informacion personal

		    Solicitante 
		        id_persona quien solicita.
		    aquien 
		        1-contrato
		        2- perona
		    
		    indice 
		        persona_id o contrato_id
	    */
        $_solicitudes = Solicitudes::all();
    	$solicitudes = [];

        foreach ($_solicitudes as $key => $solicitud) {

        	$persona = Personas::where('id',$solicitud->solicitante )->first();

        	$solicitudes[] = [
        		'id'                => $solicitud->id,
        		'Tipo_solicitud'	=> $tipo_solicitud[$solicitud->tipo_solicitud],
        		'solicitante'		=> $persona->nombres,
        		'tipo' 				=> $solicitud->aquien,
        		'indice'			=> $solicitud->indece,
                'fecha'             => Carbon::parse($solicitud->created_at)->format('d/m/Y'),
                'tipo22'            => $solicitud->tipo_solicitud
        	];
        }
        
    	return $solicitudes;
    }
}