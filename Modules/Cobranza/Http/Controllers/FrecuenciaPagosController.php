<?php

namespace Modules\Cobranza\Http\Controllers;

//Controlador Padre
use Modules\Cobranza\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use Modules\Cobranza\Http\Requests\FrecuenciaPagosRequest;

//Modelos
use Modules\Cobranza\Model\FrecuenciaPagos;

class FrecuenciaPagosController extends Controller
{
    protected $titulo = 'Frecuencia Pagos';

    public $js = [
        'FrecuenciaPagos'
    ];
    
    public $css = [
        'FrecuenciaPagos'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('cobranza::FrecuenciaPagos', [
            'FrecuenciaPagos' => new FrecuenciaPagos()
        ]);
    }

    public function nuevo()
    {
        $FrecuenciaPagos = new FrecuenciaPagos();
        return $this->view('cobranza::FrecuenciaPagos', [
            'layouts' => 'base::layouts.popup',
            'FrecuenciaPagos' => $FrecuenciaPagos
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $FrecuenciaPagos = FrecuenciaPagos::find($id);
        return $this->view('cobranza::FrecuenciaPagos', [
            'layouts' => 'base::layouts.popup',
            'FrecuenciaPagos' => $FrecuenciaPagos
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $FrecuenciaPagos = FrecuenciaPagos::withTrashed()->find($id);
        } else {
            $FrecuenciaPagos = FrecuenciaPagos::find($id);
        }

        if ($FrecuenciaPagos) {
            return array_merge($FrecuenciaPagos->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(FrecuenciaPagosRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $FrecuenciaPagos = $id == 0 ? new FrecuenciaPagos() : FrecuenciaPagos::find($id);

            $FrecuenciaPagos->fill($request->all());
            $FrecuenciaPagos->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $FrecuenciaPagos->id,
            'texto' => $FrecuenciaPagos->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            FrecuenciaPagos::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            FrecuenciaPagos::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            FrecuenciaPagos::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = FrecuenciaPagos::select([
            'id', 'frecuencia', 'dias', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}