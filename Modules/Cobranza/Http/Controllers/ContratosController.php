<?php

namespace Modules\Cobranza\Http\Controllers;

//Controlador Padre
use Modules\Cobranza\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;
use Session;
use Carbon\Carbon;
//Request
use Modules\Cobranza\Http\Requests\ContratosRequest;

//Modelos
use Modules\Base\Model\TipoPersona;
use Modules\Base\Model\Personas;
use Modules\Base\Model\PersonasBancos;
use Modules\Base\Model\PersonasDetalles;
use Modules\Base\Model\Solicitudes;
use Modules\Base\Model\Bancos;

use Modules\Cobranza\Model\Contratos;
use Modules\Cobranza\Model\Beneficiarios;
use Modules\Cobranza\Model\Parentesco;
use Modules\Cobranza\Model\FrecuenciaPagos;
use Modules\Cobranza\Model\ContratosDetalles;
use Modules\Cobranza\Model\ContratosTemp;
use Modules\Cobranza\Model\Cobros;
use Modules\Empresa\Model\Sucursal;
use Modules\Ventas\Model\VendedoresSucusal;
use Modules\Ventas\Model\Vendedores;
use Modules\Ventas\Model\Plan;
use Modules\Ventas\Model\PlanDetalles;
use Auth;
use Modules\Cobranza\Model\ContratosFacturar;
class ContratosController extends Controller
{
    protected $titulo = '';

    public $js = [
        'moment.min.js'
    ];
    
    public $css = [
    ];

    public $numero = [
        1 =>'1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9',
            '10'
    ];

    public $_tipo_pago = [
       0 => 'Por Tarjeta',
        'Transferencia',
        'efectivo'

    ];

    public $librerias = [
    ];

    public function index()
    {

        $this->librerias = [
            'datatables',
            'maskedinput',
            'alphanum',
            'template',
            'jquery-ui',
            'jquery-ui-timepicker',
            'bootstrap-wizard',
            'maskMoney'
        ];

        $this->js=[
            'Contratos',
            'jquery.validate.min.js',
            'additional-methods.min.js',
            //'bootstrap-wizard/jquery.bootstrap.wizard.min.js'
            'select2.full.min.js'
        ];

        $this->css=[
            'Contratos',
            'select2.min.css',
            'select2-bootstrap.min.css'
        ];


        $tipo = "nuevo";

        $this->titulo= "Nuevo Contratos";

        return $this->view('cobranza::Contratos',[
            'Personas' => new Personas(),
            'tipo'=>$tipo
        ]);
    }

    public function consulta(Request $request, $id = 0, $opera = 0, $solicitud_id = 0)
    {      

        $this->librerias = [
            'bootstrap-sweetalert',
            'datatables',
            'maskMoney'
        ];
       
       

        $this->css=[
            ''
        ];


        //informacion del contrato actual
        $Contratos = $this->contratoactual($id);
           
        $persona = Personas::select('*')->where('id',$Contratos->titular)->first();

    
        $beneficiario = 0;
        $plan = 'Personalizado';

        if($Contratos->plan_detalles_id  != ''){

            $plan_detalles = PlanDetalles::where('id',$Contratos->plan_detalles_id )->first();
            $plan          = Plan::where('id',$plan_detalles->plan_id )->first();
            $beneficiario  = $plan_detalles->beneficiarios;
            $plan          = $plan->nombre;
        }else{
           $beneficiario =  Beneficiarios::where('contratos_id', $id )->where('estatus',0)->count(); 
        }

        $_beneficiarios = Beneficiarios::select(
            'personas.dni',
            'tipo_persona.nombre',
            'personas.nombres',
            'parentesco.nombre as parentesco',
            'personas_detalles.sexo',
            'personas_detalles.fecha_nacimiento'
        )
        ->leftJoin('personas', 'personas.id','=','beneficiarios.personas_id')
        ->leftJoin('personas_detalles', 'personas_detalles.personas_id','=','personas.id')
        ->leftJoin('tipo_persona', 'tipo_persona.id','=','personas.tipo_persona_id')
        ->leftJoin('parentesco', 'parentesco.id','=','beneficiarios.parentesco_id')
        ->where('contratos_id', $id )->where('estatus',0)->get();
    
        $contratos_detalles = ContratosDetalles::select(
            'personas.dni',
            'tipo_persona.nombre',
            'personas.nombres',
            'contratos_detalles.operacion',
            'contratos_detalles.planilla',
            'contratos_detalles.created_at'

        )
        ->leftJoin('personas', 'personas.id','=','contratos_detalles.personas_id')
        ->leftJoin('tipo_persona', 'tipo_persona.id','=','personas.tipo_persona_id')
        ->orderBy( 'contratos_detalles.id', 'desc')
        ->where('contratos_id', $id )->get();


        $solicitud = Solicitudes::find($solicitud_id);
        // Solicitud de renovacion de contrato 
       
       
       if($solicitud){
            switch ($solicitud->tipo_solicitud) {
            case '2':
                $contrato_renovar = ContratosTemp::where('solicitud_id', $solicitud_id)->first();

                $contrato_renovar->vencimiento  =  Carbon::parse($contrato_renovar->vencimiento)->format('d/m/Y');

                $beneficiario2 = 0;
                $plan2 = 'Personalizado';

                if($contrato_renovar->plan_detalles_id  != '')
                {

                    $plan_detalles2 = PlanDetalles::where('id',$contrato_renovar->plan_detalles_id )->first();
                    $plan2          = Plan::where('id',$plan_detalles2->plan_id )->first();
                    $beneficiario   = $plan_detalles->beneficiarios;
                    $plan2          = $plan2->nombre;

                } else
                {
                   $beneficiario2   =  Beneficiarios::where('contratos_id', $id )->where('estatus',0)->count(); 
                }

                $frecuencia = FrecuenciaPagos::where('id', $contrato_renovar->frecuencia_pagos_id)->first();
                
                  $cuenta = "";
                  $Bancos = "";

                if($contrato_renovar->personas_bancos_id != ''){
                     $cuenta = PersonasBancos::find($contrato_renovar->personas_bancos_id);
                    $Bancos = Bancos::where('id', $cuenta->bancos_id)->first();
                }
               

                $this->js=[
                    'popup-renovacion',
                ];

                

                return $this->view('cobranza::Contratos-renovacion', [
                    'layouts'           => 'base::layouts.popup-consulta',
                    'Contrato'          => $Contratos,
                    'Persona'           => $persona,
                    'plan'              => $plan,
                    'beneficiario'      => $beneficiario,
                    'plan2'             => $plan2,
                    'beneficiario2'     => $beneficiario2,
                    'contrato_renovar'  => $contrato_renovar,
                    'contratos_detalles'=> $contratos_detalles,
                    'tipo'              => $opera,
                    'solicitud_id'      => $solicitud_id,
                    'frecuencia22'      => $frecuencia,
                    'per_bancos'        => $cuenta,
                    'banco'             => $Bancos
                 ]);
                break;
            }
        }
    
        $this->js=[
            'popup-consulta'
             
        ];

        return $this->view('cobranza::Contratos-consulta', [
            'layouts'           => 'base::layouts.popup-consulta',
            'Contrato'          => $Contratos,
            'Persona'           => $persona,
            'plan'              => $plan,
            'beneficiario'      => $beneficiario,
            'info_beneficiarios'=> $_beneficiarios,
            'contratos_detalles'=> $contratos_detalles,
            'tipo'              => $opera,
            'solicitud_id'      => $solicitud_id
        ]);
    }
 

    public function guardar(ContratosRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{

           $Contratos = $id == 0 ? new Contratos() : Contratos::find($id);

            $datos = $request->all();

            $persona = Personas::where('dni', $datos['dni'])->first();

            $datos['titular'] = $persona->id;

            $datos['empresa_id'] = Session::get('empresa');
            $datos['cargado'] = date('Y-m-d');

            $date =Carbon::createFromFormat('d/m/Y', $datos['inicio'])->addYear();

            $datos['vencimiento'] = $date;

            $plan_detalles = PlanDetalles::where('plan_id',$datos['planes_id'])->where('beneficiarios', $request->Num_benefe)->first();

            $datos['plan_detalles_id']  = $plan_detalles->id;
            $datos['total_contrato']    = $plan_detalles->total;
            $datos['inicial']           = $plan_detalles->inicial;
            $datos['beneficiarios']     = $request->Num_benefe;

            $pago = 0;

            if(auth()->user()->super === 'n'){
                 
                 $vende = Vendedores::select('id')
                ->where('vendedores.personas_id', auth()->user()->id)
                ->first();

                $datos['vendedor_id'] = $vende->id;

                $vendedor = $vende->id;

            }else{
                 $vendedor =  $datos['vendedor_id'];
            }
           
            if($request->tipo_pago == 1 ){
                $pago = $plan_detalles->contado;

                $datos['total_contrato'] = $pago;
               
                $datos['cuotas'] = 1;
                $datos['inicial'] = '0';
                $datos['personas_bancos_id'] = null;
                $datos['frecuencia_pagos_id'] = null;
                $datos['primer_cobro'] = datos['fecha_decontado'];
                $datos['cobrando']  = 1;
                $datos['fecha_pago'] = null;

            }else{
                $plan = Plan::where('id',$request->planes_id )->first();
                if($request->frecuencia == 'm'){

                    $cuotas = 1 * $plan->meses_pagar ;
                }else{

                    $cuotas = 2 * $plan->meses_pagar; 
                }

                $datos['cuotas'] = $cuotas;
                $datos['fecha_pago'] =  $datos['fecha_incial'];
            }

            $datos['total_pagado']  = $pago; 
            $datos['estatus_contrato_id']  = 2;  
            
            $Contratos->fill($datos);
            $Contratos->save();
            
            $_tipo_pago = [
               0 => 'Por Tarjeta',
                'Transferencia',
                'efectivo'
            ];

            if($request->tipo_pago == 1 ){

                ContratosFacturar::create([
                    "fecha_facturar" => date('Y-m-d'),
                    "contratos_id"   => $Contratos->id
                ]);
                Cobros::create([
                    "contratos_id"  => $Contratos->id,
                    "sucursal_id"   => $datos['sucursal_id'],
                    "personas_id"   => $persona->id,
                    "total_cobrar"  => $pago,
                    "concepto"      => 'Pago de Contrato de Contado por: '.$_tipo_pago[$datos['pago']],
                    "cuota"         => 1,
                    "fecha_pagado"  => $request->fecha_decontado,
                    "total_pagado"  => $pago,
                    "num_recibo"    => $datos['n_recibo'],
                    "tipo_pago"     => 'caja',
                    "completo"      => true
                ]);

            }

            ContratosDetalles::create([
               "contratos_id" => $Contratos->id,
               "personas_id"  => $vendedor,
               "operacion"    => "Carga de Nuevo contrato",
               "planilla"     => $request->planilla
            ]);
            

            $this->beneficiarios($request->all(), $Contratos->id);

             /*
                Tipo solicitud 
                    1 = contrato nuevo
                    2 = renovacion contrato
                    3 = agregar beneficiarios
                    4 = modificacion de informacion personal

                Solicitante 
                    id_persona quien solicita.
                aquien 
                    1-contrato
                    2-persona
                
                indice 
                    persona_id o contrato_id
            */

            Solicitudes::create([
               "tipo_solicitud" => 1,
               "solicitante"    => auth()->user()->personas->id,
               "aquien"         => 1,
               "indece"         => $Contratos->id
            ]);


        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $Contratos->id,  
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }
    public function beneficiarios($request, $id){
        DB::beginTransaction();
        try { 

            $datos = [];
        
            foreach ($request['dni_beneficiario'] as $_id => $beneficiario) {
               
                if($request['dni_beneficiario'][$_id] === ""){

                    $persona = Personas::where('dni', 'LIKE',$request['dni'].'%')->count();
                    $result = $persona +  1;
                    $dni = $request['dni'] . "-" . $result;

                }else{

                    $dni = $request['dni_beneficiario'][$_id];
                }

                $persona = Personas::where('dni', $dni)->first();

                if(count($persona) == 0){
                   $persona = Personas::create([
                       "tipo_persona_id" => 1,
                       "dni"             => $dni,
                       "nombres"         => $request['nombres_beneficiario'][$_id]
                    ]);

                    PersonasDetalles::create([
                       "personas_id"      => $persona->id,
                       "sexo"             => $request['sexo_beneficiario'][$_id],
                       "fecha_nacimiento" => $request['nacimiento_beneficiario'][$_id]
                    ]);
                }


                $datos= [
                    "contratos_id"  => $id,
                    "personas_id"   => $persona->id,
                    "parentesco_id" => $request['parentescos_beneficioario'][$_id],
                    "estatus"       => 0
                ];
                
                Beneficiarios::create($datos);   
            }
   

        } catch (Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }

        DB::commit();

        return ['s' => 's', 'msj' => trans('controller.incluir')];
    }


    public function confirmacion(Request $request){
        DB::beginTransaction();
        try{

      
            $contrato =  Contratos::find($request->id);
            $planilla = $contrato->planilla;
            $confi = $request->confirmacion;
            $m = false;
            switch ($confi) {

                case '1':
                    $descripcion  = 'Contrato Aceptado';
                    $confirmacion = 1;
                    $m = true;

                    break;

                case '2':
                    $descripcion = 'Contrato Rechazado Por: '.$request->descripcion;
                    $confirmacion = 4;
                    $m = true;
                    break;  


                case '3': //renovacion     
                    //Crear decripcion para el contrato detalle
                    $descripcion = "Resumen Contrato anterior, planilla: ". $contrato->planilla . ", fecha de contrato: ".$contrato->inicio. ", fecha vencimiento: ".  $contrato->vencimiento. ", total del contrato: ".$contrato->total_contrato.", total pagado: ". $contrato->total_pagado;

                    ContratosDetalles::create([
                       "contratos_id" => $request['id'],
                       "personas_id"  => auth()->user()->personas_id,
                       "operacion"    => $descripcion,
                       "planilla"     => 's/n'
                    ]);

                    $descripcion  = 'Renovacion de Contrato Aceptado';
                    $confirmacion = 1;

                    $datos = ContratosTemp::where('solicitud_id', $request->solicitud_id)->first()->toArray();
                    

                    $datos['renovaciones'] = $contrato->renovaciones + 1;

                    if($datos['tipo_pago'] == 1 ){
                       
                        $datos['inicial'] = '0';
                        $datos['personas_bancos_id'] = null;
                        $datos['frecuencia_pagos_id'] = null;
                        $datos['fecha_incial'] = null;

                    }else{
                        $datos['primer_cobro'] = '';
                    }

                    $datos['estatus_contrato_id']  = 1; 

                    $datos['renovaciones']  = $contrato->renovaciones + 1;
           
            
                    $datos['cuotas_pagadas']  = null;  
                    $datos['anulado']  = 0;  

                    $contrato->fill($datos);
                    $contrato->save();
                    $planilla = $contrato->planilla;
                    
                    ContratosTemp::where('solicitud_id', $request->solicitud_id)->delete();
                    break;
                
                case '4':
            
                    $descripcion = 'Renovacion de Contrato Rechazada Por: '.$request->descripcion;
                    $confirmacion = 4;

                    $datos = ContratosTemp::where('solicitud_id', $request->solicitud_id)->first();

                    $datos->update([
                        'rechazado' => true
                    ]);
                    $planilla =  $datos->planilla;
                    break;

                default:
                    # code...
                    break;
            }
            
            if($m){
                $contrato->update([
                    'estatus_contrato_id' => $confirmacion
                ]);
            }
        
        
            ContratosDetalles::create([
               "contratos_id" => $contrato->id,
               "personas_id"  => Auth()->user()->personas->id,
               "operacion"    => $descripcion,
               "planilla"     => $planilla
            ]);

            Solicitudes::where('id', $request->solicitud_id)->delete();
            

        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return ['s' => 's'];

    
    }

    public function vendedores(Request $request){

        $sql = VendedoresSucusal::join('vendedores', 'vendedores.id','=', 'vendedores_sucusal.vendedor_id')
            ->leftJoin('personas', 'personas.id','=','vendedores.personas_id')
            ->where('vendedores_sucusal.sucursal_id', $request->id)
            ->where('vendedores.estatus', 0)//estatus 0 = activo 
            ->pluck('personas.nombres','vendedores.id')
            ->toArray();

        $salida = ['s' => 'n' , 'msj'=> 'el sucursal no Contiene Vendedores Activos'];
        
        if($sql){
            $salida = ['s' => 's' , 'msj'=> 'Vendedores encontrados', 'vendedor_id'=> $sql];
        }               
        
        return $salida;
    } 

    public function sucursales()
    {

        $sqls = VendedoresSucusal::select('vendedores_sucusal.sucursal_id')
            ->leftJoin('vendedores', 'vendedores.id','=', 'vendedores_sucusal.vendedor_id')
            ->where('vendedores.estatus', 0)//estatus 0 = activo 
            ->where('vendedores.personas_id', auth()->user()->id)
            ->get();

            $sucursales= [];

            foreach ($sqls as $key => $sql) {
                 $sucursales[] = $sql->sucursal_id;
            }

        if(auth()->user()->super === "s"){
            return Sucursal::where('empresa_id', Session::get('empresa'))->pluck('nombre', 'id');
        }
        return Sucursal::whereIn('id', $sucursales)->pluck('nombre', 'id');
    }
   
    public function validar(Request $request){

        $persona = Personas::where('dni',$request->dni)->get();
        
        if($persona->count() == 0){
            return [
            's' => 'n'
            ];
        }   
        
        $detalles ='';

        $_persona = $persona->toArray();

        if($_persona[0]['tipo_persona_id'] == 1 or $_persona[0]['tipo_persona_id'] == 2){
            $detalles = PersonasDetalles::where('personas_id',$_persona[0]['id'])->first();
            $fecha_n =Carbon::createFromFormat('d/m/Y', $detalles->fecha_nacimiento);
            $date = Carbon::parse($fecha_n)->age;
            
            $detalles->nombre = $_persona[0]['nombres'];
            $detalles->dni = $_persona[0]['dni'];
            $detalles->edad = $date;
            $detalles->parentescos = 33;
        }
       
        $cuentas = PersonasBancos::select('personas_bancos.id', 'personas_bancos.cuenta','bancos.nombre' )
            ->leftJoin('bancos', 'bancos.id','=','personas_bancos.bancos_id')
            ->where('personas_bancos.personas_id',$_persona[0]['id'])->get();

        return [
            'persona' => $_persona[0],
            'cuentas'=> $cuentas,
            'detalles'  => $detalles,
            's' => 's', 
            'msj' => trans('controller.incluir')
        ];
    }

    public function parentescos(){
        return Parentesco::all();
    }
    
    public function tipo_persona(){
        return TipoPersona::pluck('nombre', 'id');
    }

    public function planes(Request $request){
        //dd('hola');
        
        $fecha = Carbon::createFromFormat('d/m/Y',$request->dato)->format("Y-m-d");
        $planes = Plan::where('desde','<=',  $fecha)
        ->where('hasta','>=',  $fecha)->get();


        $salida = ['s' => 'n' , 'msj'=> 'NO Hay planes activos'];
        
        if(count($planes) > 0 ){
            $salida = ['s' => 's', 'planes_id'=> $planes];
        }               
        
        return $salida;
    }

    public function infoplan(Request $request){


        $plan = Plan::where('id',$request->plan_id )->where('empresa_id',Session::get('empresa'))->get();

        $plan_detalles = PlanDetalles::where('plan_id',$request->plan_id )->where('beneficiarios', $request->beneficiarios)->first();


        $plan_detalles->contado2 = number_format($plan_detalles->contado, 2, ',', '.');
        $plan_detalles->inicial2 = number_format($plan_detalles->inicial, 2, ',', '.');
        $plan_detalles->total2 = number_format($plan_detalles->total, 2, ',', '.');

        $frecuencia = FrecuenciaPagos::where('frecuencia', $request->frecuencia)->get();

      return['plan' => $plan, 'plan_detalles'=> $plan_detalles, 'frecuencia' => $frecuencia]; 

    }

    public function contratoactual($id){

        $Contratos = Contratos::select(
            'contratos.id',
            'contratos.planilla',
            'contratos.titular',
            'contratos.vencimiento',
            'contratos.inicio',
            'contratos.cargado',
            'contratos.primer_cobro',
            'contratos.fecha_incial',
            'contratos.inicial',
            'contratos.renovaciones',
            'contratos.tipo_pago',
            'contratos.cuotas',
            'contratos.plan_detalles_id',
            'frecuencia_pagos.frecuencia',
            'frecuencia_pagos.dias',
            'contratos.total_pagado',
            'contratos.total_contrato',
            'contratos.anulado',
            'personas_bancos.cuenta',
            'bancos.nombre as banco',
            'estatus_contrato.nombre as estatus_contrato',
            'sucursal.nombre as sucursal',
            'personas.nombres as vendedor' 

        )
        ->leftJoin('vendedores', 'vendedores.id','=', 'contratos.vendedor_id')
        ->leftJoin('personas', 'personas.id','=','vendedores.personas_id')
        ->leftJoin('personas_bancos', 'personas_bancos.id','=', 'contratos.personas_bancos_id')
        ->leftJoin('bancos', 'bancos.id','=', 'personas_bancos.bancos_id')
        ->leftJoin('frecuencia_pagos', 'frecuencia_pagos.id','=', 'contratos.frecuencia_pagos_id')
        ->leftJoin('estatus_contrato','estatus_contrato.id', '=', 'contratos.estatus_contrato_id')      
        ->leftJoin('sucursal','sucursal.id', '=', 'contratos.sucursal_id')        
        ->where('contratos.id', $id)->first();

        $Contratos->vencimiento  =  Carbon::parse($Contratos->vencimiento)->format('d/m/Y');
        
        if($Contratos->anulado){
            $Contratos->estatus_contrato = 'Contrato Anulado';
        }


        return $Contratos;
    }

}