<?php namespace Modules\Cobranza\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController {

	public $app = 'cobranza';
	
	protected $patch_js = [
		'public/js',
		'public/plugins',
		'Modules/Cobranza/Assets/js',
	];

	protected $patch_css = [
		'public/css',
		'public/plugins',
		'Modules/Cobranza/Assets/css',
	];
}