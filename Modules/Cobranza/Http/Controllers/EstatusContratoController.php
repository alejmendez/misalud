<?php

namespace Modules\Cobranza\Http\Controllers;

//Controlador Padre
use Modules\Cobranza\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use Modules\Cobranza\Http\Requests\EstatusContratoRequest;

//Modelos
use Modules\Cobranza\Model\EstatusContrato;

class EstatusContratoController extends Controller
{
    protected $titulo = 'Estatus Contrato';

    public $js = [
        'EstatusContrato'
    ];
    
    public $css = [
        'EstatusContrato'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('cobranza::EstatusContrato', [
            'EstatusContrato' => new EstatusContrato()
        ]);
    }

    public function nuevo()
    {
        $EstatusContrato = new EstatusContrato();
        return $this->view('cobranza::EstatusContrato', [
            'layouts' => 'base::layouts.popup',
            'EstatusContrato' => $EstatusContrato
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $EstatusContrato = EstatusContrato::find($id);
        return $this->view('cobranza::EstatusContrato', [
            'layouts' => 'base::layouts.popup',
            'EstatusContrato' => $EstatusContrato
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $EstatusContrato = EstatusContrato::withTrashed()->find($id);
        } else {
            $EstatusContrato = EstatusContrato::find($id);
        }

        if ($EstatusContrato) {
            return array_merge($EstatusContrato->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(EstatusContratoRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $EstatusContrato = $id == 0 ? new EstatusContrato() : EstatusContrato::find($id);

            $EstatusContrato->fill($request->all());
            $EstatusContrato->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $EstatusContrato->id,
            'texto' => $EstatusContrato->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            EstatusContrato::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            EstatusContrato::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            EstatusContrato::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = EstatusContrato::select([
            'id', 'nombre', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}