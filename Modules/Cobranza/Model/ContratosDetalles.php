<?php
namespace Modules\Cobranza\Model;

use Modules\Base\Model\Modelo;

class ContratosDetalles extends modelo
{
	protected $table = 'contratos_detalles';
    protected $fillable = ["contratos_id","personas_id","operacion","planilla"];

    protected $hidden = [ 'updated_at'];

}