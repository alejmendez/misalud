<?php
namespace Modules\Cobranza\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class ContratosTemp extends Model
{
	protected $table = 'contratos_temp';
    protected $fillable = [
        "planilla",
        "contrato_id",
        "primer_cobro",
        'solicitud_id',
        "vencimiento",
        "inicio",
        "plan_detalles_id",
        "frecuencia_pagos_id",
        "tipo_pago",
        "total_contrato",
        "total_pagado",
        "cuotas",
        "cuotas_pagadas",
        "personas_bancos_id",
        "beneficiarios",
        "inicial",
        "fecha_incial",
        "fecha_decontado",
        "n_recibo",
    	"rechazado",
    	"pago"
    ];

      public function setCargadoAttribute($value)
    {
        // 2016-06-27
       
        $formato = 'd/m/Y';
        if (preg_match('/^\d{4}-\d{1,2}-\d{1,2}$/', $value)){
            $formato = 'Y-m-d';
        }
        
        $this->attributes['cargado'] = Carbon::createFromFormat($formato, $value);
    }
    
    public function getCargadoAttribute($value){
        return Carbon::parse($value)->format('d/m/Y');
    }
    
    public function setPrimerCobroAttribute($value)
    {
        // 2016-06-27
        
        if($value ==''){

            return false;
        }
        
        $formato = 'd/m/Y';
        if (preg_match('/^\d{4}-\d{1,2}-\d{1,2}$/', $value)){
            $formato = 'Y-m-d';
        }
        
        $this->attributes['primer_cobro'] = Carbon::createFromFormat($formato, $value);
    }
    
    public function getPrimerCobroAttribute($value){

        if(is_null($value)){
            return null;
        }
    
        return Carbon::parse($value)->format('d/m/Y');
    }

    //primer_cobro
    public function setFechaDeContadoAttribute($value)
    {
        // 2016-06-27
        
        if($value ==''){

            return false;
        }
        
        $formato = 'd/m/Y';
        if (preg_match('/^\d{4}-\d{1,2}-\d{1,2}$/', $value)){
            $formato = 'Y-m-d';
        }
        
        $this->attributes['fecha_decontado'] = Carbon::createFromFormat($formato, $value);
    }
    
    public function getFechaDecontadoAttribute($value){

        if(is_null($value)){
            return null;
        }
    
        return Carbon::parse($value)->format('d/m/Y');
    }
    
    //vencimiento
   /* public function setVencimientoAttribute($value)
    {
        // 2016-06-27
       
        $formato = 'd-m-Y';
        if (preg_match('/^\d{4}-\d{1,2}-\d{1,2}$/', $value)){
            $formato = 'Y-m-d';
        }
        
        $this->attributes['vencimiento'] = Carbon::createFromFormat($formato, $value);
    }
    
    public function getVencimientoAttribute($value){
        return Carbon::parse($value)->format('d/m/Y');
    }*/

    //inicio
    
    public function setInicioAttribute($value)
    {
        // 2016-06-27
       
        $formato = 'd/m/Y';
        if (preg_match('/^\d{4}-\d{1,2}-\d{1,2}$/', $value)){
            $formato = 'Y-m-d';
        }
        
        $this->attributes['inicio'] = Carbon::createFromFormat($formato, $value);
    }

    
    public function getInicioAttribute($value){

   
        return Carbon::parse($value)->format('d/m/Y');
    }

    //fecha_incial
     public function setFechaIncialAttribute($value)
    {
        // 2016-06-27
       if($value != ''){
            $formato = 'd/m/Y';
            if (preg_match('/^\d{4}-\d{1,2}-\d{1,2}$/', $value)){
                $formato = 'Y-m-d';
            }
            
            $this->attributes['fecha_incial'] = Carbon::createFromFormat($formato, $value);
        }
    }
    
    public function getFechaIncialAttribute($value){
        if($value == ''){
            return null;
        }
        return Carbon::parse($value)->format('d/m/Y');
    }
}