<?php
namespace Modules\Cobranza\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class SolicitudBeneficiariosTemp extends Model
{
	protected $table = 'solicitud_beneficiarios_temp';
    protected $fillable = ["planilla","contratos_id","solicitud_id","planes_id","fecha","operacion"];

     public function setFechaAttribute($value)
    {
        // 2016-06-27
       
        $formato = 'd/m/Y';
        if (preg_match('/^\d{4}-\d{1,2}-\d{1,2}$/', $value)){
            $formato = 'Y-m-d';
        }
        
        $this->attributes['fecha'] = Carbon::createFromFormat($formato, $value);
    }
    
    public function getFechaAttribute($value){
        return Carbon::parse($value)->format('d/m/Y');
    }
}