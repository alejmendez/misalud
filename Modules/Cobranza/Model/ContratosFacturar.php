<?php
namespace Modules\Cobranza\Model;

use Illuminate\Database\Eloquent\Model;

class ContratosFacturar extends Model
{
	protected $table = 'contratos_facturar';
    protected $fillable = ["fecha_facturar","contratos_id"];
    protected $campos = [
    'fecha_facturar' => [
        'type' => 'date',
        'label' => 'Fecha Facturar',
        'placeholder' => 'Fecha Facturar del Contratos Facturar',
        'required' => true
    ],
    'contratos_id' => [
        'type' => 'number',
        'label' => 'Contratos',
        'placeholder' => 'Contratos del Contratos Facturar',
        'required' => true
    ]
];

    public function __construct(array $attributes = array())
	{
		parent::__construct($attributes);
		
	}
}