<?php
namespace Modules\Cobranza\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class BeneficiariosTemp extends Model
{
	protected $table = 'beneficiarios_temp';
    protected $fillable = ["dni","nombre","sexo","solicitud_beneficiarios_temp_id","parentesco_id","fecha_nacimiento"];
   

    public function setFechaNacimientoAttribute($value)
    {
        // 2016-06-27
       
        $formato = 'd/m/Y';
        if (preg_match('/^\d{4}-\d{1,2}-\d{1,2}$/', $value)){
            $formato = 'Y-m-d';
        }
        
        $this->attributes['fecha_nacimiento'] = Carbon::createFromFormat($formato, $value);
    }
    
    public function getFechaNacimientoAttribute($value){
        return Carbon::parse($value)->format('d/m/Y');
    }
}