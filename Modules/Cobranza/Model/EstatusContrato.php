<?php

namespace Modules\Cobranza\Model;

use Modules\Base\Model\Modelo;



class EstatusContrato extends modelo
{
    protected $table = 'estatus_contrato';
    protected $fillable = ["nombre"];
    protected $campos = [
    'nombre' => [
        'type' => 'text',
        'label' => 'Nombre',
        'placeholder' => 'Nombre del Estatus Contrato'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    
}