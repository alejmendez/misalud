<?php
namespace Modules\Cobranza\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Cobros extends Model
{
	protected $table = 'cobros';
    protected $fillable = [
        "contratos_id",
        "sucursal_id",
        "personas_id",
        "personas_bancos_id",
        "bancos_id",
        "total_cobrar",
        "concepto",
    
        "fecha_pagado",
        "total_pagado",
        "num_recibo",
        "tipo_pago",
        "completo",
        "created_at"
    ];
    protected $hidden = ['updated_at'];

    public function setFechaPagadoAttribute($value)
    {
        // 2016-06-27
       
        $formato = 'd/m/Y';
        if (preg_match('/^\d{4}-\d{1,2}-\d{1,2}$/', $value)){
            $formato = 'Y-m-d';
        }
        
        $this->attributes['fecha_pagado'] = Carbon::createFromFormat($formato, $value);
    }
    
    public function getFechaPagadoAttribute($value){
        if($value == null){
            return null;
        }
        return Carbon::parse($value)->format('d/m/Y');
    } 
    public function getCreatedAtAttribute($value){
        return Carbon::parse($value)->format('d/m/Y H:i');
    }

}