<?php
namespace Modules\Cobranza\Model;

use Illuminate\Database\Eloquent\Model;

class Beneficiarios extends model
{
	protected $table = 'beneficiarios';
    protected $fillable = ["contratos_id","personas_id","parentesco_id","estatus"];
  
}