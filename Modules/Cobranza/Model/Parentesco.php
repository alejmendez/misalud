<?php

namespace Modules\Cobranza\Model;

use Modules\Base\Model\Modelo;



class Parentesco extends modelo
{
    protected $table = 'parentesco';
    protected $fillable = ["nombre"];
    protected $campos = [
    'nombre' => [
        'type' => 'text',
        'label' => 'Nombre',
        'placeholder' => 'Nombre del Parentesco'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    
}