<?php
namespace Modules\Cobranza\Model;

use Modules\Base\Model\Modelo;
use Carbon\Carbon;
class Contratos extends modelo
{
	protected $table = 'contratos';
    protected $fillable = [
        'planilla',
        'renovaciones',
        "cobrando",
        "titular",
        "beneficiarios",
        "vendedor_id",
        "sucursal_id",
        "empresa_id",
        "cargado",
        "primer_cobro",
        "vencimiento",
        "inicio",
        "plan_detalles_id",
        "frecuencia_pagos_id",
        "tipo_pago",
        "total_contrato",
        "total_pagado",
        "cuotas_pagadas",
        "cuotas",
        "personas_bancos_id",
        "inicial",
        "fecha_incial",
        "anulado",
        "estatus_contrato_id",
        "fecha_pago"
    ];

    
    //cargado
    public function setCargadoAttribute($value)
    {
        // 2016-06-27
       
        $formato = 'd/m/Y';
        if (preg_match('/^\d{4}-\d{1,2}-\d{1,2}$/', $value)){
            $formato = 'Y-m-d';
        }
        
        $this->attributes['cargado'] = Carbon::createFromFormat($formato, $value);
    }
    
    public function getCargadoAttribute($value){
        return Carbon::parse($value)->format('d/m/Y');
    }


    //primer_cobro
    public function setPrimerCobroAttribute($value)
    {
        // 2016-06-27
       
        if($value != '' ){
             $formato = 'd/m/Y';
            if (preg_match('/^\d{4}-\d{1,2}-\d{1,2}$/', $value)){
                $formato = 'Y-m-d';
            }
            
            $this->attributes['primer_cobro'] = Carbon::createFromFormat($formato, $value);

        }else{
            $this->attributes['primer_cobro'] = null;
        }
       
    }
    
    public function getPrimerCobroAttribute($value){

        if(is_null($value)){
            return null;
        }
    
        return Carbon::parse($value)->format('d/m/Y');
    }
    
    //vencimiento
   /* public function setVencimientoAttribute($value)
    {
        // 2016-06-27
       
        $formato = 'd-m-Y';
        if (preg_match('/^\d{4}-\d{1,2}-\d{1,2}$/', $value)){
            $formato = 'Y-m-d';
        }
        
        $this->attributes['vencimiento'] = Carbon::createFromFormat($formato, $value);
    }
    
    public function getVencimientoAttribute($value){
        return Carbon::parse($value)->format('d/m/Y');
    }*/

    //inicio
    
    public function setInicioAttribute($value)
    {
        // 2016-06-27
       
        $formato = 'd/m/Y';
        if (preg_match('/^\d{4}-\d{1,2}-\d{1,2}$/', $value)){
            $formato = 'Y-m-d';
        }
        
        $this->attributes['inicio'] = Carbon::createFromFormat($formato, $value);
    }

    
    public function getInicioAttribute($value){

   
        return Carbon::parse($value)->format('d/m/Y');
    }

    //fecha_incial
     public function setFechaIncialAttribute($value)
    {
        // 2016-06-27
       if($value != ''){
            $formato = 'd/m/Y';
            if (preg_match('/^\d{4}-\d{1,2}-\d{1,2}$/', $value)){
                $formato = 'Y-m-d';
            }
            
            $this->attributes['fecha_incial'] = Carbon::createFromFormat($formato, $value);
        }else{
             $this->attributes['fecha_incial'] = null;
        }
    }
    
    public function getFechaIncialAttribute($value){
        return Carbon::parse($value)->format('d/m/Y');
    }
    public function setFechaPagoAttribute($value)
    {
        // 2016-06-27
       if($value != '' ){
            $formato = 'd/m/Y';
            if (preg_match('/^\d{4}-\d{1,2}-\d{1,2}$/', $value)){
                $formato = 'Y-m-d';
            }
            
            $this->attributes['fecha_pago'] = Carbon::createFromFormat($formato, $value);
        }else{
            $this->attributes['fecha_pago'] = null;
        }
    } 
    
    public function getFechaPagoAttribute($value){
        return Carbon::parse($value)->format('d/m/Y');
    }
}   