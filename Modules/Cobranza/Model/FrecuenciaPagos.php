<?php

namespace Modules\Cobranza\Model;

use Modules\Base\Model\Modelo;



class FrecuenciaPagos extends modelo
{
    protected $table = 'frecuencia_pagos';
    protected $fillable = ["frecuencia","dias","dias_str"];
    protected $campos = [
    'frecuencia' => [
            'type'       => 'select',
            'label'      => 'Frecuencia de pagos',
            'cont_class' => 'form-group col-md-4',
            'options'    =>[
                'm' => 'Mensual',
                'q' => 'Quincenal'
            ] 
    ],
    'dias' => [
        'type' => 'text',
        'label' => 'Dias',
        'placeholder' => 'Dias'
    ],
    'dias_str' => [
        'type' => 'text',
        'label' => 'dias_str',
        'placeholder' => 'dias_str'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    
}