<?php

Route::group(['prefix' => 'contratos'], function () {
    
	Route::group(['prefix' => 'parentescos'], function() {
		Route::get('/', 				'ParentescoController@index');
		Route::get('buscar/{id}', 		'ParentescoController@buscar');
		Route::get('nuevo', 		    'ParentescoController@nuevo');

		Route::get('cambiar/{id}', 		'ParentescoController@cambiar');
		Route::post('guardar',			'ParentescoController@guardar');
		Route::put('guardar/{id}', 		'ParentescoController@guardar');

		Route::delete('eliminar/{id}', 	'ParentescoController@eliminar');
		Route::post('restaurar/{id}', 	'ParentescoController@restaurar');
		Route::delete('destruir/{id}', 	'ParentescoController@destruir');

		Route::post('cambio', 			'ParentescoController@cambio');
		Route::get('datatable', 		'ParentescoController@datatable');
	});
	
	Route::group(['prefix' => 'tipocontrato'], function() {
		Route::get('/', 				'ContratoTipoController@index');
		Route::get('buscar/{id}', 		'ContratoTipoController@buscar');
		Route::get('nuevo', 		    'ContratoTipoController@nuevo');

		Route::get('cambiar/{id}', 		'ContratoTipoController@cambiar');
		Route::post('guardar',			'ContratoTipoController@guardar');
		Route::put('guardar/{id}', 		'ContratoTipoController@guardar');

		Route::delete('eliminar/{id}', 	'ContratoTipoController@eliminar');
		Route::post('restaurar/{id}', 	'ContratoTipoController@restaurar');
		Route::delete('destruir/{id}', 	'ContratoTipoController@destruir');

		Route::post('cambio', 			'ContratoTipoController@cambio');
		Route::get('datatable', 		'ContratoTipoController@datatable');
	});
	
	Route::group(['prefix' => 'estatuscontratos'], function() {
		Route::get('/', 				'EstatusContratoController@index');
		Route::get('buscar/{id}', 		'EstatusContratoController@buscar');
		Route::get('nuevo', 		    'EstatusContratoController@nuevo');

		Route::get('cambiar/{id}', 		'EstatusContratoController@cambiar');
		Route::post('guardar',			'EstatusContratoController@guardar');
		Route::put('guardar/{id}', 		'EstatusContratoController@guardar');

		Route::delete('eliminar/{id}', 	'EstatusContratoController@eliminar');
		Route::post('restaurar/{id}', 	'EstatusContratoController@restaurar');
		Route::delete('destruir/{id}', 	'EstatusContratoController@destruir');

		Route::post('cambio', 			'EstatusContratoController@cambio');
		Route::get('datatable', 		'EstatusContratoController@datatable');
	});
	
	Route::group(['prefix' => 'frecuencia'], function() {
		Route::get('/', 				'FrecuenciaPagosController@index');
		Route::get('buscar/{id}', 		'FrecuenciaPagosController@buscar');
		Route::get('nuevo', 		    'FrecuenciaPagosController@nuevo');

		Route::get('cambiar/{id}', 		'FrecuenciaPagosController@cambiar');
		Route::post('guardar',			'FrecuenciaPagosController@guardar');
		Route::put('guardar/{id}', 		'FrecuenciaPagosController@guardar');

		Route::delete('eliminar/{id}', 	'FrecuenciaPagosController@eliminar');
		Route::post('restaurar/{id}', 	'FrecuenciaPagosController@restaurar');
		Route::delete('destruir/{id}', 	'FrecuenciaPagosController@destruir');

		Route::post('cambio', 			'FrecuenciaPagosController@cambio');
		Route::get('datatable', 		'FrecuenciaPagosController@datatable');
	});

	Route::group(['prefix' => 'contrato'], function() {
		Route::get('/', 				'ContratosController@index');
		Route::get('buscar/{id}', 		'ContratosController@buscar');
		Route::get('consulta/{id}/{opera}/{solicitud_id}', 'ContratosController@consulta');
		//contato analista
		//Route::get('analista', 		    'ContratosController@contratoanalista');

		
		Route::get('vendedores/{id}',   'ContratosController@vendedores');
		Route::post('validar',			'ContratosController@validar');

		Route::post('confirmacion',		'ContratosController@confirmacion');
		
		
		Route::post('infoplan',			'ContratosController@infoplan');
		Route::post('planes',			'ContratosController@planes');
		
		Route::get('cambiar/{id}', 		'ContratosController@cambiar');
		Route::post('guardar',			'ContratosController@guardar');
		Route::put('guardar/{id}', 		'ContratosController@guardar');

		Route::delete('eliminar/{id}', 	'ContratosController@eliminar');
		Route::post('restaurar/{id}', 	'ContratosController@restaurar');
		Route::delete('destruir/{id}', 	'ContratosController@destruir');

		Route::post('cambio', 			'ContratosController@cambio');
		
		Route::get('datatable', 		'ContratosController@datatable');
		
	});

	Route::group(['prefix' => 'analista'], function() {
		Route::get('/', 				'AnalistaController@index');
		Route::get('buscar/{id}', 		'AnalistaController@buscar');
		Route::get('consulta', 		    'AnalistaController@consulta');
		Route::get('cobros/{id}', 		'AnalistaController@cobros');
		Route::get('historial/{id}', 	'AnalistaController@historial');

		//contato analista
		Route::get('vendedores/{id}',   		'AnalistaController@vendedores');
		Route::get('refinanciamiento/{id}',     'AnalistaController@refinaciamiento');
		Route::post('refinaciamientoguardar',   'AnalistaController@refinaciamientoguardar');
		Route::get('beneficiario',   			'AnalistaController@beneficiario');
		Route::post('validar',					'AnalistaController@validar');
		
		Route::post('infoplan',			'AnalistaController@infoplan');
		Route::post('planes',			'AnalistaController@planes');
		
		Route::get('cambiar/{id}', 		'AnalistaController@cambiar');
		Route::post('guardar',			'AnalistaController@guardar');
		Route::put('guardar/{id}', 		'AnalistaController@guardar');

		Route::delete('eliminar/{id}', 	'AnalistaController@eliminar');
		Route::post('restaurar/{id}', 	'AnalistaController@restaurar');
		Route::delete('destruir/{id}', 	'AnalistaController@destruir');

		Route::post('cambio', 			'AnalistaController@cambio');
		Route::get('datatable', 		'AnalistaController@datatable');
		Route::get('cobrostable/{id}', 	'AnalistaController@cobrostable');
		
	});

	Route::group(['prefix' => 'opcontratos'], function() {
		
		Route::get('opanular', 						'OPcontratosController@opanular');
		Route::get('renovacion/{id}/{adonde}', 		'OPcontratosController@renovacion');
		Route::get('beneficiarios/{id}/{adonde}', 	'OPcontratosController@beneficiarios');
		Route::get('beneficiariosconfirmacion/{id}','OPcontratosController@beneficiariosconfirmacion');
		Route::POST('frecuencias', 					'OPcontratosController@frecuencias');
		Route::POST('confirmacionbene', 			'OPcontratosController@confirmacionbene');
		Route::post('renovacion/guardar',			'OPcontratosController@renovacionGuardar');
		Route::put('renovacion/guardar/{id}',		'OPcontratosController@renovacionGuardar');
		Route::POST('beneficiario',   				'OPcontratosController@beneficiario');
		Route::POST('planes',   					'OPcontratosController@plan');
		Route::POST('infoplan',						'OPcontratosController@infoplan');
		Route::POST('guardar/beneficiarios',		'OPcontratosController@beneficiariosguardar');
		Route::POST('reenviarsolicitud',			'OPcontratosController@reenviarsolicitud');
		//Route::put('guardar/{id}/{adonde}', 		'OPcontratosController@renovacionGuardar');
	});

	Route::group(['prefix' => 'pagos'], function() {
		Route::get('/consulta/pagos/{id}', 		'PagosCajasController@index');
		Route::post('/debe', 					'PagosCajasController@debe');
		Route::post('/guardar',					'PagosCajasController@guardar');
	});

});

Route::group(['prefix' => 'cobranza'], function () {
	Route::group(['prefix' => 'escritorio/analista'], function() {
		Route::get('/', 		   'EscritorioanalistaController@index');
		Route::post('solicitudes', 'EscritorioanalistaController@solicitudes');	
	});
});
Route::group(['prefix' => 'auto'], function () {
	Route::group(['prefix' => 'contratos/vencidos'], function() {
		Route::get('/', 'TarasAutomatizadaController@contratosvencidos');
	});
});