@extends('base::layouts.default')
@section('content')
	@include('base::partials.ubicacion', ['ubicacion' => ['Escritorio Vendedor']])
	<div class="row">
		<div class="col-md-3">	
			<div class="list-group">
				<a href="#" class="list-group-item active"><center>Opciones</center></a>
				<a href="{{ url('contratos/analista') }}" class="list-group-item"><i class="fa fa-plus" aria-hidden="true"></i>Contratos</a>	
				<a href="{{ url('contratos/contrato') }}" class="list-group-item"><i class="fa fa-plus" aria-hidden="true"></i>Asistente Creacion de Contratos</a>
		
				<a href="{{ url('consulta/') }}" class="list-group-item "><i class="fa fa-search" aria-hidden="true"></i> Consultar Cliente</a>

				<a href="#" class="list-group-item "><i class="fa fa-download" aria-hidden="true"></i> Generar Lotes de Cobranza</a>

				<a href="#" class="list-group-item "><i class="fa fa-upload" aria-hidden="true"></i> Procesar Lotes de Cobranza</a>
				
			</div>
		</div> 
		<div class="col-md-9">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<center><h3 class="panel-title">Solicitudes</h3></center>
				</div>
				<div class="panel-body">
					<center><table id="tabla" class="table table-striped table-hover table-bordered tables-text">
						<thead>
							<tr>
								<th style="width: 10%; text-align: center;">N°</th>
								<th style="width: 15%; text-align: center;">Fecha</th>
								<th style="width: 15%; text-align: center;">Tipo de Solicitud</th>
								<th style="width: 30%; text-align: center;">Responsable</th>
							</tr>
						</thead>
						<tbody id="solicitudes">
							<tr></tr>
						</tbody>
					</table></center>
				</div>
			</div>	
		</div>
	</div>
@endsection