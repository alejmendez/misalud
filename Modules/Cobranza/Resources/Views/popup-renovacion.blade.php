@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Contratos Historial']])
    
@endsection
@section('content')
	<div class="row"> 
		{!! Form::open(['id' => 'submit_form', 'name' => 'formulario', 'method' => 'POST' ]) !!}
			
			<input type="hidden" name="adonde" value="{{$adonde}}">
			<input type="hidden" name="id" id="id" value="{{$contratos_id}}">
			<div class="col-md-12">
		        <div class="portlet light" id="form_wizard_1">
		            <div class="portlet-title">
		                <div class="caption">
		                    <i class=" icon-layers font-red"></i>
		                    <span class="caption-subject font-red bold uppercase"> Renovación De Contrato
		                        <span class="step-title"> 1 de 2 </span>
		                    </span>
		                </div>
		            </div>
		            <div class="portlet-body form">
		                
		                    <div class="form-wizard">
		                        <div class="form-body">
		                            <ul class="nav nav-pills nav-justified steps">
		                                <li>
		                                    <a href="#tab1" data-toggle="tab" class="step">
		                                        <span class="number"> 1 </span>
		                                        <span class="desc">
		                                            <i class="fa fa-check"></i>Datos Generales</span>
		                                    </a>
		                                </li>
		                                <li>
		                                    <a href="#tab2" data-toggle="tab" class="step">
		                                        <span class="number"> 2 </span>
		                                        <span class="desc">
		                                            <i class="fa fa-check"></i> Planes y tipos de Pagos</span>
		                                    </a>
		                                </li>
		                            </ul>
		                            <div id="bar" class="progress progress-striped" role="progressbar">
		                                <div class="progress-bar progress-bar-success"> </div>
		                            </div>
		                            <div class="tab-content">
		                                <div class="alert alert-danger display-none">
		                                    <button class="close" data-dismiss="alert"></button> Usted tiene algunos errores en el formulario. Por favor, compruebe.
		                                </div>
		                                <div class="alert alert-success display-none">
		                                    <button class="close" data-dismiss="alert"></button> ¡La validación de su formulario es exitosa!
		                                </div>
		                                
		                                <div class="tab-pane active" id="tab1">
							                <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
												<label for="min">Fecha del Renovacion</label>
												<input id="fecha" name="inicio" class="form-control" type="text" placeholder=""  required="date" />
											</div>

											{{ Form::bsNumber('planilla', '',[
												'label'    => 'N° de Planilla',
												'required' => 'required'
											]) }}    
		                                   
		                                </div>

		                                <div class="tab-pane" id="tab2">
		                                	
		                                	{{ Form::bsSelect('planes_id', [], '', [
												'label'    => 'Planes',
												'required' => 'required'
											]) }}

											 {{ Form::bsText('Num_benefe',$beneficiarios, [
							                        'label'    => 'N° de Beneficiarios',
							                        'required' => 'required',
							                        'readonly' => 'true'
							                ]) }}

											{{ Form::bsSelect('frecuencia', [
													'm'=>'Mensual',
													'q'=>'Quincenal'

												], '', [
												'label'    => 'Frecuencia de Pago',
												'required' => 'required'
											]) }}

											{{ Form::bsText('meses', '', [
												'class'       => 'form-control',
												'label'       => 'Meses para Pagar',
												'disabled' => 'disabled'
								        	]) }}  
 											<button type="button" id="calcular"  class="btn btn-info col-md-12"> Calcular</button>
                							
								        	<div class="col-md-12"></div>
								        	<div class="col-md-12">
								        		
								        		<center>					
													<span class="caption-subject font-red bold uppercase"> 
														De Contado
								                    </span>
						                   		 </center>	
								        	</div>
								        							 
									  
							            	<div class="table-scrollable">
							            		
							               		<table id="tabla-plan" class="table table-striped table-hover table-bordered">
								                    <thead>
								                        <tr>
								                            <th>(-) %</th>
								                            <th>Bs</th>
								                        </tr>
								                    </thead>
								                    <tbody>
								                        <tr>
								                            <td><input name="" id="porsentaje" class="form-control" type="text" disabled="" /></td>
								                            <td><input name="" id="contado" class="form-control" type="text" disabled=""/></td>
								                           
								                        </tr>
								                       
								                    </tbody>
								                </table>
							            	</div>

							            	<center>
								            	<span class="caption-subject font-red bold uppercase"> 
													Credito
							                    </span>
						                    </center>
											<div class="table-scrollable">
							               		<table id="tabla-plan" class="table table-striped table-hover table-bordered">
								                    <thead>
								                        <tr>
								                            <th>Inicial Credito</th> 
								                            <th>N. de Giros</th>
								                            <th>Monto por Giros (Bs)</th>
								                            <th>Total 1 A&ntilde;o (Bs)</th>
								                        </tr>
								                    </thead>
								                    <tbody>
								                        <tr>
								                            <td><input name="" id="inicial" class="form-control" type="text" disabled=""/></td>
								                           
								                            <td><input name="" id="n_giros" class="form-control" type="text" disabled=""/></td>
								                            <td><input name="" id="giro_bs" class="form-control" type="text" disabled=""/></td>
								                            <td><input name="" id="total" class="form-control" type="text" disabled=""/></td>
								                        </tr>
								                       
								                    </tbody>
								                </table>
							            	</div>	 

							            	{{ Form::bsSelect('tipo_pago', [
													'1'=>'De Contado',
													'2'=>'Credito'					
												], '', [
												'label'    => 'Tipo de Pago',
												'required' => 'required'
											]) }}

											<div class="col-md-12"></div>

											<div class="col-md-12" id="credito">

												<div class="form-group col-md-12 col-sm-6 col-xs-12">
												    <label for="personas_bancos_id" class="requerido">Número de Cuenta</label>
												   <select name="personas_bancos_id" id="personas_bancos_id" class="form-control 'col-md-4 col-sm-6 col-xs-12'" required="">
													  	<option value="">seleccione</option>
													  	@foreach($cuentas as $cuenta)
													  		<option value="{{$cuenta->id}}">{{$cuenta->nombre}}=>{{$cuenta->cuenta }}</option>
													  	@endforeach
													</select>
												</div>

												<div >
													
												</div>
												<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
													<label for="min">Fecha de Cobro de Inicial</label>
													<input id="fecha2" name="fecha_incial" class="form-control" type="text" placeholder="" required="" />
												</div>

												{{ Form::bsSelect('frecuencia_pagos_id', [], '', [
													'label'    => 'Dias de Cobros',
													'required' => 'required',
													'class_cont' => 'col-md-4 col-sm-6 col-xs-12'
												]) }}

											</div>

											<div class="col-md-12" id="decontado">
												{{ Form::bsText('fecha_decontado', '', [
									            'label'         => 'Fecha',
									            'placeholder'   => 'Fecha',
									            'required'      => 'required',
									            'class_cont'    => 'col-md-4 col-sm-6 col-xs-12'
									        	]) }}

									        	{{ Form::bsNumber('n_recibo', '',[
												'label'    => 'N° de Recibo',
												'required' => 'required'
												]) }} 

												{{ Form::bsSelect('pago', [
													'0'=>'Por Tarjeta',
													'1'=>'Transferencia',
													'2'=>'Efectivo'					
												], '', [
												'label'    => 'Tipo de Pago',
												'required' => 'required'
												]) }}

											</div>				
		                                </div>      
		                        </div>
		                        <div class="form-actions">
		                            <div class="row">
		                                <div class="col-md-offset-3 col-md-9">
		                                    <a href="javascript:;" class="btn default button-previous">
		                                        <i class="fa fa-angle-left"></i> Atras </a>
		                                    <a href="javascript:;" class="btn btn-outline green button-next"> Siguiente
		                                        <i class="fa fa-angle-right"></i>
		                                    </a>
		                                    <button type="" class="btn green button-submit" id="guardar"> Guardar <i class="fa fa-check"></i></button>
		                                    
		                                </div>
		                            </div>
		                        </div>
		                    </div>
		                
		            </div>
		        </div>
		    </div>	
		{!! Form::close() !!}   
	</div>
@endsection
@push('css')
<style type="text/css" media="screen">
	hr{
		border-color: #000;
	}

	#credito{
		display: none;
	}

	#decontado{
		display: none;
	}
</style>	
@endpush