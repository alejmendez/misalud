@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Contratos Historial']])
    
@endsection
@section('content')
	<div class="row"> 
		{!! Form::open(['id' => 'submit_form', 'name' => 'formulario', 'method' => 'POST' ]) !!}
			
			<input type="hidden" name="adonde" value="{{$adonde}}">
			<input type="hidden" name="id" value="{{$contratos_id}}">
			<div class="col-md-12">
		        <div class="portlet light" id="form_wizard_1">
		            <div class="portlet-title">
		                <div class="caption">
		                    <i class=" icon-layers font-red"></i>
		                    <span class="caption-subject font-red bold uppercase"> Beneficiarios
		                        <span class="step-title"> 1 de 2 </span>
		                    </span>
		                </div>
		            </div>
		            <div class="portlet-body form">
		                
		                    <div class="form-wizard">
		                        <div class="form-body">
		                            <ul class="nav nav-pills nav-justified steps">
		                                <li>
		                                    <a href="#tab1" data-toggle="tab" class="step">
		                                        <span class="number"> 1 </span>
		                                        <span class="desc">
		                                            <i class="fa fa-check"></i>Datos Generales</span>
		                                    </a>
		                                </li>
		                                <li>
		                                    <a href="#tab2" data-toggle="tab" class="step">
		                                        <span class="number"> 2 </span>
		                                        <span class="desc">
		                                            <i class="fa fa-check"></i> Planes y tipos de Pagos</span>
		                                    </a>
		                                </li>
		                            </ul>
		                            <div id="bar" class="progress progress-striped" role="progressbar">
		                                <div class="progress-bar progress-bar-success"> </div>
		                            </div>
		                            <div class="tab-content">
		                                <div class="alert alert-danger display-none">
		                                    <button class="close" data-dismiss="alert"></button> Usted tiene algunos errores en el formulario. Por favor, compruebe.
		                                </div>
		                                <div class="alert alert-success display-none">
		                                    <button class="close" data-dismiss="alert"></button> ¡La validación de su formulario es exitosa!
		                                </div>
		                                
		                                <div class="tab-pane active" id="tab1">
								                
			                                <div class="row">
				                                <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
													<label for="min">Fecha</label>
													<input id="fecha" name="inicio" class="form-control" type="text" placeholder=""  required="date" />
												</div>

												{{ Form::bsNumber('planilla', '',[
													'label'    => 'N° de Planilla',
													'required' => 'required'
												]) }}

			                                </div>
							                
											<div class="row">
								               <center> <h3 class="">Beneficiarios</h3></center>
								                <div class="col-md-12">
								                    <table border='1' class="table table-striped table-hover" id="tabla-beneficiarios">
								                        <thead>
								                            <tr>
								                                <th>N° C.I</th>
								                                <th>Nombres</th>
								                                <th>Sexo</th>
								                                <th>Fecha de Nacimiento</th>
								                                <th>Parentesco</th>
								                                <th style="width: 60px">
								                                    <button id="agregar" type="button" class="btn green tooltips circule" data-container="body" data-placement="top">
								                                        <i class="fa fa-plus" aria-hidden="true"></i>
								                                    </button>
								                                </th>
								                            </tr>
								                        </thead>
								                        <tbody id="beneficiarios"></tbody>
								                    </table>
								                </div>
										    </div>     
		                                </div>

		                                <div class="tab-pane" id="tab2">

		                                	<center><h2>Tipo de Operacion: <span id="estatus"></span></h2></center>

										    <div class="panel panel-primary" style="display: none;" id="plan_old">
												<div class="panel-heading">
													<center><h3 class="panel-title">Infomacion de Pagos del Contrato Actual</h3></center>
												</div>
												<div class="panel-body">
													<center><table id="tabla" class="table table-striped table-hover table-bordered tables-text">
														<thead>
															<tr>
																<th style="width: 14.28%; text-align: center;">Plan</th>
																<th style="width: 14.28%; text-align: center;">Tipo de Pago</th>
																<th style="width: 14.28%; text-align: center;">Frecuencia de Pago</th>
																<th style="width: 14.28%; text-align: center;">Cantidad Cuotas</th>
																<th style="width: 14.28%; text-align: center;">Cuotas(Bs)</th>
																<th style="width: 14.28%; text-align: center;">Total Contrato(Bs)</th>
																<th style="width: 14.28%; text-align: center;">Total Cancelado(Bs)</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td><span id="inf-plan"></span></td>
																<td><span id="inf-tipo"></span></td>
																<td><span id="inf-frecuencia"></span></td>
																<td><span id="inf-cuotas2"></span></td>
																<td><span id="inf-cuotas"></span></td>
																<td><span id="inf-total"></span></td>
																<td><span id="inf-total-pagado"></span></td>
															</tr>
														</tbody>
													</table></center>
												</div>
											</div>	


		                                	{{ Form::bsSelect('planes_id', [], '', [
												'label'    => 'Planes',
												'required' => 'required'
											]) }}

											 {{ Form::bsText('Num_benefe',$beneficiarios, [
							                        'label'    => 'N° de Beneficiarios',
							                        'required' => 'required',
							                        'readonly' => 'true'
							                ]) }}

											{{ Form::bsSelect('frecuencia', [
													'm'=>'Mensual',
													'q'=>'Quincenal'

												], '', [
												'label'    => 'Frecuencia de Pago',
												'required' => 'required'
											]) }}

											{{ Form::bsText('meses', '', [
												'class'       => 'form-control',
												'label'       => 'Meses para Pagar',
												'disabled' => 'disabled'
								        	]) }}  
 											<button type="button" id="calcular"  class="btn btn-info col-md-12"> Calcular</button>
                							
								        	<div class="col-md-12"></div>
								        	<div class="col-md-12">
								        		
								        		<center>					
													<span class="caption-subject font-red bold uppercase"> 
														De Contado
								                    </span>
						                   		 </center>	
								        	</div>
								        							 
									  
							            	<div class="table-scrollable">
							            		
							               		<table id="tabla-plan" class="table table-striped table-hover table-bordered">
								                    <thead>
								                        <tr>
								                            <th>(-) %</th>
								                            <th>Bs</th>
								                        </tr>
								                    </thead>
								                    <tbody>
								                        <tr>
								                            <td><input name="" id="porsentaje" class="form-control" type="text" disabled="" /></td>
								                            <td><input name="" id="contado" class="form-control" type="text" disabled=""/></td>
								                           
								                        </tr>
								                       
								                    </tbody>
								                </table>
							            	</div>

							            	<center>
								            	<span class="caption-subject font-red bold uppercase"> 
													Credito
							                    </span>
						                    </center>
											<div class="table-scrollable">
							               		<table id="tabla-plan" class="table table-striped table-hover table-bordered">
								                    <thead>
								                        <tr>
								                            <th>Inicial Credito</th> 
								                            <th>N. de Cuota</th>
								                            <th>Monto por Cuota (Bs)</th>
								                            <th>Total 1 A&ntilde;o (Bs)</th>
								                        </tr>
								                    </thead>
								                    <tbody>
								                        <tr>
								                            <td><input name="" id="inicial" class="form-control" type="text" disabled=""/></td>
								                           
								                            <td><input name="" id="n_giros" class="form-control" type="text" disabled=""/></td>
								                            <td><input name="" id="giro_bs" class="form-control" type="text" disabled=""/></td>
								                            <td><input name="" id="total" class="form-control" type="text" disabled=""/></td>
								                        </tr>
								                       
								                    </tbody>
								                </table>
							            	</div>	 

							            		<input type="hidden" name="opera" id="opera" value="">
							 
											 {{ Form::bsSelect('tipo_pago', [
							                    '1'=>'De Contado',
							                    '2'=>'Credito'                  
							                ], '', [
							                'label'    => 'Tipo de Pago',
							                'required' => 'required'
							                ]) }}

							                <div class="col-md-12"></div>

							            	 <div class="col-md-12" id="credito" style="display: none;" >
     	

											<div class="form-group col-md-12 col-sm-6 col-xs-12">
											    <label for="personas_bancos_id" class="requerido">Número de Cuenta</label>
											   <select name="personas_bancos_id" id="personas_bancos_id" class="form-control 'col-md-4 col-sm-6 col-xs-12'" required="">
												  	<option value="">seleccione</option>
												</select>
											</div>

											<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
												<label for="min">Fecha de Cobro de Inicial</label>
												<input id="fecha2" name="fecha_incial" class="form-control" type="text" placeholder="" required="" />
											</div>

											{{ Form::bsSelect('frecuencia_pagos_id', [], '', [
												'label'    => 'Dias de Cobros',
												'required' => 'required',
												'class_cont' => 'col-md-4 col-sm-6 col-xs-12'
											]) }}

											</div>

							                <div class="col-md-12" id="decontado" style="display: none;" >
							                    {{ Form::bsText('fecha_decontado', '', [
							                        'label'         => 'Fecha',
							                        'placeholder'   => 'Fecha',
							                        'required'      => 'required',
							                        'class_cont'    => 'col-md-4 col-sm-6 col-xs-12'
							                    ]) }}
							                    
							                    {{ Form::bsNumber('n_recibo', '',[
							                        'label'    => 'N° de Recibo',
							                        'required' => 'required'
							                    ]) }} 

							                    {{ Form::bsSelect('pago', [
							                        '0'=>'Por Tarjeta',
							                        '1'=>'Transferencia',
							                        '2'=>'Efectivo'                 
							                        ], '', [
							                        'label'    => 'Tipo de Pago',
							                        'required' => 'required'
							                    ]) }}
							                   
							                </div>

											<div class="col-md-12"></div>			
		                                </div>      
		                        </div>
		                        <div class="form-actions">
		                            <div class="row">
		                                <div class="col-md-offset-3 col-md-9">
		                                    <a href="javascript:;" class="btn default button-previous">
		                                        <i class="fa fa-angle-left"></i> Atras </a>
		                                    <a href="javascript:;" class="btn btn-outline green button-next"> Siguiente
		                                        <i class="fa fa-angle-right"></i>
		                                    </a>
		                                    <button type="" class="btn green button-submit" id="guardar"> Guardar <i class="fa fa-check"></i></button>
		                                    
		                                </div>
		                            </div>
		                        </div>
		                    </div>
		                
		            </div>
		        </div>
		    </div>	
		{!! Form::close() !!}   
	</div>
@endsection
@push('css')
<style type="text/css" media="screen">
	hr{
		border-color: #000;
	}

	#credito{
		display: none;
	}

	#decontado{
		display: none;
	}
</style>	
@endpush

@push('js')
<script s type="text/javascript" charset="utf-8" async defer>
    
    $id 	 = '{{$id}}';


</script>
<script type="text/x-tmpl" id="tmpl-demo2">
        <tr>
            <td>
                <input type="text"  id="dni_beneficiario" class="form-control" name="dni_beneficiario[]" placeholder="N° C.I" value="">
            </td> 
            <td>
               <input type="text" value=""  id="nombres_beneficiario" class="form-control" name="nombres_beneficiario[]" placeholder="Nombres y Apellidos"> 
            </td>
            <td>
                <select name="sexo_beneficiario[]"  id="sexo_beneficiario" class="form-control">
                    <option value="m">Masculino</option>
                    <option value="f">Femenino</option>
                </select>
            </td>
            <td>
                <input  value="" class="form-control nacimiento_beneficiario" id="nacimiento_beneficiario" type="text" name='nacimiento_beneficiario[]' placeholder="" />
            </td>
            <td>
                <select name="parentescos_beneficioario[]" id="parentescos_beneficioario" class="form-control">
                    <option value="">seleccione</option>
                    @foreach($controller->parentescos() as $parentesco)
                        <option value="{{$parentesco->id}}">{{$parentesco->nombre}}</option>
                    @endforeach
                </select>
            </td>
            <td>
               <button type="button" class="btn btn-danger eliminar"><i class="fa fa-minus-circle" aria-hidden="true"></i></button> 
            </td> 
        </tr>
</script>
<script type="text/x-tmpl" id="tmpl-demo5">
    {% for (var i=0, file; file=o.datos[i]; i++) { %}
       <tr>
            <td>
                <input type="text"  id="dni_beneficiario" class="form-control" name="dni_beneficiario[]" placeholder="Dni" value="{%=file.dni%}">
            </td> 
            <td>
               <input type="text" value="{%=file.nombres%}"  id="nombres_beneficiario" class="form-control" name="nombres_beneficiario[]" placeholder="Nombres y Apellidos"> 
            </td>
            <td>
                <select name="sexo_beneficiario[]"  id="sexo_beneficiario" class="form-control">
                    <option value="m" {% if (file.sexo == 'm') { %} selected="selected" {% } %}>Masculino</option>
                    <option value="f" {% if (file.sexo == 'f') { %} selected="selected" {% } %}>Femenino</option>
                </select>
            </td>
            <td>
                <input  value="{%=file.fecha_nacimiento%}" class="form-control nacimiento_beneficiario" id="nacimiento_beneficiario" type="text" name='nacimiento_beneficiario[]' placeholder="" />
            </td>
            <td>
                <select name="parentescos_beneficioario[]" id="parentescos_beneficioario" class="form-control">
                    <option value="">seleccione</option>
                    @foreach($controller->parentescos() as $parentesco)
                        <option value="{{$parentesco->id }}" {% if (file.parentesco_id == {{$parentesco->id}}) { %} selected="selected" {% } %}>
                                {{$parentesco->nombre}}
                            </option>
                         
                    @endforeach
                </select>
            </td>
            <td>
               <button type="button" class="btn btn-danger eliminar">
                    <i class="fa fa-minus-circle" aria-hidden="true"></i>
                </button> 
            </td>  
        </tr>
    {% } %}
</script>
@endpush
