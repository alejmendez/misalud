@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Contratos detalles']])
    
@endsection
@section('content')
	<div class="row"> 
		<div class="col-md-12">
			<div class="portlet light ">
				<div class="portlet-title tabbable-line">
					<div class="caption caption-md">
						<i class="icon-globe theme-font hide"></i>
						<span class="caption-subject font-blue-madison bold uppercase">{{$tipo}}</span>
					</div>
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab_1_1" data-toggle="tab"> Informacion Contrato</a></li>

						<li><a href="#tab_1_2" data-toggle="tab">Beneficiarios(Actuales)</a></li>
						
						<li><a href="#tab_1_3" data-toggle="tab">Beneficiarios(Solicitud)</a></li>
						
						
						<li><a href="#tab_1_4" data-toggle="tab">Contratos Historial</a></li>

					</ul>
				</div>
				<div class="portlet-body">
					<div class="tab-content">
						<div class="tab-pane active" id="tab_1_1"> 
							<div class="form-body">	
								<div class="row">
									<div class="col-md-12">
										<table class="table table-striped table-hover " border="0">
											<tbody >
												<tr>
													<td width="30%"><b>Planilla:</b> <br><span id="planilla">{{$Contrato->planilla}}</span></td>
													<td width="20%"><b>Dni:</b><br> <span id="dni">{{$Persona->tipo_persona->nombre}}-{{$Persona->dni}} </span></td>
													<td width="50%"><b>Titular:</b><br> <span id="titular">{{$Persona->nombres}}</span></td>
												</tr>
												<tr>
													<td><b>Sucursal:</b><br> <span id ="sucursal">{{$Contrato->sucursal}}</span></td>
													<td><b>Renovaciones:</b><br> <span id ="renovaciones">{{$Contrato->renovaciones}}</span> </td>
													<td><b>Vendedor:</b><br> <span id="vendedor">{{$Contrato->vendedor}}</span></td>
												</tr>
												<tr >
													<td><b>Fecha de Contrato:</b><br> <span id ="inicio">{{$Contrato->inicio}}</span></td>
													<td></td>
													<td><b>Fecha Vencimiento:</b><br> <span id ="vencimiento">{{$Contrato->vencimiento}}</span> </td>
												</tr>

												<tr >
												
													
														<td><b>Total Beneficiarios:</b><br> <span id ="num_beneficiarios"> {{$beneficiario}}</span></td>
														<td></td>
														<td><b>Plan:</b><br> <span id ="plan">{{$plan}}</span> </td>
													
												</tr>

												<tr >
													<td><b>Total Contrato</b><br> <span id ="total_contrato">{{$Contrato->total_contrato}}</span> </td>
													<td><b>Total Pagado</b><br> <span id ="total_pagado">{{$Contrato->total_pagado}}</span> </td>
													@if($Contrato->tipo_pago == 2)
														<td><b>Banco</b><br> <span id ="banco">{{$Contrato->banco}}</span> </td>
													@else
														<td><b>Banco</b><br> <span id ="banco"></span> </td>
													@endif	
												</tr>
												<tr >
													@if($Contrato->tipo_pago == 1)
														<td><b>Tipo de pago:</b> <br><span id ="tipo_pago">De Contado</span></td>
													@else
														<td><b>Tipo de pago:</b> <br><span id ="tipo_pago">Credido</span></td>
														<?php
															$frecuencia= 'Mensual';
															if($Contrato->frecuencia == 'q'){
																$frecuencia = 'Quincenal';
															}

														?>
														<td><b>Frecuencia de pago:</b><br> <span id ="frecuencia_pagos"> {{$frecuencia}}: {{$Contrato->dias}}</span></td>
														<td><b>Cuenta Afiliada:</b><br> <span id ="personas_bancos_id">{{$Contrato->cuenta}}</span></td>
													@endif	
												</tr>
												<tr >
													@if($Contrato->tipo_pago == 2)
														<td><b>inicial:</b><br> <span id ="inicial">{{$Contrato->inicial}}</span></td>
														<td><b>fecha Inicial:</b><br> <span id ="fecha_incial">{{$Contrato->fecha_incial}}</span></td>
													@else
														<td><b>inicial:</b><br> <span id ="inicial"></span></td>
														<td><b>fecha Inicial:</b><br> <span id ="fecha_incial"></span></td>
													@endif	
														<td><b>Primer Cobro:</b> <br><span id ="primer_cobro">{{$Contrato->primer_cobro}}</span></td>
												</tr> 
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane " id="tab_1_2">
							<table class="table table-striped table-hover ">
								<thead>
									<tr>
										<th>#</th>
										<th>Dni</th>
										<th>Nombre y Apellido</th>
										<th>Sexo</th>
										<th>Fecha de Nacimiento</th>
										
										<th>Parentesco</th>
									</tr>
								</thead>
								<tbody>
									<?php $num = 1?>
									@foreach($info_beneficiarios as $beneficiario)
										<tr>
											<td>{{$num++}}</td>
											<td>{{$beneficiario->nombre}}-{{$beneficiario->dni}}</td>
											<td>{{$beneficiario->nombres}}</td>

											@if($beneficiario->sexo == 'm')
												<td>Masculino</td>
											@else
												<td>Femenino</td>
											@endif
											<?php
										        $fecha_n = \Carbon\Carbon::parse($beneficiario->fecha_nacimiento)->format('d/m/Y');

											?>
											<td>{{$fecha_n}}</td>
										
											<td>{{$beneficiario->parentesco}}</td>
										</tr>

									@endforeach
								</tbody>
							</table> 
						</div> 
						<div class="tab-pane " id="tab_1_3">

							<div class="panel panel-primary" id="plan_old">
								<div class="panel-heading">
									<center><h3 class="panel-title">Infomacion de Pago</h3></center>
								</div>
								<div class="panel-body">
									<center><table id="tabla" class="table table-striped table-hover table-bordered tables-text">
										<thead>
											<tr>
												<th style="width: 14.28%; text-align: center;">Plan</th>
												<th style="width: 14.28%; text-align: center;">Tipo de Pago</th>
												<th style="width: 14.28%; text-align: center;">Frecuencia de Pago</th>
												<th style="width: 14.28%; text-align: center;">Cantidad Cuotas</th>
												<th style="width: 14.28%; text-align: center;">Cuotas(Bs)</th>
												<th style="width: 14.28%; text-align: center;">Total Contrato(Bs)</th>
												<th style="width: 14.28%; text-align: center;">Total Cancelado(Bs)</th>
											</tr>
										</thead>
										<tbody>
											<tr>
		
												<td><span id="inf-plan">{{$inf_plan}}</span></td>
												<td><span id="inf-tipo">{{$inf_tipo}}</span></td>
												<td><span id="inf-frecuencia">{{$inf_frecuencia}}</span></td>
												<td><span id="inf-cuotas">{{$inf_cuotas}}</span></td>
												<td><span id="inf-cuotas2">{{$inf_cuotas2}}</span></td>
												<td><span id="inf-total">{{$inf_total}}</span></td>
												<td><span id="inf-total-pagado">{{$inf_total_pagado}}</span></td>
											</tr>
										</tbody>
									</table></center>
								</div>
							</div>	


							<table class="table table-striped table-hover ">
								<thead>
									<tr>
										<th>#</th>
										<th>Dni</th>
										<th>Nombre y Apellido</th>
										<th>Sexo</th>
										<th>Fecha de Nacimiento</th>
										
										<th>Parentesco</th>
									</tr>
								</thead>
								<tbody>
									<?php $num = 1?>
									@foreach($beneficiarios_temp as $beneficiario)
										<tr>
											<td>{{$num++}}</td>
											<td>{{$beneficiario->dni}}</td>
											<td>{{$beneficiario->nombre}}</td>

											@if($beneficiario->sexo == 'm')
												<td>Masculino</td>
											@else
												<td>Femenino</td>
											@endif
											
											<td>{{$beneficiario->fecha_nacimiento}}</td>
											
											<td>{{$beneficiario->parentesco}}</td>
										</tr>

									@endforeach
								</tbody>
							</table> 
						</div> 
						<div class="tab-pane " id="tab_1_4">
							<table class="table table-striped table-hover ">
								<thead>
									<tr>
										<th>#</th>
										<th>fecha</th>
										<th>Operacion</th>
										<th>Responsable</th>
										<th>Planilla</th>
									</tr>
								</thead>
								<tbody>
								<?php $num2 = 1;?>
									@foreach($contratos_detalles as $contrato_detalle )
										<tr>
											<td>{{$num2++}}</td>
											
											<?php
										        $fecha_n = \Carbon\Carbon::parse($contrato_detalle->created_at)->format('d/m/Y');

											?>
											
											<td>{{$fecha_n}}</td>
											<td>{{$contrato_detalle->operacion}}</td>
											<td>{{$contrato_detalle->nombre}}-{{$contrato_detalle->dni}} {{$contrato_detalle->nombres}}</td>
											<td>{{$contrato_detalle->planilla}}</td>
										</tr>
									@endforeach
								</tbody>
							</table> 
						</div> 
						
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-4"></div>
		<div class="col-md-4">
			
			
			<input type="hidden" name="solicitud_id" id="solicitud_id" value="{{$solicitud_id}}">

			<a href="#" id='aceptar' class="btn btn-primary">Confirmar</a> 
		</div>
		<div class="col-md-4"></div>
	</div>
		
	
@endsection
