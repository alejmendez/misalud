@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Contratos detalles']])
    
@endsection
@section('content')
	<div class="row"> 
		<div class="col-md-12">
			<div class="portlet light ">
				<div class="portlet-title tabbable-line">
					<div class="caption caption-md">
						<i class="icon-globe theme-font hide"></i>
						<span class="caption-subject font-blue-madison bold uppercase">Contrato ID: {{$Contrato->id}} ({{$Contrato->estatus_contrato}})</span>
					</div>
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab_1_1" data-toggle="tab">Contrato</a></li>
						<li><a href="#tab_1_2" data-toggle="tab">Beneficiarios</a></li>
						@if($tipo != 1)
							<li><a href="#tab_1_3" data-toggle="tab">Cobros</a></li>
						@endif
						<li><a href="#tab_1_4" data-toggle="tab">Contratos Historial</a></li>
					</ul>
				</div>
				<div class="portlet-body">
					<div class="tab-content">
						<div class="tab-pane active" id="tab_1_1"> 
							<div class="form-body">	
								<div class="row">
									<div class="col-md-12">
										<table class="table table-striped table-hover " border="0">
											<tbody >
												<tr>
													<td width="30%"><b>Planilla:</b> <br><span id="planilla">{{$Contrato->planilla}}</span></td>
													<td width="20%"><b>N° C.I:</b><br> <span id="dni">{{$Persona->tipo_persona->nombre}}-{{$Persona->dni}} </span></td>
													<td width="50%"><b>Titular:</b><br> <span id="titular">{{$Persona->nombres}}</span></td>
												</tr>
												<tr>
													<td><b>Sucursal:</b><br> <span id ="sucursal">{{$Contrato->sucursal}}</span></td>
													<td><b>Renovaciones:</b><br> <span id ="renovaciones">{{$Contrato->renovaciones}}</span> </td>
													<td><b>Vendedor:</b><br> <span id="vendedor">{{$Contrato->vendedor}}</span></td>
												</tr>
												<tr >
													<td><b>Fecha de Contrato:</b><br> <span id ="inicio">{{$Contrato->inicio}}</span></td>
													<td></td>
													<td><b>Fecha Vencimiento:</b><br> <span id ="vencimiento">{{$Contrato->vencimiento}}</span> </td>
												</tr>

												<tr >
														<td><b>Total Beneficiarios:</b><br> <span id ="num_beneficiarios"> {{$beneficiario}}</span></td>
														<td></td>
														<td><b>Plan:</b><br> <span id ="plan">{{$plan}}</span> </td>
													
												</tr>

												<tr >
													<td><b>Total Contrato</b><br> <span id ="total_contrato">{{$Contrato->total_contrato}}</span> </td>
													<td><b>Total Pagado</b><br> <span id ="total_pagado">{{$Contrato->total_pagado}}</span> </td>
													@if($Contrato->tipo_pago == 2)
														<td><b>Banco</b><br> <span id ="banco">{{$Contrato->banco}}</span> </td>
													@else
														<td><b>Banco</b><br> <span id ="banco"></span> </td>
													@endif	
												</tr>
												<tr >
													@if($Contrato->tipo_pago == 1)
														<td><b>Tipo de pago:</b> <br><span id ="tipo_pago">De Contado</span></td>
													@else
														<td><b>Tipo de pago:</b> <br><span id ="tipo_pago">Credido</span></td>
														<?php
															$frecuencia= 'Mensual';
															if($Contrato->frecuencia == 'q'){
																$frecuencia = 'Quincenal';
															}

														?>
														<td><b>Frecuencia de pago:</b><br> <span id ="frecuencia_pagos"> {{$frecuencia}}: {{$Contrato->dias}}</span></td>
														<td><b>Cuenta Afiliada:</b><br> <span id ="personas_bancos_id">{{$Contrato->cuenta}}</span></td>
													@endif	
												</tr>
												<tr >
													@if($Contrato->tipo_pago == 2)
														<td><b>inicial:</b><br> <span id ="inicial">{{$Contrato->inicial}}</span></td>
														<td><b>fecha Inicial:</b><br> <span id ="fecha_incial">{{$Contrato->fecha_incial}}</span></td>
													@else
														<td><b>inicial:</b><br> <span id ="inicial"></span></td>
														<td><b>fecha Inicial:</b><br> <span id ="fecha_incial"></span></td>
													@endif	
														<td><b>Primer Cobro:</b> <br><span id ="primer_cobro">{{$Contrato->primer_cobro}}</span></td>
												</tr> 
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane " id="tab_1_2">
							<table class="table table-striped table-hover ">
								<thead>
									<tr>
										<th>#</th>
										<th>N°C.I</th>
										<th>Nombre y Apellido</th>
										<th>Sexo</th>
										<th>Fecha de Nacimiento</th>
										<th>Edad</th>
										<th>Parentesco</th>
									</tr>
								</thead>
								<tbody>
									<?php $num = 1?>
									@foreach($info_beneficiarios as $beneficiario)
										<tr>
											<td>{{$num++}}</td>
											<td>{{$beneficiario->nombre}}-{{$beneficiario->dni}}</td>
											<td>{{$beneficiario->nombres}}</td>

											@if($beneficiario->sexo == 'm')
												<td>Masculino</td>
											@else
												<td>Femenino</td>
											@endif
											<?php
										        $fecha_n = \Carbon\Carbon::parse($beneficiario->fecha_nacimiento)->format('d/m/Y');

											?>
											<td>{{$fecha_n}}</td>
											<?php

										        $date = \Carbon\Carbon::parse($beneficiario->fecha_nacimiento)->age;
											?>

											<td> {{$date}}</td>
											<td>{{$beneficiario->parentesco}}</td>
										</tr>

									@endforeach
								</tbody>
							</table> 
						</div> 
						<div class="tab-pane " id="tab_1_3">
							<center>
								<table id="tabla" class="table table-striped table-hover table-bordered tables-text">
									<thead>
										<tr>
											<th style="width: 10%; text-align: center;">Fecha solicitud</th>
											<th style="width: 15%; text-align: center;">Monto a pagar</th>
											<th style="width: 15%; text-align: center;">Fecha pagado</th>
											<th style="width: 15%; text-align: center;">Total pagador</th>
											<th style="width: 15%; text-align: center;">Tipo de pago</th>
											<th style="width: 15%; text-align: center;">Número de recibo</th>
											<th style="width: 20%; text-align: center;">Concepto</th>
										</tr>
									</thead>
								</table>
							</center>
							

						</div> 
						<div class="tab-pane " id="tab_1_4">
							<table class="table table-striped table-hover ">

								<thead>
									<tr>
										<th>#</th>
										<th>fecha</th>
										<th>Operacion</th>
										<th>Responsable</th>
										<th>Planilla</th>
									</tr>
								</thead>
								<tbody>
								<?php $num2 = 1;?>
									@foreach($contratos_detalles as $contrato_detalle )
										<tr>
											<td>{{$num2++}}</td>
											
											<?php
										        $fecha_n = \Carbon\Carbon::parse($contrato_detalle->created_at)->format('d/m/Y');

											?>
											
											<td>{{$fecha_n}}</td>
											<td>{{$contrato_detalle->operacion}}</td>
											<td>{{$contrato_detalle->nombre}}-{{$contrato_detalle->dni}} {{$contrato_detalle->nombres}}</td>
											<td>{{$contrato_detalle->planilla}}</td>
										</tr>
									@endforeach
								</tbody>
							</table> 
						</div> 
					</div>
				</div>
			</div>
		</div>
	</div>
	<input type="hidden" name="contrato_id" id="contrato_id" value="{{$Contrato->id}}">
	<input type="hidden" name="solicitud_id" id="solicitud_id" value="{{$solicitud_id}}">
	@if($tipo == 1)
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4">
				
				<a href="#" id='aceptar' class="btn btn-primary">Aceptar</a>

				<a href="#" id='rechazar' class="btn btn-danger">Rechazar</a> 
			</div>
			<div class="col-md-4"></div>
		</div>
	@elseif($tipo == 3)
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4">
				
				<a href="#" id='reenviar' class="btn btn-primary">Reenviar Solicitud</a>

			</div>
			<div class="col-md-4"></div>
		</div> 
	@endif
@endsection
@push('js')

<script type="text/javascript">
        var contrato_id = "{{ $Contrato->id}}";
</script>
@endpush