@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Contratos']])
    
@endsection
@section('content')
    {!! Form::open(['id' => 'submit_form', 'name' => 'formulario', 'method' => 'POST' ]) !!}
    	<input type="hidden" name="tipo_contrato" value="{{$tipo}}">

    	<div class="col-md-12">
	        <div class="portlet light" id="form_wizard_1">
	            <div class="portlet-title">
	                <div class="caption">
	                    <i class=" icon-layers font-red"></i>
	                    <span class="caption-subject font-red bold uppercase"> Nuevo Contrato
	                        <span class="step-title"> 1 de 4 </span>
	                    </span>
	                </div>
	            </div>
	            <div class="portlet-body form">
	                
	                    <div class="form-wizard">
	                        <div class="form-body">
	                            <ul class="nav nav-pills nav-justified steps">
	                                <li>
	                                    <a href="#tab1" data-toggle="tab" class="step">
	                                        <span class="number"> 1 </span>
	                                        <span class="desc">
	                                            <i class="fa fa-check"></i>Datos Generales</span>
	                                    </a>
	                                </li>
	                                <li>
	                                    <a href="#tab2" data-toggle="tab" class="step">
	                                        <span class="number"> 2 </span>
	                                        <span class="desc">
	                                            <i class="fa fa-check"></i> Titular</span>
	                                    </a>
	                                </li>
	                                <li>
	                                    <a href="#tab3" data-toggle="tab" class="step active">
	                                        <span class="number"> 3 </span>
	                                        <span class="desc">
	                                            <i class="fa fa-check"></i> Planes y tipos de Pagos</span>
	                                    </a>
	                                </li>
	                                
	                                <li>
	                                    <a href="#tab4" data-toggle="tab" class="step active">
	                                        <span class="number"> 4 </span>
	                                        <span class="desc">
	                                            <i class="fa fa-check"></i> Beneficiarios </span>
	                                    </a>
	                                </li>
	                                
	                            </ul>
	                            <div id="bar" class="progress progress-striped" role="progressbar">
	                                <div class="progress-bar progress-bar-success"> </div>
	                            </div>
	                            <div class="tab-content">
	                                <div class="alert alert-danger display-none">
	                                    <button class="close" data-dismiss="alert"></button> Usted tiene algunos errores en el formulario. Por favor, compruebe.
	                                </div>
	                                <div class="alert alert-success display-none">
	                                    <button class="close" data-dismiss="alert"></button> ¡La validación de su formulario es exitosa!
	                                </div>
	                                
	                                <div class="tab-pane active" id="tab1">
						                <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
											<label for="min">Fecha del Contrato</label>
											<input id="fecha" name="inicio" class="form-control" type="text" placeholder=""  required="date" />
										</div>

										{{ Form::bsSelect('sucursal_id', $controller->sucursales(), '', [
											'label'    => 'Sucursal',
											'required' => 'required'
										]) }}

										@if (auth()->user()->super === 's')
											{{ Form::bsSelect('vendedor_id', [], '', [
											'label'    => 'Vendedor',
											'required' => 'required'
										]) }}
										
										@endif

										{{ Form::bsNumber('planilla', '',[
											'label'    => 'N° de Planilla',
											'required' => 'required'
										]) }}    
	                                   
	                                </div>

	                                <div class="tab-pane" id="tab2">
	                                	<div class="form-group col-md-3 cont-persona">
											<label for="nombres">Cedula:</label>
											<div class="form-group multiple-form-group input-group">
						                       {{--  <div class="input-group-btn input-group-select">
						                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
						                                <span class="concept">-</span>
						                                <span class="caret"></span>
						                            </button>
						                            <ul class="dropdown-menu" role="menu">
						                                @foreach ($controller->tipo_persona() as $id => $tipo)
						                                <li><a href="{{ $id }}">{{ $tipo }}</a></li>
						                                @endforeach
						                            </ul>

													<input id="tipo_persona" name="tipo_persona" class="input-group-select-val" type="hidden" />
						                        </div> --}}
						                        {{ Form::text('dni', '', [
													'id'          => 'dni',
													'class'       => 'form-control',
													'placeholder' => '',
													'label'		  => 'C.I', 
													'required' => 'required'

									        	]) }}
						                        <span class="input-group-btn">
						                            <button id="btn-buscar-persona" type="button" class="btn btn-primary btn-add"><i class="fa fa-search"></i></button>
												</span>
							                 </div>
										</div>
											{{ Form::bsText('nombres', '', [
												'class'       => 'form-control',
												'label'       => 'Nombres y Apellidos:',
												'class_cont'  => 'col-md-6',
												'placeholder' => '',
												'required' => 'required'
								        	]) }}
								    
										{{ Form::bsSelect('goza', [
												'0'=>'Si',
												'1'=>'No'

											], '', [
											'label'    => 'Goza de los beneficios',
											'required' => 'required'
										]) }}		
	                                </div>

	                                <div class="tab-pane" id="tab3">
	                                	
	                                	{{ Form::bsSelect('planes_id', [], '', [
											'label'    => 'Planes',
											'required' => 'required'
										]) }}

										{{ Form::bsSelect('Num_benefe', $controller->numero, '', [
											'label'    => 'N° de Beneficiarios',
											'required' => 'required'
										]) }}

										{{ Form::bsSelect('frecuencia', [
												'm'=>'Mensual',
												'q'=>'Quincenal'

											], '', [
											'label'    => 'Frecuencia de Pago',
											'required' => 'required'
										]) }}

										{{ Form::bsText('meses', '', [
											'class'       => 'form-control',
											'label'       => 'Meses para Pagar',
											'disabled' => 'disabled'
							        	]) }}  
							        	<button type="button" id="calcular"  class="btn btn-info col-md-12"> Calcular</button>
                							
								        <div class="col-md-12"></div>
							        	<div class="col-md-12"></div>
							        	<div class="col-md-12">
							        		
							        		<center>					
												<span class="caption-subject font-red bold uppercase"> 
													De Contado
							                    </span>
					                   		 </center>	
							        	</div>
							        							 
								  
						            	<div class="table-scrollable">
						            		
						               		<table id="tabla-plan" class="table table-striped table-hover table-bordered">
							                    <thead>
							                        <tr>
							                            <th>(-) %</th>
							                            <th>Bs</th>
							                        </tr>
							                    </thead>
							                    <tbody>
							                        <tr>
							                            <td><input name="" id="porsentaje" class="form-control" type="text" disabled="" /></td>
							                            <td><input name="" id="contado" class="form-control" type="text" disabled=""/></td>
							                           
							                        </tr>
							                       
							                    </tbody>
							                </table>
						            	</div>

						            	<center>
							            	<span class="caption-subject font-red bold uppercase"> 
												Credito
						                    </span>
					                    </center>
										<div class="table-scrollable">
						               		<table id="tabla-plan" class="table table-striped table-hover table-bordered">
							                    <thead>
							                        <tr>
							                            <th>Inicial Credito</th> 
							                            <th>N. de Giros</th>
							                            <th>Monto por Giros (Bs)</th>
							                            <th>Total 1 A&ntilde;o (Bs)</th>
							                        </tr>
							                    </thead>
							                    <tbody>
							                        <tr>
							                            <td><input name="" id="inicial" class="form-control" type="text" disabled=""/></td>
							                           
							                            <td><input name="" id="n_giros" class="form-control" type="text" disabled=""/></td>
							                            <td><input name="" id="giro_bs" class="form-control" type="text" disabled=""/></td>
							                            <td><input name="" id="total" class="form-control" type="text" disabled=""/></td>
							                        </tr>
							                       
							                    </tbody>
							                </table>
						            	</div>	 

						            	{{ Form::bsSelect('tipo_pago', [
												'1'=>'De Contado',
												'2'=>'Credito'					
											], '', [
											'label'    => 'Tipo de Pago',
											'required' => 'required'
										]) }}

										<div class="col-md-12"></div>

										<div class="col-md-12" id="credito">


											<div class="form-group col-md-12 col-sm-6 col-xs-12">
											    <label for="personas_bancos_id" class="requerido">Número de Cuenta</label>
											   <select name="personas_bancos_id" id="personas_bancos_id" class="form-control 'col-md-4 col-sm-6 col-xs-12'" required="">
												  	<option value="">seleccione</option>
												</select>
											</div>

											<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
												<label for="min">Fecha de Cobro de Inicial</label>
												<input id="fecha2" name="fecha_incial" class="form-control" type="text" placeholder="" required="" />
											</div>

											{{ Form::bsSelect('frecuencia_pagos_id', [], '', [
												'label'    => 'Dias de Cobros',
												'required' => 'required',
												'class_cont' => 'col-md-4 col-sm-6 col-xs-12'
											]) }}

										</div>

										<div class="col-md-12" id="decontado">
											{{ Form::bsText('fecha_decontado', '', [
								            'label'         => 'Fecha',
								            'placeholder'   => 'Fecha',
								            'required'      => 'required',
								            'class_cont'    => 'col-md-4 col-sm-6 col-xs-12'
								        	]) }}

								        	{{ Form::bsNumber('n_recibo', '',[
											'label'    => 'N° de Recibo',
											'required' => 'required'
											]) }} 

											{{ Form::bsSelect('pago', [
												'0'=>'Por Tarjeta',
												'1'=>'Transferencia',
												'2'=>'Efectivo'					
											], '', [
											'label'    => 'Tipo de Pago',
											'required' => 'required'
											]) }}

										</div>				
	                                </div>

	                                <div class="tab-pane" id="tab4">
						               	<div class="col-md-12"></div>
						               	<table  border='1' class="table table-striped table-hover" id="tabla-beneficiarios">
						                	<thead>
						                		<tr>
						                			<th>DNI</th>
						                			<th>Nombres</th>
						                			<th>Sexo</th>
						                			<th>Fecha de Nacimiento</th>
						                			
						                			<th>Parentesco</th>
						                		
						                		</tr>
						                	</thead>
						                	<tbody id="beneficiarios"></tbody>
						                </table>
	                            	</div>      
	                        </div>
	                        <div class="form-actions">
	                            <div class="row">
	                                <div class="col-md-offset-3 col-md-9">
	                                    <a href="javascript:;" class="btn default button-previous">
	                                        <i class="fa fa-angle-left"></i> Atras </a>
	                                    <a href="javascript:;" class="btn btn-outline green button-next"> Siguiente
	                                        <i class="fa fa-angle-right"></i>
	                                    </a>
	                                    <button type="" class="btn green button-submit" id="guardar"> Guardar <i class="fa fa-check"></i></button>
	                                    
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                
	            </div>
	        </div>
	    </div>	
    {!! Form::close() !!}
@endsection
@push('css')
<style type="text/css">
#tabla-plan thead{
    background-color: white;
}
.form-body{
	overflow: hidden;
}
.tcentro{
    text-align: center;
    vertical-align: middle !important;
}

#tabla-plan input{
    width: 100%;
}
#ui-datepicker-div{
	z-index: 3 !important;
}
</style>
@endpush
@push('js')
<script s type="text/javascript" charset="utf-8" async defer>
	
	$super = '{{auth()->user()->super}}';

</script>
<script type="text/x-tmpl" id="tmpl-demo2">
		<tr>
		    <td>
			   	<input type="text"  id="dni_beneficiario" class="form-control" name="dni_beneficiario[]" placeholder="Dni" value="">
			</td> 
			<td>
			   <input type="text" value=""  id="nombres_beneficiario" class="form-control" name="nombres_beneficiario[]" placeholder="Nombres y Apellidos">	
			</td>
			<td>
				<select name="sexo_beneficiario[]"  id="sexo_beneficiario" class="form-control">
					<option value="m">Masculino</option>
					<option value="f">Femenino</option>
				</select>
			</td>
			<td>
				<input  value="" class="form-control nacimiento_beneficiario" id="nacimiento_beneficiario" type="text" name='nacimiento_beneficiario[]' placeholder="" />
			</td>
		    <td>
				<select name="parentescos_beneficioario[]" id="parentescos_beneficioario" class="form-control">
				  	<option value="">seleccione</option>
				  	@foreach($controller->parentescos() as $parentesco)
				  		<option value="{{$parentesco->id}}">{{$parentesco->nombre}}</option>
				  	@endforeach
				</select>
			</td> 
		</tr>
</script>
@endpush