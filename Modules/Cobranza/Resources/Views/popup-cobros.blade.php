@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Contratos Cobros']])

    
@endsection
@section('content')
	<div class="row"> 					
		<center><table id="tabla" class="table table-striped table-hover table-bordered tables-text">
			<thead>
				<tr>
					<th style="width: 10%; text-align: center;">Fecha solicitud</th>
					<th style="width: 15%; text-align: center;">Monto a pagar</th>
					<th style="width: 15%; text-align: center;">Fecha pagado</th>
					<th style="width: 15%; text-align: center;">Total pagador</th>
					<th style="width: 15%; text-align: center;">Tipo de pago</th>
					<th style="width: 15%; text-align: center;">Número de recibo</th>
					<th style="width: 20%; text-align: center;">Concepto</th>
				</tr>
			</thead>
		</table></center>
	</div>	
@endsection
@push('js')
	<script type="text/javascript">
		 var contrato_id = "{{ $contrato_id}}";
	</script>

@endpush
