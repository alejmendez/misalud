@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Refinanciamiento']])

    
@endsection
@section('content')
	<div class="row"> 
    	<input type="hidden" name="id" value="{{$contrato_id}}" id="id">
        <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <label for="min">Fecha  Inicio</label>
            <input id="fecha" name="inicio" class="form-control" type="text" placeholder=""  required="date" />
        </div>			
		
	</div>	
@endsection
@push('js')
	<script type="text/javascript">
		 var contrato_id = "{{ $contrato_id}}";

        $("#fecha").datepicker({
          minDate: '0',
        });

        $('#guardar').on('click', function(){
            $.ajax({
                url: dire + '/contratos/analista/refinaciamientoguardar',
                type: 'POST',
                data: {
                    'id': contrato_id,
                    'fecha': $('#fecha').val(),
                },
                success: function(r) {
                    aviso(r);
                    window.opener.aplicacion.buscar(contrato_id);
                    window.close();
                }
            });
         });

	</script>

@endpush
