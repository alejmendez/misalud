@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Frecuencia Pagos']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar FrecuenciaPagos.',
        'columnas' => [
            'Frecuencia' => '50',
		'Dias' => '50'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $FrecuenciaPagos->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection