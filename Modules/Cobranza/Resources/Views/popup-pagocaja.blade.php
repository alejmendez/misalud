@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Pagos Contrato']])
    
@endsection
@section('content')
	{!! Form::open(['id' => 'submit_form', 'name' => 'formulario', 'method' => 'POST' ]) !!}
		<div class="row"> 
			{{ Form::bsSelect('sucursal_id', $controller->sucursales(), '', [
				'label'    => 'Sucursal',
				'required' => 'required'
			]) }}
			<input type="hidden" name="id" id="id" value="{{$contrato->id}}">
			<input type="hidden" name="pagos_pendientes" id="pagos_pendientes" value="">
		</div>
		<div class="row">
			<div class="portlet box green" id="empresas">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-table"></i>Pagar</div>
					<div class="tools">
						<a href="javascript:;" class="collapse"> </a>
					</div>
				</div>
				<div class="portlet-body flip-scroll">
				<center>
						<table class="table table-striped table-hover table-bordered tables-text">
							<thead>
								<tr>
									@if($inicial == 's')
										<th style="width: 50%; text-align: center;">Pagar Inicial</th>
									@endif
									<th style="width: 50%; text-align: center;">Cuotas ha Pagar</th>
									<input type="hidden" name="inicial" id="_inicial" value="{{$inicial}}">
								</tr>
							</thead>
							<tbody>
								<tr>
									@if($inicial == 's')
										<td style="width: 50%; text-align: center;">
											<input id="inicial" type="checkbox" class="make-switch" data-on-text="Si" data-off-text="No">	
										</td>
									@endif
									<td style="width: 50%; text-align: center;">
										<input id="cuotas" type="text" value=""  name="cuotas" read="">
									</td>
								</tr>
							</tbody>
						</table>
					</center>
						
					
				</div>
			</div>
		</div>
		{{--  tabla de coutas pendientes o enviadas  --}}
		<div class="row">
			<div class="portlet box green" id="">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-table"></i>Cargos pendientes </div>
					<div class="tools">
						<a href="javascript:;" class="collapse"> </a>
					</div>
				</div>
				<div class="portlet-body flip-scroll">
					<table id="cargos_pendiente" class="table table-striped table-hover table-bordered ">
						<thead>
							<tr>
								<th style="width: 20px; text-align: center;">&nbsp;</th>
								<th style="text-align: center;">Fecha de cobro</th>
								<th style="text-align: center;">Monto a cobrar</th>
								<th style="text-align: center;">Fecha pago</th>
								<th style="text-align: center;">Pago recibido</th>
								<th style="text-align: center;">Saldo pendiente</th>
								<th style="text-align: center;">Estatus</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
		{{--  nuevos cobros  --}}
		<div class="row">
			<div class="portlet box green" id="">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-table"></i>Cargos Efectuados </div>
					<div class="tools">
						<a href="javascript:;" class="collapse"> </a>
					</div>
				</div>
				<div class="portlet-body flip-scroll">
					<center>
						<table id="" class="table table-striped table-hover table-bordered ">
							<thead>
								<tr>
									<th style="width: 10%; text-align: center;">N°</th>
									<th style="width: 40%; text-align: center;">Concepto</th>
									<th style="width: 40%; text-align: center;">Monto</th>
								</tr>
							</thead>
							<tbody id="cargos">
								
							</tbody>
						</table>
					</center>
						
					
				</div>
			</div>
		</div>

		<div class="row">
			<div class="portlet box green" id="empresas">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-table"></i>Informacion del Pago </div>
					<div class="tools">
						<a href="javascript:;" class="collapse"> </a>
					</div>
				</div>
				<div class="portlet-body flip-scroll">
					<center>
						<table id="" class="table table-striped table-hover table-bordered tables-text">
							<thead>
								<tr>
									<th style="width: %; text-align: center;">N° Recibo</th>
									<th style="width: %; text-align: center;">Tipo De Pago</th>
									<th style="width: %; text-align: center;">Total</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<input id="recibo" type="text" value=""  id="recibo" name="recibo"class="form-control"  >
									</td>
									<td>
										<select name="tipo_pago" class_cont="col-lg-3 col-md-4 col-sm-6 col-xs-12" id="ano" required="required" class="form-control">
											<option value="Efectivo" selected="selected">Efectivo</option>
											<option value="Tarjeta">Tarjeta</option>
											<option value="Transferencia">Transferencia</option>
										</select>
									
									</td>

									<td id="total">
									
									</td>
								</tr>
							</tbody>
						</table>
					</center>
				</div>
			</div>
		</div>
	{!! Form::close() !!}
@endsection

@push('js')
<script type="text/javascript">
        var $inicial = {{ $contrato->inicial }};
		var $cuota = {{ round(($contrato->total_contrato - $contrato->inicial)  / $contrato->cuotas, 2) }};
		var $pendientes = {{ $cuotas_pendientes}}; 
</script>
@endpush
