<?php

namespace Modules\Cobranza\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'cobranza');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'cobranza');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations', 'cobranza');
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
