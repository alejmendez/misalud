var op = 0,
    tabla;
$(function() {

    $('.confirm').live('click', function() {
        cerrar_windows(op)
    });

    tabla = datatable('#tabla', {
        ajax: dire + '/contratos/analista/cobrostable/' + contrato_id,
        columns: [
            { "data": "total_cobrar", "name": "total_cobrar" },
            { "data": "created_at", "name": "created_at" },
            { "data": "fecha_pagado", "name": "fecha_pagado" },
            { "data": "total_pagado", "name": "total_pagado" },
            { "data": "tipo_pago", "name": "tipo_pago" },
            { "data": "num_recibo", "name": "num_recibo" },
            { "data": "concepto", "name": "concepto" }
        ]
    });

    $('#aceptar').on('click', function() {

        swal({
                title: "",
                text: "Esta Seguro que Desea Aceptar y Activar EL Contrato?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-success",
                cancelButtonClass: "btn-danger",
                confirmButtonText: "Si",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false,
                showLoaderOnConfirm: true
            },
            function(isConfirm) {
                if (isConfirm) {
                    confirmacion($('#contrato_id').val(), 1, '', $('#solicitud_id').val());

                } else {
                    swal("Cancelado", "", "error");
                }
            });

    });
    $('#reenviar').on('click', function() {
        console.log($('#contrato_id').val());
        swal({
                title: "",
                text: "Esta Seguro que Desea Reenviar la solicitud?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-success",
                cancelButtonClass: "btn-danger",
                confirmButtonText: "Si",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false,
                showLoaderOnConfirm: true
            },
            function(isConfirm) {
                if (isConfirm) {
                    reenviar($('#contrato_id').val());

                } else {
                    swal("Cancelado", "", "error");
                }
            });

    });

    $('#rechazar').on('click', function() {

        swal({
                title: "",
                text: "Esta Seguro que Desea Rechazar el Contrato?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-success",
                cancelButtonClass: "btn-danger",
                confirmButtonText: "Si",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    swal({
                        title: "",
                        text: "Motivo de Rechazo",
                        type: "input",
                        showCancelButton: true,
                        closeOnConfirm: false,
                        inputPlaceholder: "Write something"
                    }, function(inputValue) {
                        if (inputValue === false) return false;
                        if (inputValue === "") {
                            swal.showInputError("Debe Introducir motivo de Rechazo");
                            return false
                        }
                        swal("", " Contrato Rechazado", "success");
                        confirmacion($('#contrato_id').val(), 2, inputValue, $('#solicitud_id').val());
                    });
                } else {
                    swal("Cancelado", "", "error");
                }
            });
    });

});

function confirmacion($id, $confirmacion, result, solicitud_id) {

    $.ajax({
        url: dire + '/contratos/contrato/confirmacion',
        data: {
            'id': $id,
            'confirmacion': $confirmacion,
            'descripcion': result,
            'solicitud_id': solicitud_id
        },
        type: 'POST',
        success: function(r) {

            if ($confirmacion == 1) {
                swal({
                    title: "Listo",
                    text: "Esta Seguro que Desea Aceptar y Activar EL Contrato?",
                    type: "success"
                });

            }

            op = 1;


        }
    });
}

function cerrar_windows(op) {
    if (op == 1) {
        window.opener.location.reload();
        window.close();
    }

}

function reenviar($id) {

    $.ajax({
        url: dire + '/contratos/opcontratos/reenviarsolicitud',
        data: {
            'id': $id
        },
        type: 'POST',
        success: function(r) {
            cerrar_windows(1);
        }
    });
}