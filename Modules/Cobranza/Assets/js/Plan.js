var aplicacion, $form, tabla, $tablaPlan;
$(function() {
	aplicacion = new app('formulario', {
		'limpiar' : function(){
			tabla.fnDraw();
			
			var $tablaPlan = $('#tabla-plan');
			$(".id", $tablaPlan).val(0);
		},
		'buscar' : function(r){
			var $tablaPlan = $('#tabla-plan');

			for (var i in r.detalle) {
				var detalle = r.detalle[i],
				    fila = $('.fila' + detalle.beneficiarios, $tablaPlan);
				
				$.each(detalle, function(id, valor){
					$('.' + id, fila).val(valor);
				});
			}
		}
	});

	$form = aplicacion.form;

	tabla = datatable('#tabla', {
		ajax: $url + "datatable",
		columns: [
			{"data" : "nombre", "name" : "nombre"},
			{"data" : "desde",  "name" : "desde"},
			{"data" : "hasta",  "name" : "hasta"}
		]
	});
	
	$('#tabla').on("click", "tbody tr", function(){
		aplicacion.buscar(this.id);
	});

	$tablaPlan = $('#tabla-plan');

	$("#cuotas_especiales").blur(function(){
		var cuotas_especiales = $(this).val();

		$('input[name="precios_especial[]"]', $tablaPlan).each(function(){
			var tr = $(this).parents('tr'),
				target = $(this),
				total = $('input[name="total[]"]', tr).val();

			if (total <= 0) {
				return;
			}
			
			target.val(round(total * 0.6 / cuotas_especiales, 2, 'PHP_ROUND_HALF_UP'));
		});
	});

	$('input[name="porsertaje_descuento[]"]', $tablaPlan).blur(function() {
		var tr = $(this).parents('tr'),
			target = $('input[name="contado[]"]', tr),
			valor = $(this).val(),
			total = $('input[name="total[]"]', tr).val();

		if (total <= 0) {
			target.val('');
			$(this).val('');
			return;
		}
		
		target.val(round(total - (total * valor / 100), 2, 'PHP_ROUND_HALF_UP'));
	});

	$('input[name="giros[]"]', $tablaPlan).blur(function() {
		var tr = $(this).parents('tr'),
			target = $('input[name="precios_giros[]"]', tr),
			valor = $(this).val(),
			total = $('input[name="total[]"]', tr).val();

		if (total <= 0) {
			target.val('');
			$(this).val('');
			return;
		}
		
		target.val(round(total / valor, 2, 'PHP_ROUND_HALF_UP'));
	});

	$('input[name="total[]"]', $tablaPlan).blur(function() {
		var tr = $(this).parents('tr'),
			target = $('input[name!="total[]"]', tr),
			valor = $(this).val();

		if (valor <= 0) {
			target.val('');
			$(this).val('');
		}
	});
});

function round (value, precision, mode) {
  var m, f, isHalf, sgn // helper variables
  // making sure precision is integer
  precision |= 0
  m = Math.pow(10, precision)
  value *= m
  // sign of the number
  sgn = (value > 0) | -(value < 0)
  isHalf = value % 1 === 0.5 * sgn
  f = Math.floor(value)
  if (isHalf) {
    switch (mode) {
      case 'PHP_ROUND_HALF_DOWN':
      // rounds .5 toward zero
        value = f + (sgn < 0)
        break
      case 'PHP_ROUND_HALF_EVEN':
      // rouds .5 towards the next even integer
        value = f + (f % 2 * sgn)
        break
      case 'PHP_ROUND_HALF_ODD':
      // rounds .5 towards the next odd integer
        value = f + !(f % 2)
        break
      default:
      // rounds .5 away from zero
        value = f + (sgn > 0)
    }
  }
  return (isHalf ? value : Math.round(value)) / m
}