var $num = 1,
    $total = 0,
    $cantidad_cuotas = 0,
    $minimo = 0;

$(function() {
    debe();

    $('#cuotas').on('change', function() {
        var cuota = $('#cuotas').val();

        if (cuota <= $pendientes) {
            cargos('cuota', $cantidad_cuotas > cuota ? 'menos' : 'mas');
            $cantidad_cuotas = cuota;
        }
    });

    $('#inicial').bootstrapSwitch('state', false, true);
    $('#inicial').on('switchChange.bootstrapSwitch', function(event, state) {
        $opera = 'menos';
        $('#_inicial').val('n');
        if (state) {
            $opera = 'mas';
            $('#_inicial').val('s');
        }
        cargos('inicial', $opera);
    });

    $('#guardar').on('click', function() {
        $('#submit_form').ajaxSubmit({
            'url': dire + '/contratos/pagos/guardar',
            'type': 'POST',
            'success': function(r) {
                aviso(r);
            }
        });

        return false;
    });

    $('tbody', '#cargos_pendiente').on('click', 'tr', function(e){
        var t = $(this),
            id         = t.data('id'), 
            fecha      = $('td:eq(1)', t).text(),
            monto      = $('td:eq(2)', t).text(),
            fecha_pago = $('td:eq(3)', t).text(),
            pago       = $('td:eq(4)', t).text(),
            pendiente  = $('td:eq(5)', t).text(),
            estatus    = $('td:eq(6)', t).text();
            
        monto      = to_number(monto);
        pago       = to_number(pago);
        pendiente  = to_number(pendiente);

        if (!$(e.target).is('input')) {
            $('input', t).prop('checked',  !$('input', t).prop('checked'));
        }

        cargos();
    });

});

function debe() {
    $.ajax({
        url: dire + '/contratos/pagos/debe',
        type: 'POST',
        data: {
            'id': $('#id').val(),
        },
        success: function(r) {
            $_cuota = 0;
            $_inicial = false;
            for (var i in r) {
                if (r[i].total_cobrar == $inicial) {
                    $debe_tipo = 'inicial';
                    $('#inicial').click();
                    $('#_inicial').val('s');
                }

                if (r[i].total_cobrar == $cuota) {
                    $debe_tipo = 'cuota';
                    $_cuota++;
                    $('#cuotas').val($_cuota);
                }

                $estatus = parseFloat(r[i].total_pagado) == 0 ? "En espera de cobro" : "Cobro parcial";

                $fecha = r[i].fecha_pagado || '';

                $('#cargos_pendiente tbody').append(
                    '<tr data-id="' + r[i].id + '">' + 
                        '<td><input type="checkbox" /></td>' +
                        '<td>' + r[i].created_at + '</td>' +
                        '<td>' + number_format(r[i].total_cobrar, 2) + '</td>' +
                        '<td>' + $fecha + '</td>' +
                        '<td>' + number_format(r[i].total_pagado, 2) + '</td>' +
                        '<td>' + number_format(r[i].total_cobrar - r[i].total_pagado, 2) + '</td>' +
                        '<td>' + $estatus + '</td>' +
                    '</tr>'
                );
            }
            $("#cuotas").TouchSpin({
                'min': $_cuota,
                'max': $pendientes
            });
        }
    });
}

function cargos($tipo, $operacion) {
    $concepto = '';
    $pagar = 0;

    switch ($tipo) {
        case 'inicial':
            $concepto = 'Pago de Inicial';
            $pagar = $inicial;
            $clase = 'inicial';
            break;
        case 'cuota':
            $concepto = 'Pago de Cuota';
            $pagar = $cuota;
            $clase = 'cuota';
            break;
        default:
            break;
    }

    if ($operacion == 'mas') {
        $num++;
        $total += $pagar;

        $('#cargos').append(
            '<tr class="' + $clase + '">' + 
                '<td>&nbsp;</td>' +
                '<td>' + $concepto + '</td>' +
                '<td>' + number_format($pagar, 2) + '</td>' +
            '</tr>'
        );
    } else {
        $num--;
        $monto = $clase == 'inicial' ? $inicial : $cuota;
        $total -= $monto;

        $('.' + $clase + ':last').remove();
    }

    var ids = [],
        pagos_pendientes = $("#pagos_pendientes");
    $('input:checked', '#cargos_pendiente').each(function(){
        var tr = $($(this).parents('tr')),
            id = tr.data('id'),
            pendiente  = to_number($('td:eq(5)', tr).text());
        
        $total += pendiente;
        
        ids.push(id);
    });
    
    pagos_pendientes.val(ids.join(','));
        
    $('#total').html(number_format($total, 2));
}

function to_number($var){
    $var = $var || '0';
    return +($var.replace(/\./g,'').replace(/,/g,'.'));;
}