var aplicacion, $form, datos_persona = [];
$(function() {
    aplicacion = new app('submit_form', {
        'limpiar': function() {
            $('#meses').val('');
            $('#porsentaje').val('');
            $('#contado').val('');
            $('#inicial').val('');
            $('#credito').css('display', 'none');
            $('#decontado').css('display', 'none');
        }
    });

    $form = aplicacion.form;

    $('#fecha').change(function() {
        validar($(this).val());
    });

    $('#guardar').on('click', function() {
        // aplicacion.guardar();
        $.ajax({
            url: dire + '/contratos/opcontratos/renovacion/guardar',
            type: 'post',
            data: $("#submit_form").serialize(),
            success: function(r) {

                //window.opener.location.reload();
                window.opener.aplicacion.buscar($('#id').val());
                window.close();
            }
        });

    });

    $('#calcular').on('click', function() {
        info_plan($('#planes_id').val(), $('#Num_benefe').val());
    });

    $('#tipo_pago').change(function() {

        $('#credito').css('display', 'none');
        $('#decontado').css('display', 'none');

        if ($(this).val() == 1) {
            $('#decontado').css('display', 'block');
        } else if ($(this).val() == 2) {
            $('#credito').css('display', 'block');
        } else {

            $('#credito').css('display', 'none');
            $('#decontado').css('display', 'none');
        }

    });

    $("#fecha", $form).datepicker({
        maxDate: "+0d"
    });

    $("#fecha2", $form).datepicker({
        maxDate: "+15d"
    });

    $("#fecha_decontado", $form).datepicker({
        maxDate: "+0d"
    });

});

/// toca metodos de ContratosController
//valida DNI, RIF y CORREO
function validar($dato) {
    /*
    $dato = dato a validar
    $tipo = que campo validar
    */
    $.ajax({
        url: dire + '/contratos/contrato/planes',
        type: 'POST',
        data: {
            'dato': $dato,
        },
        success: function(r) {
            $('#planes_id').html('').append('<option value="">- Seleccione</option>');
            if (r.s == 'n') {
                $('#planes_id').html('').append('<option value="">- Seleccione</option>');

            }
            if (r.s == 's') {
                for (var i in r.planes_id) {
                    $('#planes_id').append('<option value="' + r.planes_id[i].id + '">' + r.planes_id[i].nombre + '</option>');
                }
            }
        }

    });
}

function info_plan($plan_id, $beneficiarios) {

    $.ajax({
        url: dire + '/contratos/contrato/infoplan',
        type: 'POST',
        data: {
            'plan_id': $plan_id,
            'beneficiarios': $beneficiarios,
            'frecuencia': $('#frecuencia').val()
        },
        success: function(r) {
            $('#meses').val(r.plan[0].meses_pagar);

            $('#porsentaje').val(r.plan_detalles.porsertaje_descuento + ' %');

            $('#contado').val(r.plan_detalles.contado2);

            $('#inicial').val(r.plan_detalles.inicial2);

            var cuotas = 0;

            switch ($('#frecuencia').val()) {
                case 'm':
                    // mensual
                    cuotas = 1 * r.plan[0].meses_pagar;

                    break;

                case 'q':
                    // mensaul
                    cuotas = 2 * r.plan[0].meses_pagar;
                    break;

                default:
                    // statements_def
                    cuotas = 0
                    break;
            }

            var $result = r.plan_detalles.total - r.plan_detalles.inicial;

            $('#n_giros').val(cuotas);

            $('#giro_bs').val($result / cuotas).maskMoney();

            $('#total').val(r.plan_detalles.total2);


            $('#cobros').html('').append('<option value="">- Seleccione</option>');

            aplicacion.rellenar({
                frecuencia_pagos_id: {}
            });

            for (var i in r.frecuencia) {
                $('#frecuencia_pagos_id').append('<option value="' + r.frecuencia[i].id + '">' + r.frecuencia[i].dias_str + '</option>');
            }
        }
    });
}