var aplicacion, $form, $numero = 0,
    datos_persona = [],
    p = false,
    id_contrato = '',
    $orinial = 0,
    $id = '';
$(function() {
    aplicacion = new app('submit_form', {
        'limpiar': function() {

        },
        'buscar': function(r) {

        },
    });
    buscar_template($id);

    $numero = $('#Num_benefe').val();
    $orinial = $('#Num_benefe').val();

    //fecha Inicio
    $("#fecha", $form).datepicker({
        maxDate: "+0d"
    });

    $('#guardar').on('click', function() {

        $('#submit_form').ajaxSubmit({
            'url': dire + '/contratos/opcontratos/guardar/beneficiarios',
            'type': 'POST',
            'success': function(r) {
                aviso(r);
                if (r.donde == 1) {
                    cerrar_windows(2);
                }
                cerrar_windows(1);
            }
        });
        return false;

    });

    $('#tabla-beneficiarios').on('click', '.eliminar', function() {

        if (p == true) {
            return false;
        }

        if ($numero == 1) {
            alert('Operacion No permitida');
            return false;
        }
        $(this).parents('tr').remove();
        $numero = $numero - 1;
        $('#Num_benefe').val($numero);

    });

    $('#calcular').click(function() {
        info_plan($('#planes_id').val(), $('#Num_benefe').val(), '');
    });

    $('.button-next').click(function() {
        $('#estatus').html('');
        $resultado = $numero - $orinial;
        $op = '';
        if ($resultado == 0) {
            $msj = "Cambio";
            $op = 0;

        } else if ($resultado < 0) {
            $msj = "Reduccion";

            $op = 1;

        } else if ($resultado > 0) {
            $msj = "Inclusion";

            $op = 2;
        }
        $('#estatus').append($msj);
        validar($('#fecha').val(), $id, $op);
    });


    $("#fecha2", $form).datepicker({
        maxDate: "+15d"
    });

    $(".nacimiento_beneficiario").live().datepicker({
        maxDate: "+0d"
    });

    $("#fecha_decontado", $form).datepicker({
        maxDate: "+0d"
    });

    $('#agregar').on("click", function() {

        if (p == true) {
            return false;
        }

        if ($numero <= 9) {
            $numero++;
            $("#beneficiarios").append(tmpl("tmpl-demo2"));
            $('#Num_benefe').val($numero);
        } else {
            alert('no puedes Tener mas Beneficiarios');
        }

    });

    $('#tipo_pago').change(function() {

        $('#credito').css('display', 'none');
        $('#decontado').css('display', 'none');

        if ($(this).val() == 1) {
            $('#decontado').css('display', 'block');
        } else if ($(this).val() == 2) {
            $('#credito').css('display', 'block');
        } else {
            $('#credito').css('display', 'none');
            $('#decontado').css('display', 'none');
        }

    });

    $('#frecuencia').change(function() {
        buscar_frecuencias($('#frecuencia').val());
    });

});


function validar($dato, $id, $op) {

    /*
    	op == cambio = 0. reduccion = 1  o inclusion = 2 

    	"Solo aplica a esta vista"
    */
    $('#opera').val($op);

    $.ajax({
        url: dire + '/contratos/opcontratos/planes',
        type: 'POST',
        data: {
            'dato': $dato,
            'id_contrato': $id,
            'op': $op,
            'beneficiarios': $numero
        },
        success: function(r) {
            $('#calcular').prop('disabled', false);
            $('#tipo_pago').prop('disabled', false);
            $('#planes_id').prop('disabled', false);
            $('#frecuencia').prop('disabled', false);

            $('#plan_old').css('display', 'none');
            aplicacion.rellenar({
                planes_id: {}
            });

            aplicacion.rellenar({
                personas_bancos_id: {}
            });

            limpiar();

            if ($op == 0) {

                pago_estatico(r);

            } else if ($op == 2) {
                plan_old(r);
                for (var i in r.planes) {
                    $('#planes_id').append('<option value="' + r.planes[i].id + '">' + r.planes[i].nombre + '</option>');
                }

                $('#tipo_pago').val(r.contrato.tipo_pago);
                $('#tipo_pago').prop('disabled', true);

                if (r.frecuencia !== null) {
                    $('#frecuencia').val(r.frecuencia.frecuencia);
                    $('#tipo_pago').prop('readonly', true);
                }

                for (var i in r.cuentas) {
                    $('#personas_bancos_id').append('<option value="' + r.cuentas[i].id + '"> ' + r.cuentas[i].nombre + ' => ' + r.cuentas[i].cuenta + '</option>');
                }
            } else if ($op == 1) {
                plan_old(r);

                pago_estatico(r);

            }

        }

    });
}


function info_plan($plan_id, $beneficiarios, $valor_f) {

    $.ajax({
        url: dire + '/contratos/opcontratos/infoplan',
        type: 'POST',
        data: {
            'plan_id': $plan_id,
            'beneficiarios': $beneficiarios,
            'frecuencia': $('#frecuencia').val()

        },
        success: function(r) {

            $('#meses').val(r.plan[0].meses_pagar);

            $('#porsentaje').val(r.plan_detalles.porsertaje_descuento + ' %');

            $('#contado').val(r.plan_detalles.contado2);

            $('#inicial').val(r.plan_detalles.inicial2);

            var cuotas = 0;

            switch ($('#frecuencia').val()) {
                case 'm':
                    // mensual
                    cuotas = 1 * r.plan[0].meses_pagar;

                    break;

                case 'q':
                    // mensaul
                    cuotas = 2 * r.plan[0].meses_pagar;
                    break;

                default:
                    // statements_def
                    cuotas = 0
                    break;
            }

            var $result = r.plan_detalles.total - r.plan_detalles.inicial;

            $('#n_giros').val(cuotas);

            $('#giro_bs').val($result / cuotas).maskMoney();

            $('#total').val(r.plan_detalles.total2);


            $('#cobros').html('').append('<option value="">- Seleccione</option>');

            aplicacion.rellenar({
                frecuencia_pagos_id: {}
            });

            for (var i in r.frecuencia) {
                $('#frecuencia_pagos_id').append('<option value="' + r.frecuencia[i].id + '">' + r.frecuencia[i].dias_str + '</option>');
            }
            if ($valor_f != '') {
                $('#frecuencia_pagos_id').val($valor_f);
            }
        }
    });
}

function buscar_template($contrato_id) {
    //funcion que busca los los datos de los formularios con template
    //---------------------------------------------------------------------
    //busca informacion de los bancos

    $.ajax({
        'url': dire + '/contratos/opcontratos/beneficiario',
        'data': {
            'id': $contrato_id
        },
        'method': 'POST',
        'success': function(r) {

            $("#beneficiarios").html('');
            if (r.datos.length) {
                $("#beneficiarios").append(tmpl('tmpl-demo5', r));
            } else {
                //$("#"+$button).click();
            }
        }
    });

    //---------------------------------------------------------------------	
}

function confirmacion($id_contrato, $porque) {
    $.ajax({
        url: dire + '/contratos/contrato/opconsulta',
        data: {
            id: $id_contrato,
            porque: $porque
        },
        type: 'GET',
        success: function(r) {

            buscar(result);

        }
    });
}

function limpiar() {

    $('#planes_id').val('');

    $('#meses').val('');

    $('#porsentaje').val('');

    $('#contado').val('');

    $('#inicial').val('');

    $('#tipo_pago').val('');

    $('#frecuencia').val('');

    $('#n_giros').val('');
    $('#giro_bs').val('');

    $('#total').val('');

    $("#inf-plan").html('');
    $("#inf-tipo").html('');
    $("#inf-frecuencia").html('');
    $("#inf-cuotas").html('');
    $("#inf-total").html('');
}

function buscar_frecuencias($id) {
    $.ajax({
        url: dire + '/contratos/opcontratos/frecuencias',
        data: {
            id: $id,

        },
        type: 'POST',
        success: function(r) {

            aplicacion.rellenar({
                frecuencia_pagos_id: {}
            });

            for (var i in r) {
                $('#frecuencia_pagos_id').append('<option value="' + r[i].id + '">' + r[i].dias + '</option>');
            }

        }
    });
}

function plan_old($datos) {

    $('#plan_old').css('display', 'block');


    $("#inf-plan").append($datos.plan_old.nombre);

    var tipo = "Credido"

    $("#inf-total").append($datos.contrato.total_contrato2).maskMoney();
    $("#inf-total-pagado").append($datos.contrato.total_pagado2).maskMoney();

    if ($datos.contrato.tipo_pago == 1) {
        tipo = " De Contado";

        return false;
    }

    $("#inf-tipo").append(tipo);

    frecuencia = "Quincenal los: " + $datos.frecuencia.dias;

    if ($datos.frecuencia.frecuencia == 'm') {

        frecuencia = "Mensual los: " + $datos.frecuencia.dias;
    }

    $("#inf-frecuencia").append(frecuencia);

    var cuotas = 0;
    switch ($datos.frecuencia.frecuencia) {
        case 'm':
            // mensual
            cuotas = 1 * $datos.plan_old.meses_pagar;

            break;

        case 'q':
            // mensaul
            cuotas = 2 * $datos.plan_old.meses_pagar;
            break;

        default:
            // statements_def
            cuotas = 0
            break;
    }
    $result = $datos.contrato.total_contrato / cuotas;

    var $result = $datos.contrato.total_contrato - $datos.contrato.inicial;

    $("#inf-cuotas2").append(cuotas);
    $("#inf-cuotas").append($result / cuotas).maskMoney();

}

function pago_estatico($datos) {

    switch ($op) {
        case 0:
            $porsentaje = $datos.plan_detalles.porsertaje_descuento;
            $contado = $datos.plan_detalles.contado;
            $contado2 = $datos.plan_detalles.contado2;
            $inicial = $datos.plan_detalles.inicial;
            $inicial2 = $datos.plan_detalles.inicial2;

            $total = $datos.plan_detalles.total;
            $total2 = $datos.plan_detalles.total2;

            break;
        case 1:
            $porsentaje = $datos.plan_detalles_new.porsertaje_descuento;
            $contado = $datos.plan_detalles_new.contado;
            $contado2 = $datos.plan_detalles_new.contado2;
            $inicial = $datos.plan_detalles_new.inicial;
            $inicial2 = $datos.plan_detalles_new.inicial2;
            $total = $datos.plan_detalles_new.total;
            $total2 = $datos.plan_detalles_new.total2;
            break;
        default:
            // statements_def
            break;
    }


    $('#calcular').prop('disabled', true);

    $('#planes_id').append('<option value="' + $datos.planes.id + '">' + $datos.planes.nombre + '</option>');

    $('#planes_id').val($datos.planes.id);
    $('#planes_id').prop('disabled', true);

    $('#meses').val($datos.planes.meses_pagar);

    $('#porsentaje').val($porsentaje + ' %');

    $('#contado').val($contado2);

    $('#inicial').val($inicial2);


    $('#tipo_pago').val($datos.contrato.tipo_pago);

    $('#tipo_pago').prop('disabled', true);
    if ($datos.frecuencia !== null) {
        $('#frecuencia').val($datos.frecuencia.frecuencia);
    }

    $('#frecuencia').prop('disabled', true);


    $('#total').val($total2);

    var cuotas = 0;

    switch ($('#frecuencia').val()) {
        case 'm':
            // mensual
            cuotas = 1 * $datos.planes.meses_pagar;

            break;

        case 'q':
            // mensaul
            cuotas = 2 * $datos.planes.meses_pagar;
            break;

        default:
            // statements_def
            cuotas = 0
            break;
    }

    var $result = $total - $inicial;

    $('#n_giros').val(cuotas);
    // $('#giro_bs').val($result / cuotas);
    $('#giro_bs').val($result / cuotas).maskMoney();

    aplicacion.rellenar({
        frecuencia_pagos_id: {}
    });
}
/**/
function cerrar_windows(op) {
    if (op == 1) {
        window.opener.aplicacion.limpiar();
        window.opener.aplicacion.buscar($id);
    }
    window.close();

}