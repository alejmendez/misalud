var op = 0;
$(function() {

	$('.confirm').live('click', function(){
			cerrar_windows(op)
	});

	$('#aceptar').on('click', function(){

		swal({
		  title: "",
		  text: "Esta Seguro que Desea Aceptar y Activar EL Contrato?",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-success",
		  cancelButtonClass: "btn-danger",
		  confirmButtonText: "Si",
		  cancelButtonText: "No",
		  closeOnConfirm: false,
		  closeOnCancel: false,
		  showLoaderOnConfirm: true
		},
		function(isConfirm) {
		  if (isConfirm) {
			   	confirmacion($('#solicitud_id').val());

		  } else {
		    	swal("Cancelado", "", "error");
		  }
		});

  	});
 });

function confirmacion($id){

	$.ajax({
		url : dire + '/contratos/opcontratos/confirmacionbene',
		data: {
			'id' : $id
		},
		type : 'POST',
		success : function(r){
			
		aviso(r);

		window.opener.location.reload();
		window.close();

			
		}
	});
}
