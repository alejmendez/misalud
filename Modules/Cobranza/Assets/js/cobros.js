$(function() {

    tabla = datatable('#tabla', {
        ajax: dire + '/contratos/analista/cobrostable/' + contrato_id,
        columns: [
            { "data": "total_cobrar", "name": "total_cobrar" },
            { "data": "created_at", "name": "created_at" },
            { "data": "fecha_pagado", "name": "fecha_pagado" },
            { "data": "total_pagado", "name": "total_pagado" },
            { "data": "tipo_pago", "name": "tipo_pago" },
            { "data": "num_recibo", "name": "num_recibo" },
            { "data": "concepto", "name": "concepto" }
        ]
    });

});