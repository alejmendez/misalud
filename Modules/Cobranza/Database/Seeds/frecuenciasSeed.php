<?php

namespace Modules\Cobranza\Database\Seeds;

use Illuminate\Database\Seeder;
use DB;
use Modules\Cobranza\Model\FrecuenciaPagos;

class frecuenciasSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $frecuencias = [
        	['m', '05','Mensual los 05'], 	
        	['m', '10','Mensual los 10'], 	
        	['m', '20','Mensual los 20'], 	
        	['m', '25','Mensual los 25'], 	
        	['m', '30','Mensual los 30'], 	
        	['q', '08,23', 'Quincenal los 08 y 23'], 	
        	['q', '16,01', 'Quincenal los 01 y 16'], 	
        	['q', '13,28', 'Quincenal los 13 y 28'] 	
		
		];

		DB::beginTransaction();
		try{
			foreach ($frecuencias as $parentesco) {
				FrecuenciaPagos::create([
					'frecuencia'  => $parentesco[0],
					'dias'  	  => $parentesco[1],
					'dias_str'   => $parentesco[2]
				]);
			}
		}catch(Exception $e){
			DB::rollback();
			echo "Error ";
		}
		DB::commit();
    }
}
