<?php

namespace Modules\Cobranza\Database\Seeds;

use Illuminate\Database\Seeder;
use DB;
use Modules\Cobranza\Model\ContratoTipo;
class tipocontratoseed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
		$tipo_contratos = [ 	
			['Contrato 1'],
			['Contrato 2']
		];

		DB::beginTransaction();
		try{
			foreach ($tipo_contratos as $contrato) {
				ContratoTipo::create([
					'nombre'  => $contrato[0]
				]);
			}
		}catch(Exception $e){
			DB::rollback();
			echo "Error ";
		}
		DB::commit();
	
    }
}
