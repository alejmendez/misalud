<?php

namespace Modules\Cobranza\Database\Seeds;

use Illuminate\Database\Seeder;
use DB;
use Modules\Cobranza\Model\Parentesco;

class parentescoseed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		
		$parentescos = [ 	
			['ABUELA'],
			['ABUELO'],
			['CUÑADO'],
			['CUÑADA'],
			['DESCONOCIDO'],
			['SIN ESPECIFICAR'],
			['ESPOSO'],
			['ESPOSA'],
			['FAMILIAR LEJANO'],
			['HERMANO'],
			['HERMANA'],
			['HIJO'],
			['HIJA'],
			['NIETO'],
			['NIETA'],
			['NINGUNO'],
			['NUERA'],
			['YERNO'],
			['PAPA'],
			['MAMA'],
			['PRIMO'],
			['PRIMA'],
			['SOBRINO'],
			['SOBRINA'],
			['SUEGRO'],
			['SUEGRA'],
			['TIO'],
			['TIA'],
			['VECINO'],
			['VECINA'],
			['CONOCIDO'],
			['CONOCIDA'],
			['TITULAR'],
			['AMIGO'],
			['AMIGA']
		];

		DB::beginTransaction();
		try{
			foreach ($parentescos as $parentesco) {
				Parentesco::create([
					'nombre'  => $parentesco[0]
				]);
			}
		}catch(Exception $e){
			DB::rollback();
			echo "Error ";
		}
		DB::commit();
	

    }
}
