<?php

namespace Modules\Cobranza\Database\Seeds;

use Illuminate\Database\Seeder;
use DB;
use Modules\Cobranza\Model\EstatusContrato;

class estatuscontrotatoseed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $estatus_contratos = [ 	
			['Activo'],
			['En espera de Confirmacion'],
			['En Revision'],
			['Rechazado'],
			['Vencido']
		];

		DB::beginTransaction();
		try{
			foreach ($estatus_contratos as $contrato) {
				EstatusContrato::create([
					'nombre'  => $contrato[0]
				]);
			}
		}catch(Exception $e){
			DB::rollback();
			echo "Error ";
		}
		DB::commit();
	
    }
}
