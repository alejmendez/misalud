<?php 

namespace Modules\Cobranza\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CobranzaDatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call(parentescoseed::class);
		$this->call(tipocontratoseed::class);
		$this->call(estatuscontrotatoseed::class);
		$this->call(frecuenciasSeed::class);

		Model::reguard();
	}
}
