<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ContratosDetalles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contratos_detalles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contratos_id')->unsigned();
            $table->integer('personas_id')->unsigned();
            $table->text('operacion');
            $table->string('planilla', 10)->nullable();

            $table->timestamps();
            $table->softDeletes(); 

            $table->foreign('contratos_id')
                ->references('id')->on('contratos')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('personas_id')
                ->references('id')->on('personas')
                ->onDelete('cascade')->onUpdate('cascade');   

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contratos_detalles');
    }
}
