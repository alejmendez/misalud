<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cobros extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cobros', function (Blueprint $table) {
            $table->increments('id');
            //generado pago
            $table->integer('contratos_id')->unsigned();
            $table->integer('sucursal_id')->unsigned();
            $table->integer('personas_id')->unsigned();//titular
            $table->integer('personas_bancos_id')->unsigned()->nullable();
            $table->integer('bancos_id')->unsigned()->nullable();
            $table->decimal('total_cobrar', 15, 2);
            $table->text('concepto');//puede ser sistema / caja
           
            // pago
            $table->date('fecha_pagado')->nullable();
            $table->decimal('total_pagado', 15, 2)->nullable();
            $table->string('num_recibo', 20)->nullable();//solo los que son por caja
            $table->string('tipo_pago', 20)->nullable();//caja o sistema
            $table->boolean('completo')->default(false);// si el pago viene completo 1 y si no 
            $table->timestamps();

            $table->foreign('sucursal_id')
                ->references('id')->on('sucursal')
                ->onDelete('cascade')->onUpdate('cascade'); 

            $table->foreign('contratos_id')
                ->references('id')->on('contratos')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('personas_id')
                ->references('id')->on('personas')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('personas_bancos_id')
                ->references('id')->on('personas_bancos')
                ->onDelete('cascade')->onUpdate('cascade'); 

            $table->foreign('bancos_id')
                  ->references('id')->on('bancos')
                  ->onDelete('cascade')->onUpdate('cascade');  

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cobros');
    }
}
