<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Contratos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contratos', function (Blueprint $table) {
            $table->increments('id');
            //datos Generales
            $table->string('planilla', 10)->nullable();
            $table->integer('titular')->unsigned();
            $table->integer('vendedor_id')->unsigned();
            $table->integer('sucursal_id')->unsigned();
            $table->integer('empresa_id')->unsigned();
            
            //Fechas
            $table->date('cargado');
            $table->date('primer_cobro')->nullable();
            $table->date('vencimiento');
            $table->date('inicio');

            //datos Cobros
            $table->integer('plan_detalles_id')->unsigned()->nullable();
            $table->integer('frecuencia_pagos_id')->unsigned()->nullable();
            
            $table->integer('tipo_pago')->unsigned();

            $table->decimal('total_contrato', 15, 2); 
            $table->decimal('total_pagado', 15, 2);
            $table->integer('cuotas')->unsigned();
            $table->integer('cuotas_pagadas')->nullable();

            $table->integer('personas_bancos_id')->unsigned()->nullable();
            $table->integer('beneficiarios')->unsigned();

            $table->decimal('inicial', 15, 2)->nullable();
            $table->date('fecha_incial')->nullable();
            $table->date('fecha_pago')->nullable();

            //$table->decimal('precios_especial', 15, 2);
            //$table->integer('giros')->unsigned();
            //$table->decimal('precios_giros', 15, 2); 
            
            //estatus contratos

            $table->boolean('anulado')->default(false);
            $table->boolean('cobrando')->default(false);
            $table->integer('renovaciones')->default(0);
            $table->integer('estatus_contrato_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('sucursal_id')
                ->references('id')->on('sucursal')
                ->onDelete('cascade')->onUpdate('cascade'); 

            $table->foreign('frecuencia_pagos_id')
                ->references('id')->on('frecuencia_pagos')
                ->onDelete('cascade')->onUpdate('cascade');   

            $table->foreign('empresa_id')
                ->references('id')->on('empresa')
                ->onDelete('cascade')->onUpdate('cascade'); 

            
            $table->foreign('plan_detalles_id')
                ->references('id')->on('plan_detalles')
                ->onDelete('cascade')->onUpdate('cascade');   

            $table->foreign('vendedor_id')
                ->references('id')->on('vendedores')
                ->onDelete('cascade')->onUpdate('cascade'); 

            $table->foreign('estatus_contrato_id')
                ->references('id')->on('estatus_contrato')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('personas_bancos_id')
                ->references('id')->on('personas_bancos')
                ->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contratos');
    }
}