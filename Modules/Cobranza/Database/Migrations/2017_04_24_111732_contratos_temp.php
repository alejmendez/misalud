<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ContratosTemp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contratos_temp', function (Blueprint $table) {
            $table->increments('id');

            //datos Generales
            $table->string('planilla', 10)->nullable();
            $table->integer('contrato_id')->unsigned();
            $table->integer('solicitud_id')->unsigned();
            
            //Fechas
            $table->date('primer_cobro')->nullable();
            $table->date('vencimiento');
            $table->date('inicio');

            //datos Cobros
            $table->integer('plan_detalles_id')->unsigned()->nullable();
            $table->integer('frecuencia_pagos_id')->unsigned()->nullable();
            
            $table->integer('tipo_pago')->unsigned();

            $table->decimal('total_contrato', 15, 2); 
            $table->decimal('total_pagado', 15, 2);
            $table->integer('cuotas')->unsigned();
            $table->integer('cuotas_pagadas')->nullable();

            $table->integer('personas_bancos_id')->unsigned()->nullable();
            $table->integer('beneficiarios')->unsigned();
            $table->boolean('rechazado')->default(false);
                  
            $table->decimal('inicial', 15, 2)->nullable();
            $table->date('fecha_incial')->nullable();

            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contratos');
    }
}