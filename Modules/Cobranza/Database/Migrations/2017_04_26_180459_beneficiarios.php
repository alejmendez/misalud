<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Beneficiarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beneficiarios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contratos_id')->unsigned();
            $table->integer('personas_id')->unsigned();
            $table->integer('parentesco_id')->unsigned();
            $table->boolean('estatus')->default(false);

            $table->timestamps();
           // $table->softDeletes(); 

            $table->foreign('contratos_id')
                ->references('id')->on('contratos')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('personas_id')
                ->references('id')->on('personas')
                ->onDelete('cascade')->onUpdate('cascade');   


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('beneficiarios');
    }
}
