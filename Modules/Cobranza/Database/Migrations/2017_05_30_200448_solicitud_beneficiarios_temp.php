<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SolicitudBeneficiariosTemp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    
        Schema::create('solicitud_beneficiarios_temp', function (Blueprint $table) {
            $table->increments('id');
            $table->string('planilla', 10)->nullable();
            $table->integer('contratos_id')->unsigned();
            $table->integer('solicitud_id')->unsigned();
            $table->integer('planes_id')->nullable();
            $table->date('fecha');
            $table->integer('operacion')->unsigned();
            $table->timestamps();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitud_beneficiarios_temp');
    }
}
