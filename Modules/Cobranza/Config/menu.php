<?php

$menu['cobranza'] = [
	[
		'nombre' 	=> 'Cobranza',
		'direccion' => '#cobranza',
		'icono' 	=> 'fa fa-clipboard',
		'menu' 		=> [
			[
				'nombre' 	=> 'Inicio',
				'direccion' => 'cobranza/escritorio/analista',
				'icono' 	=> 'fa fa-plus-circle'
			],
			[
				'nombre' 	=> 'Contratos',
				'direccion' => '#contratos',
				'icono' 	=> 'fa fa-gear',
				'menu' 		=> [
					[
						'nombre' 	=> 'Definiciones',
						'direccion' => '#Definiciones',
						'icono' 	=> 'fa fa-gear',
						'menu' 		=> [
							[
								'nombre' 	=> 'Parentescos',
								'direccion' => 'contratos/parentescos',
								'icono' 	=> 'fa fa-users'
							],
							[
								'nombre' 	=> 'Tipos de Contratos',
								'direccion' => 'contratos/tipocontrato',
								'icono' 	=> 'fa fa-list-ul'
							],
							[
								'nombre' 	=> 'Frecuencias de pago',
								'direccion' => 'contratos/frecuencia',
								'icono' 	=> 'fa fa-list-ul'
							],
							[
								'nombre' 	=> 'Estatus Contratos',
								'direccion' => 'contratos/estatuscontratos',
								'icono' 	=> 'fa fa-list-ul'
							]
						]
					],
					[
						'nombre' 	=> 'Contratos',
						'direccion' => 'contratos/analista',
						'icono' 	=> 'fa fa-plus-circle'
					],
					[
						'nombre' 	=> 'Asistente de Creacion de Contratos',
						'direccion' => 'contratos/contrato',
						'icono' 	=> 'fa fa-plus-circle'
					]
				]
			]
			
		]
	]
];