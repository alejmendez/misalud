var id_contrato = '',
    result = '';
$(function() {

    //inicio();
    limpiar();
    $("#buscar").keypress(function(event) {
        if (event.which == 13) {
            if ($('#buscar').val() == '') {
                return false;
            }
            limpiar();
            buscar($('#buscar').val());
        }
    });

    $("#agregar_unidad").on('click', function() {

        if ($('#buscar').val() == '') {
            return false;
        }
        limpiar();
        buscar($('#buscar').val());
    });

    $('.opciones').live('change', function() {
        id_contrato = this.getAttribute('data-id');
        $_estatus = this.getAttribute('data-estatus');
    });

    $('.info').live('click', function() {
        var id = this.getAttribute('data-id');

        var win = window.open(dire + '/consulta/consulta/' + id, 'consulta', 'height=500,width=1000,resizable=yes,scrollbars=yes');
        win.focus();
        return false
    });
    $('.pagos').live('click', function() {

        if (perfil_nombre == "Consulta" || perfil_nombre == 'Vendedor') {
            return false;
        }

        var id = this.getAttribute('data-id');

        var win = window.open(dire + '/contratos/pagos/consulta/pagos/' + id, 'pagos', 'height=500,width=1000,resizable=yes,scrollbars=yes');
        win.focus();
        return false
    });

    $('#renovacion').on('click', function() {

        if (id_contrato == '') {
            alert('Debe seleccionar un contrato');
            return false
        }

        var win = window.open(dire + '/contratos/opcontratos/renovacion/' + id_contrato + '/1', 'renovacion', 'height=500,width=1000,resizable=yes,scrollbars=yes');
        win.focus();
        return false
    });

    $('#bene').on('click', function() {

        if (id_contrato == '') {
            alert('Debe seleccionar un contrato');
            return false
        }

        if ($_estatus == 1) {
            alert('Operacion No disponible para este contrato');
            return false;
        }

        var win = window.open(dire + '/contratos/opcontratos/beneficiarios/' + id_contrato + '/1', 'beneficiarios', 'height=500,width=1000,resizable=yes,scrollbars=yes');
        win.focus();
        return false
    });

    $('#anular_contrato').on('click', function() {

        if (id_contrato == '') {
            alert('Seleccione un contrato');
            return false;
        }
        swal({
                title: "",
                text: "Esta Seguro que Desea Anular el Contrato?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-success",
                cancelButtonClass: "btn-danger",
                confirmButtonText: "Si",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    swal({
                        title: "",
                        text: "Motivo de Anulacion",
                        type: "input",
                        showCancelButton: true,
                        closeOnConfirm: false,
                        inputPlaceholder: ""
                    }, function(inputValue) {
                        if (inputValue === false) return false;
                        if (inputValue === "") {
                            swal.showInputError("Debe Introducir motivo de Anulacion");
                            return false
                        }
                        swal("", " Contrato Anulado", "success");
                        confirmacion(id_contrato, inputValue);


                    });
                } else {
                    swal("Cancelado", "", "error");
                }
            });

    });

});

function buscar(id) {
    limpiar();
    result = '';
    $.ajax({
        url: $url + 'buscar',
        data: {
            id: id
        },
        method: 'get',
        success: function(r) {
            aviso(r);

            $("#dni").append(r.datos_personales.dni);
            $("#nombres").append(r.datos_personales.nombres);

            if (r.datos_personales.tipo == 'V' || r.datos_personales.tipo == 'E') {
                $("#sexo").append(r.datos_personales.sexo);
                $("#fecha_nacimiento").append(r.datos_personales.fecha_nacimiento);
                $("#edad").append(r.datos_personales.edad);
                $("#profesion").append(r.datos_personales.profesion);
            }


            $("#tlf_movil").append(r.datos_personales.tlf_movil);
            $("#tlf_casa").append(r.datos_personales.tlf_casa);
            $("#correo").append(r.datos_personales.correo);

            $("#estado").append(r.datos_personales.estado);
            $("#ciudad").append(r.datos_personales.ciudad);
            $("#municipio").append(r.datos_personales.municipio);
            $("#parroquia").append(r.datos_personales.parroquia);
            $("#sector").append(r.datos_personales.sector);
            $("#direccion").append(r.datos_personales.direccion);

            $num = 1;
            for (var i in r.bancos) {


                $('#bancos').append('<tr><td> ' +
                    $num + '</td><td>' +
                    r.bancos[i].banco + '</td><td>' +
                    r.bancos[i].nombre + '</td><td>' +
                    r.bancos[i].cuenta + '</td></tr>');

                $num++;

            }

            console.log(r.contratos);
            result = id;
            tablacontratos(r);

        }
    });
}

function tablacontratos(r) {
    $('#registros').html('');
    var input = '';
    for (var i in r.contratos) {
        estatus = r.contratos[i].estatus_contrato;
        if (r.contratos[i].anulado == 0) {
            var color = 'success';
            var text = "Activo";
            var a = 0;

        } else {
            var color = 'danger';

            var text = "Anulado";
            var a = 1
            estatus = "Anulado";

        }
        if (r.contratos[i].estatus_contrato == 'Rechazado') {
            var color = 'danger';
            var text = "Rechazado";
            var a = 1
        }
        if (r.contratos[i].cobrando == 1) {
            var a = 1
        }


        input = '<input type="radio" name="optionsRadios" data-id="' + r.contratos[i].id + '" data-estatus="' + a + '" class ="opciones" id="optionsRadios1" value="' + r.contratos[i].id + '"/>'
        var total = 0;

        total = r.contratos[i].total_contrato - r.contratos[i].total_pagado;
        $('#registros').append('<tr class="' + color + ' contratos" data-id="' + r.contratos[i].id + '" title="' + text + '"><td>' + input + '</td><td>' +
            r.contratos[i].planilla + '</td><td>' +
            r.contratos[i].inicio + '</td><td>' +
            r.contratos[i].vencimiento + '</td><td>' +
            number_format(total, 2) + '</td><td>' +
            number_format(r.contratos[i].total_contrato, 2) + '</td><td>' +
            r.contratos[i].vendedor + '</td><td>' +
            r.contratos[i].sucursal + '</td><td>' +
            estatus + '</td> <td><a href="#"><i class="fa fa-info-circle fa-2x info" data-id= "' + r.contratos[i].id + '"aria-hidden="true" title="Informacion del contrato"></i></a></td>' +
            '<td><a href="#"><i class="fa fa-usd fa-2x pagos" style="color: green;"  data-id= "' + r.contratos[i].id + '"aria-hidden="true" title="Realizar pagos"></i></a></td>' +
            '</tr>');
    }
}

function limpiar() {

    $("#dni").html('');
    $("#nombres").html('');
    $("#sexo").html('');
    $("#fecha_nacimiento").html('');
    $("#edad").html('');
    $("#profesion").html('');
    $("#tlf_movil").html('');
    $("#tlf_casa").html('');
    $("#estado").html('');
    $("#ciudad").html('');
    $("#municipio").html('');
    $("#parroquia").html('');
    $("#sector").html('');
    $("#direccion").html('');
    $('#bancos').html('');
    $('#registros').html('');
    a = false;
    c = false;

}

function confirmacion($id_contrato, $porque) {
    $.ajax({
        url: dire + '/contratos/opcontratos/opanular',
        data: {
            id: $id_contrato,
            porque: $porque
        },
        type: 'GET',
        success: function(r) {

            buscar(result);

        }
    });
}