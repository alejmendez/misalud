@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
 
		@include('base::partials.ubicacion', ['ubicacion' => ['Consulta']])
		
@endsection

@section('content')
	<div class="row">
				
		<div class="form-group col-md-3">
			<label for="nombre">Consultar Cliente:</label>
			<div class="input-group">
					<input type="text" class="form-control" id="buscar" name="buscar" placeholder="Cedula">
					<span class="input-group-addon btn green" id="agregar_unidad">
							<i class="fa fa-search" style="color:#fff" title="Buscar"></i>
					</span>
			</div>
		</div>
    	<br><br><br><br>
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<center><h3 class="panel-title">Datos Personales</h3></center>
				</div>
				<div class="panel-body">
					<div class="profile-sidebar col-md-3" style="margin-bottom: 35px;">
						<div class="portlet light profile-sidebar-portlet ">
							<div class="mt-element-overlay">
								<div class="row">
									<div class="col-md-12">
										<div class="mt-overlay-6">
											<img  id="foto" src="{{ url('public/img/usuarios/user.png') }}" class="img-responsive" alt="">
											<div class="mt-overlay">
												<h2> </h2>
												<p>
													<input id="upload" name="foto" type="file" />
													<a href="#" id="upload_link" class="mt-info uppercase btn default btn-outline">
														<i class="fa fa-camera"></i>
													</a>
												</p>
											</div>
											<h4 style="color:#fff;font-weight:bold;">Imagen de perfil</h4>
										</div>
									</div>
								</div>
							</div>
							<br />
						</div>
					</div>  
					<div class="col-md-9">
						<div class="portlet light ">
							<div class="portlet-title tabbable-line">
								<div class="caption caption-md">
									<i class="icon-globe theme-font hide"></i>
									<span class="caption-subject font-blue-madison bold uppercase">Cliente</span>
								</div>
								<ul class="nav nav-tabs">
									<li class="active"><a href="#tab_1_1" data-toggle="tab">Persona</a></li>
									<li><a href="#tab_1_2" data-toggle="tab">Cuentas Bancarias</a></li>
								</ul>
							</div>
							<div class="portlet-body">
								<div class="tab-content">
									<div class="tab-pane active" id="tab_1_1"> 
										<div class="form-body">	
											<div class="row">
												<div class="col-md-12">
													<table class="table table-striped table-hover " border="0">
														<tbody >
															<tr>
																<td width="30%"><b>N° C.I:</b> <span id ="dni"></span></td>
																<td width="70%"><b>Nombres y Apellidos:</b> <span id="nombres"></span></td>
															</tr>
															<tr>
																<td><b>Sexo:</b> <span id ="sexo"></span></td>
																<td><b>Fecha de Nacimiento:</b> <span id ="fecha_nacimiento"></span></td>
															</tr>
															<tr >
																<td><b>Edad:</b> <span id ="edad"></span></td>
																<td><b>Profesion:</b> <span id ="profesion"></span> </td>
															</tr>
															<tr >
																<td><b>Telefono Movil:</b> <span id ="tlf_movil"></span> </td>
																<td><b>Correo:</b> <span id ="correo"></span> </td>
															</tr>
															<tr >
																<td><b>Telefono Casa:</b> <span id ="tlf_casa"></span> </td>
																<td></td>
															</tr>
															<tr >
																<td><b>Estado:</b> <span id ="estado"></span></td>
																<td><b>Ciudad:</b> <span id ="ciudad"></span></td>
															</tr>
															<tr >
																<td><b>Municipio:</b> <span id ="municipio"></span></td>
																<td><b>Parroquia:</b> <span id ="parroquia"></span></td>
															</tr> 
															<tr>
																<td><b>Sector:</b> <span id ="sector"></span></td>
																<td><b>Direccion:</b> <span id ="direccion"></span></td>
															</tr>
														</tbody>
													</table> 
												</div>
											</div>	
										</div>
									</div>
									<div class="tab-pane " id="tab_1_2">
										<table class="table table-striped table-hover ">
											<thead>
												<tr>
													<th>#</th>
													<th>Banco</th>
													<th>Tipo de Cuenta</th>
													<th>Cuenta</th>
												</tr>
											</thead>
											<tbody id="bancos">
												
											</tbody>
										</table> 
									</div> 
								</div>
							</div>
						</div>
					</div>
				</div>									
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title">Contratos</h3>
				</div>
				<div class="panel-body">
					<table class="table table-striped table-hover ">
						<thead>
						<tr>
							<th></th>
							<th>Planilla</th>
							<th>Fecha del Contrato</th>
							<th>Fecha de Vencimiento</th>
							<th>Saldo pendiente</th>
							<th>Total de contrato</th>
							<th>Vendedor</th>
							<th>Sucursal</th>
							<th>Estatus Contratos</th> 
							<th></th> 
						</tr>

						</thead>
						<tbody id="registros" style="cursor: pointer;">
						

						</tbody>
					</table> 
				</div>
			</div> 
		</div>
		{{-- <input type="hidden" name="ci" id="cedula" value="{{$cedula}}"> --}}
	</div>

	@if ($controller->permisologia('cobranza/escritorio/analista') || $controller->permisologia('ventas/escritorio/vendedor'))
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title"><i class="fa fa-cogs" aria-hidden="true"></i>Panel de Control </h3>
				</div>
				<div class="panel-body">
				<div class="btn-group btn-group-justified">

				  <a id="renovacion" class="btn btn-primary">Renovación del contrato</a>

				  <a id="bene" class="btn btn-info">Agregar o Eliminar Beneficiario del contrato</a>

				</div>
				

				</div>
			</div> 
		</div>
		{{-- <input type="hidden" name="ci" id="cedula" value="{{$cedula}}"> --}}
	</div>
	@endif
@endsection
@push('js')

<script type="text/javascript">
        var perfil_nombre = "{{ $Perfil }}";
</script>
@endpush