<?php

namespace Modules\Cobranza\Model;

use Modules\Base\Model\Modelo;



class ContratoTipo extends modelo
{
    protected $table = 'contrato_tipo';
    protected $fillable = ["nombre"];
    protected $campos = [
    'nombre' => [
        'type' => 'text',
        'label' => 'Nombre',
        'placeholder' => 'Nombre del Contrato Tipo'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    
}