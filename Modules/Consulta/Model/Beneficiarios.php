<?php
namespace Modules\Cobranza\Model;

use Modules\Base\Model\Modelo;

class Beneficiarios extends modelo
{
	protected $table = 'beneficiarios';
    protected $fillable = ["contratos_id","personas_id","parentesco_id","estatus"];
  
}