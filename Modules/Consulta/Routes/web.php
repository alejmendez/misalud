<?php


Route::group(['prefix' => 'consulta'], function() {
	Route::get('/', 	    		'ConsultaContratoController@index');

	Route::get('/buscar',   		'ConsultaContratoController@buscar');
	Route::get('consulta/{id}', 	'ConsultaContratoController@consulta');
	Route::get('cobrostable/{id}', 	'ConsultaContratoController@cobrostable');
});
