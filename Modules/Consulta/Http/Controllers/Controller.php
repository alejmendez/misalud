<?php namespace Modules\Consulta\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController {

	public $app = 'Consulta';
	
	protected $patch_js = [
		'public/js',
		'public/plugins',
		'Modules/Consulta/Assets/js',
	];

	protected $patch_css = [
		'public/css',
		'public/plugins',
		'Modules/Consulta/Assets/css',
	];
}