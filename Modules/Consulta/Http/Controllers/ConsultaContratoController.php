<?php

namespace Modules\Consulta\Http\Controllers;

//Controlador Padre
use Modules\Consulta\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;
use Carbon\Carbon;

//Modelos

use Modules\Base\Model\Personas;
use Modules\Base\Model\PersonasDetalles;
use Modules\Base\Model\PersonasDireccion;
use Modules\Base\Model\PersonasBancos;
use Modules\Base\Model\PersonasTelefono;
use Modules\Base\Model\PersonasCorreo;
use Modules\Base\Model\Bancos;
use Modules\Base\Model\BancoTipoCuenta;
use Modules\Base\Model\TipoTelefono;

use Modules\Base\Model\Estados;
use Modules\Base\Model\Municipio;
use Modules\Base\Model\Parroquia;
use Modules\Base\Model\Ciudades;
use Modules\Base\Model\Sector;

use Modules\Cobranza\Model\Contratos;
use Modules\Cobranza\Model\Beneficiarios;
use Modules\Cobranza\Model\Parentesco;
use Modules\Cobranza\Model\FrecuenciaPagos;
use Modules\Cobranza\Model\ContratosDetalles;
use Modules\Empresa\Model\Sucursal;
use Modules\Ventas\Model\VendedoresSucusal;
use Modules\Ventas\Model\Vendedores;
use Modules\Ventas\Model\Plan;
use Modules\Ventas\Model\PlanDetalles;
use Modules\Cobranza\Model\Cobros;

class ConsultaContratoController extends Controller
{
    protected $titulo = 'Consulta Cliente';

    public $js = [
        'consulta'
    ];
    
    public $css = [
        'consulta'

    ];

    public $librerias = [
        'datatables',
        'bootstrap-sweetalert'
    ];

    public function index()
    {
        $perfil = \Auth()->user()->perfil->nombre;
        return $this->view('consulta::Consulta',[
            'Perfil' => $perfil
        ]);
    } 

    public function validar(Request $request){

        return $this->validar2($request);
    }

    public function cobrostable(Request $request, $id = 0)
    {
       
        $sql = Cobros::select(
            'id',
            'total_cobrar',
            'created_at',
            'fecha_pagado',
            'total_pagado',
            'tipo_pago',
            'num_recibo',
            'concepto'
        )->where('contratos_id', $id);

      
       

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }

    protected function validar2($id){
        
        $consulta= Personas::select('dni')
            ->where('dni',$id->id)
            ->where('dni','not like', $id->id.'-%');


        if(count($consulta->get()) == 0){
                
            return array_merge([
                'consul' => count($consulta->get()),
                's' => 'n',
                'msj' => trans('controller.nobuscar'),
            ]);
        }

        return array_merge([
                'consul' => count($consulta->get()),
                's' => 's',
                'msj' => trans('controller.buscar'),
            ]);
    }
    
    public function contratoactual($id){

        $Contratos = Contratos::select(
            'contratos.id',
            'contratos.planilla',
            'contratos.titular',
            'contratos.vencimiento',
            'contratos.inicio',
            'contratos.cargado',
            'contratos.primer_cobro',
            'contratos.fecha_incial',
            'contratos.inicial',
            'contratos.renovaciones',
            'contratos.tipo_pago',
            'contratos.cuotas',
            'contratos.plan_detalles_id',
            'frecuencia_pagos.frecuencia',
            'frecuencia_pagos.dias',
            'contratos.total_pagado',
            'contratos.total_contrato',
            'contratos.anulado',
            'personas_bancos.cuenta',
            'bancos.nombre as banco',
            'estatus_contrato.nombre as estatus_contrato',
            'sucursal.nombre as sucursal',
            'personas.nombres as vendedor' 

        )
        ->leftJoin('vendedores', 'vendedores.id','=', 'contratos.vendedor_id')
        ->leftJoin('personas', 'personas.id','=','vendedores.personas_id')
        ->leftJoin('personas_bancos', 'personas_bancos.id','=', 'contratos.personas_bancos_id')
        ->leftJoin('bancos', 'bancos.id','=', 'personas_bancos.bancos_id')
        ->leftJoin('frecuencia_pagos', 'frecuencia_pagos.id','=', 'contratos.frecuencia_pagos_id')
        ->leftJoin('estatus_contrato','estatus_contrato.id', '=', 'contratos.estatus_contrato_id')      
        ->leftJoin('sucursal','sucursal.id', '=', 'contratos.sucursal_id')        
        ->where('contratos.id', $id)->first();

        $Contratos->vencimiento  =  Carbon::parse($Contratos->vencimiento)->format('d/m/Y');
        
        if($Contratos->anulado){
            $Contratos->estatus_contrato = 'Contrato Anulado';
        }


        return $Contratos;
    }

    

    public function buscar(Request $request){

        $validar = $this->validar2($request);

        if($validar['s'] == 'n'){

            return $validar;
        }
        $datos = [];

       $persona = Personas::select('*')->where('dni',$request->id)->where('dni','not like', $request->id.'-%')->first();
       $datos['id']      = $persona->id;
       $datos['tipo']    = $persona->tipo_persona->nombre;
       $datos['dni']     = $persona->tipo_persona->nombre.'-'.$persona->dni;
       $datos['nombres'] = $persona->nombres;

        if($persona->tipo_persona->nombre == 'V' or $persona->tipo_persona->nombre == 'E'){
            $persona_detalles = PersonasDetalles::where('personas_id', $persona->id)->first();
            
            $datos['sexo'] = '';
            $datos['profesion'] = '';
            $datos['edad'] = '';

            if(count($persona_detalles) == 1){
                $datos['fecha_nacimiento'] = $persona_detalles->fecha_nacimiento;
                    $sexo  = 'Femenino';
                if($persona_detalles->sexo == 'm'){
                    $sexo = 'Masculino';
                }
                $datos['sexo'] = $sexo;
                $datos['profesion'] = $persona_detalles->profesion->nombre;
                
                $fecha_n =Carbon::createFromFormat('d/m/Y', $persona_detalles->fecha_nacimiento);
                $date = Carbon::parse($fecha_n)->age;
                $datos['edad'] = $date;
            }

          
        }
        
        $persona_direccion = PersonasDireccion::where('personas_id', $persona->id)->first();

        $datos['estado']     = $persona_direccion->estado->nombre;
        $datos['municipio']  = $persona_direccion->municipio->nombre;
        $datos['ciudad']     = $persona_direccion->ciudad->nombre;
        $datos['sector']     = $persona_direccion->sector->nombre;
        $datos['parroquia'] = $persona_direccion->parroquia->nombre;
        $datos['direccion'] = $persona_direccion->direccion;
   
        $persona_telefono = PersonasTelefono::where('personas_id', $persona->id)
        ->where('tipo_telefono_id', 1)
        ->where('principal', 1)
        ->first();
   
        $datos['tlf_movil'] = '';
        if(count($persona_telefono) == 1){
            $datos['tlf_movil'] = $persona_telefono->numero;
        }

        $persona_telefono2 = PersonasTelefono::where('personas_id', $persona->id)
        ->where('tipo_telefono_id', 2)
        ->first();

        $datos['tlf_casa'] = '';
        if(count($persona_telefono2) == 1){
            $datos['tlf_casa'] = $persona_telefono2->numero;
        }

        $correo =  PersonasCorreo::where('personas_id', $persona->id)
        ->where('principal', 1)
        ->first();

        $datos['correo'] =  '';
        if(!is_null($correo)){
            $datos['correo'] = $correo->correo;

        }

        $bancos = PersonasBancos::select('banco_tipo_cuenta.nombre', 'personas_bancos.cuenta', 'bancos.nombre as banco')
        ->join('banco_tipo_cuenta','banco_tipo_cuenta.id', '=', 'personas_bancos.tipo_cuenta_id')
        ->join('bancos','bancos.id', '=', 'personas_bancos.bancos_id')
        ->where('personas_id', $persona->id)->get();

        if(count($bancos) == 0){
             $bancos = '';
        }
    
    
        $contratos = Contratos::select(
            'contratos.id',
            'contratos.planilla',
            'contratos.vencimiento',
            'contratos.inicio', 
            'contratos.vencimiento',
            'contratos.total_pagado',
            'contratos.total_contrato',
            'contratos.cobrando',
            'contratos.anulado',

            'estatus_contrato.nombre as estatus_contrato',
            'sucursal.nombre as sucursal',
            'personas.nombres as vendedor' 

        )
        ->join('vendedores', 'vendedores.id','=', 'contratos.vendedor_id')
        ->join('personas', 'personas.id','=','vendedores.personas_id')
        ->join('estatus_contrato','estatus_contrato.id', '=', 'contratos.estatus_contrato_id')      
        ->join('sucursal','sucursal.id', '=', 'contratos.sucursal_id')        
        ->where('titular', $persona->id)->get()->toArray();




        return array_merge([
            'datos_personales' => $datos,
            'bancos'           => $bancos, 
            'contratos'        => $contratos,    
            's'                => 's',
            'msj'              => 'Datos Encontrados',
        ]);
    }

    public function consulta(Request $request, $id = 0)
    {      

        $this->librerias = [
            'bootstrap-sweetalert',
            'datatables',
            'maskMoney'
        ];
       

        $this->css=[
            ''
        ];


        //informacion del contrato actual
        $Contratos = $this->contratoactual($id);
           
        $persona = Personas::select('*')->where('id',$Contratos->titular)->first();

    
        $beneficiario = 0;
        $plan = 'Personalizado';

        if($Contratos->plan_detalles_id  != ''){

            $plan_detalles = PlanDetalles::where('id',$Contratos->plan_detalles_id )->first();
            $plan          = Plan::where('id',$plan_detalles->plan_id )->first();
            $beneficiario  = $plan_detalles->beneficiarios;
            $plan          = $plan->nombre;
        }else{
           $beneficiario =  Beneficiarios::where('contratos_id', $id )->where('estatus',0)->count(); 
        }

        $_beneficiarios = Beneficiarios::select(
            'personas.dni',
            'tipo_persona.nombre',
            'personas.nombres',
            'parentesco.nombre as parentesco',
            'personas_detalles.sexo',
            'personas_detalles.fecha_nacimiento'
        )
        ->leftJoin('personas', 'personas.id','=','beneficiarios.personas_id')
        ->leftJoin('personas_detalles', 'personas_detalles.personas_id','=','personas.id')
        ->leftJoin('tipo_persona', 'tipo_persona.id','=','personas.tipo_persona_id')
        ->leftJoin('parentesco', 'parentesco.id','=','beneficiarios.parentesco_id')
        ->where('contratos_id', $id )->where('estatus',0)->get();
    
        $contratos_detalles = ContratosDetalles::select(
            'personas.dni',
            'tipo_persona.nombre',
            'personas.nombres',
            'contratos_detalles.operacion',
            'contratos_detalles.planilla',
            'contratos_detalles.created_at'

        )
        ->leftJoin('personas', 'personas.id','=','contratos_detalles.personas_id')
        ->leftJoin('tipo_persona', 'tipo_persona.id','=','personas.tipo_persona_id')
        ->orderBy( 'contratos_detalles.id', 'desc')
        ->where('contratos_id', $id )->get();


    
        $this->js=[
            'popup-consulta'
             
        ];

        return $this->view('consulta::Contratos-consulta', [
            'layouts'           => 'base::layouts.popup-consulta',
            'Contrato'          => $Contratos,
            'Persona'           => $persona,
            'plan'              => $plan,
            'beneficiario'      => $beneficiario,
            'info_beneficiarios'=> $_beneficiarios,
            'contratos_detalles'=> $contratos_detalles,
            'tipo'              => '',
            'solicitud_id'      => ''
        ]);
    }
 
    
}