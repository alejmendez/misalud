<?php namespace Modules\Configuracion\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Configuracion\Model\Configuracion;

class ConfiguracionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$configuraciones = [
			'logo'           => 'logo.png',
			'login_logo'     => 'login_logo.png',
			'nombre'         => 'Tomasini',
			'formato_fecha'  => 'd/m/Y',
			'miles'          => '.',
			'email'          => 'admin@tumundoclick.com.ve',
			'email_name'     => 'Tumundoclick',
			'nombre_empresa' => 'Vallee Tomasini y Asociados'
    	];

    	foreach ($configuraciones as $propiedad => $valor) {
	        Configuracion::create([
				'propiedad' => $propiedad,
				'valor' => $valor
			]);
    	}
    }
}
