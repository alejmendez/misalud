<?php

namespace Modules\Ventas\Http\Requests;

use App\Http\Requests\Request;

class PlanRequest extends Request {
    protected $reglasArr = [
		'nombre' => ['required', 'min:3', 'max:100'], 
		'hasta' => ['required'], 
		'desde' => ['required'], 
		//'giros' => ['required', 'integer'], 
		'empresa_id' => ['required', 'integer']
	];
}