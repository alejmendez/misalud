<?php

namespace Modules\Ventas\Http\Controllers;

use Modules\Ventas\Http\Controllers\Controller;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;

use Modules\Base\Model\TipoPersona;
use Modules\Base\Model\Personas;
use Modules\Base\Model\PersonasBancos;
use Modules\Base\Model\PersonasDetalles;

use Modules\Cobranza\Model\Contratos;
use Modules\Cobranza\Model\ContratosTemp;
use Modules\Cobranza\Model\Beneficiarios;
use Modules\Cobranza\Model\Parentesco;
use Modules\Cobranza\Model\FrecuenciaPagos;
use Modules\Cobranza\Model\ContratosDetalles;
use Modules\Empresa\Model\Sucursal;
use Modules\Ventas\Model\VendedoresSucusal;
use Modules\Ventas\Model\Vendedores;
use Modules\Ventas\Model\Plan;
use Modules\Ventas\Model\PlanDetalles;
use Auth;

class EscritoriovendedorController extends Controller {


	protected $titulo = 'Escritorio Vendedor';

	public $librerias = [
        'datatables'
    ];
	public $js = [
        'escritorio'
    ];
    
    public $css = [
        'escritorio'
    ];

	public function index() {

			return $this->view('ventas::Escritorio');
	
	}

	public function confirmacion(Request $request)
    {
    
        $sql = Contratos::select([
            'contratos.id', 
            'contratos.planilla', 
            'contratos.cargado', 
            'contratos.inicio', 
            'personas.nombres', 
            'sucursal.nombre'
           
        ])//->join('contratos_detalles', 'contratos_detalles.contratos_id', '=', 'contratos.id')
          ->join('personas', 'personas.id', '=', 'contratos.titular')
          ->join('sucursal', 'sucursal.id', '=', 'contratos.sucursal_id')
          ->where('contratos.estatus_contrato_id', 2);

        if(auth()->user()->super === 'n'){

         $vende = Vendedores::where('vendedores.personas_id', auth()->user()->id)
            ->first();
           
          $sql ->where('contratos.vendedor_id',  $vende->id);

         }



        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return  'apuntador';
            })
            ->make(true);
    }

    public function rechazadosrenovacion(Request $request)
    {
    
        $sql = ContratosTemp::select([
            'contratos_temp.id', 
            'contratos_temp.planilla', 
            'contratos_temp.inicio', 
            'personas.nombres', 
            'sucursal.nombre'
           
        ])->LeftJoin('contratos', 'contratos_temp.contrato_id', '=', 'contratos.id')
          ->LeftJoin('personas', 'personas.id', '=', 'contratos.titular')
          ->LeftJoin('sucursal', 'sucursal.id', '=', 'contratos.sucursal_id')
          ->where('contratos_temp.rechazado', 1);

        if(auth()->user()->super === 'n'){

         $vende = Vendedores::where('vendedores.personas_id', auth()->user()->id)
            ->first();
           
          $sql ->where('contratos.vendedor_id',  $vende->id);

         }

  
        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return  'apuntador';
            })
            ->make(true);
    }

    public function rechazados(Request $request){
    
        $sql = Contratos::select([
            'contratos.id', 
            'contratos.planilla', 
            'contratos.cargado', 
            'contratos.inicio', 
            'personas.nombres', 
            'sucursal.nombre'
           
        ])//->join('contratos_detalles', 'contratos_detalles.contratos_id', '=', 'contratos.id')
        ->join('personas', 'personas.id', '=', 'contratos.titular')
        ->join('sucursal', 'sucursal.id', '=', 'contratos.sucursal_id')
        ->where('contratos.estatus_contrato_id', 4);

        if(auth()->user()->super === 'n'){

         $vende = Vendedores::where('vendedores.personas_id', auth()->user()->id)
            ->first();
           
          $sql ->where('contratos.vendedor_id',  $vende->id);

         }

        if ($request->verSoloEliminados == 'true') {
            $sql2->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql2->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return  'apuntador';
            })
            ->make(true);
    }
    
}