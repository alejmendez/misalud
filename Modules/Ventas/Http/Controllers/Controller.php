<?php 

namespace Modules\Ventas\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController {
	public $app = 'ventas';
	
	protected $patch_js = [
		'public/js',
		'public/plugins',
		'Modules/Ventas/Assets/js',
	];

	protected $patch_css = [
		'public/css',
		'public/plugins',
		'Modules/Ventas/Assets/css',
	];
}