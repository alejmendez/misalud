<?php

namespace Modules\Ventas\Http\Controllers;

use Modules\Ventas\Http\Controllers\Controller;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;

class EscritoriogerenteventasController extends Controller {


	protected $titulo = 'Escritorio Gerente de Ventas';

	public $librerias = [
        'datatables'
    ];
	public $js = [
        'escritoriogerente'
    ];
    
    public $css = [
        'escritoriogerente'
    ];

	public function index() {
		return $this->view('ventas::EscritorioGerente');
	
	}
    
}