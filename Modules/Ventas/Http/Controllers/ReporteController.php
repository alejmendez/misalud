<?php

namespace Modules\Ventas\Http\Controllers;

use Modules\Ventas\Http\Controllers\Controller;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;

use Modules\Ventas\Model\Plan;
use Modules\Ventas\Model\PlanDetalles;
use Auth;
use PDF;

class ReporteController extends Controller {


	protected $titulo = 'Reporte Vendedor';

	public $librerias = [
        'datatables',
        'vue'
    ];
	public $js = [
        'reporte'
    ];
    
    public $css = [
    ];

	public function index() {

		return $this->view('ventas::Reporte');
	
	}

    public function imprimir(){

    $fecha = date('Y-m-d');
    $plan = Plan::with('detalle')
        ->where('plan.desde','<=',  $fecha)
        ->where('plan.hasta','>=',  $fecha);
      //dd($plan->get()->toArray());
    /*  $campos = ["id","meses_pagar"];
        foreach ($campos as $campo) {
            $filtro = $request->{$campo};
            if (empty($filtro) || (is_array($filtro) && $filtro[0] == '')) {    
                continue;
            }
            $plan->whereIn('plan.'.$campo, explode(',', $filtro[0]));
        }
        
        if ($request->has('html')) {
                $Plan = [];
                $i = 0;
                foreach ($plan->get() as $pla) {
                    $Plan[] = [
                        $pla->id,
                        $pla->meses_pagar
                    ];
                }
            return ['Plan' => $Plan];
        }*/
          
       $html = view('ventas::pdf.reporte', [
                'i'            => 0,
                'query'        => $plan->get()->toArray(),
                'total'        => 0
       ])->render();

        $pdf = PDF::loadHTML($html);

        return $pdf->download('reporte.pdf');
    }

    public function plan(){
        
        return Plan::pluck('nombre', 'id');
    }

    public function meses(){
        
        return Plan::pluck('meses_pagar','meses_pagar');
    }
}