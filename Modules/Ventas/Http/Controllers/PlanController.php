<?php

namespace Modules\Ventas\Http\Controllers;

//Controlador Padre
use Modules\Ventas\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use Modules\Ventas\Http\Requests\PlanRequest;

//Modelos
use Modules\Ventas\Model\Plan;
use Modules\Ventas\Model\PlanDetalles;

class PlanController extends Controller
{
    protected $titulo = 'Plan';

    public $js = [
        'Plan'
    ];
    
    public $css = [
        'Plan'
    ];

    public $librerias = [
        'datatables',
        'jquery-ui',
        'jquery-ui-timepicker',
        'maskMoney'
    ];
    

    public function index()
    {
        return $this->view('ventas::Plan', [
            'Plan' => new Plan()
        ]);
    }

    public function nuevo()
    {
        $Plan = new Plan();
        return $this->view('ventas::Plan', [
            'layouts' => 'base::layouts.popup',
            'Plan' => $Plan
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Plan = Plan::find($id);
        return $this->view('ventas::Plan', [
            'layouts' => 'base::layouts.popup',
            'Plan' => $Plan
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Plan = Plan::withTrashed()->with('detalle')->find($id);
        } else {
            $Plan = Plan::with('detalle')->find($id);
        }


        if ($Plan) {
            $Plan = $Plan->toArray();

            foreach ($Plan['detalle'] as $i => $detalle) {
                $Plan['detalle'][$i]['contado'] = number_format($Plan['detalle'][$i]['contado'], 2, ',', '.');
                $Plan['detalle'][$i]['inicial'] = number_format($Plan['detalle'][$i]['inicial'], 2, ',', '.');
                $Plan['detalle'][$i]['total']   = number_format($Plan['detalle'][$i]['total'], 2, ',', '.');
            }

            return array_merge($Plan, [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(PlanRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $Plan = $id == 0 ? new Plan() : Plan::find($id);

            $Plan->fill($request->all());
            $Plan->save();

            $num  = 1;
            foreach ($request->total as $key => $value) {
                $porsentaje = $request->porsertaje_descuento[$key] == '' ? 0 : $request->porsertaje_descuento[$key];
                $contado    = $request->contado[$key] == '' ? 0 : $request->contado[$key];
                $inicial    = $request->inicial[$key] == '' ? 0 : $request->inicial[$key];
                $total      = $request->total[$key];

                PlanDetalles::updateOrCreate(
                    [
                        'plan_id'       => $Plan->id,
                        'beneficiarios' => $num
                    ],
                    [
                        'porsertaje_descuento' =>  $porsentaje,
                        'contado'              =>  $this->formatoMoneda($contado),
                        'inicial'              =>  $this->formatoMoneda($inicial),
                        'total'                =>  $this->formatoMoneda($total)
                    ]
                );
                $num++;
            }
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $Plan->id,
            'texto' => $Plan->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Plan::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Plan::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Plan::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Plan::select([
            'id', 'nombre', 'desde', 'hasta', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }

    protected function formatoMoneda($monto){

        $monto = str_replace('.', '', $monto);
        $monto = str_replace(',', '.', $monto);
        $monto = floatval($monto);
        return $monto;
    }
}