<?php
namespace Modules\Ventas\Model;

use Modules\Base\Model\Modelo;

class Vendedores extends modelo
{
	protected $table = 'vendedores';
    protected $fillable = ["personas_id","estatus"];
    protected $campos = [];
 

    public function __construct(array $attributes = array())
	{
		parent::__construct($attributes);
		
	}
}