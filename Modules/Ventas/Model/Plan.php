<?php

namespace Modules\Ventas\Model;

use Modules\Base\Model\Modelo;

use Modules\Empresa\Model\Empresa;
use Carbon\Carbon;
class Plan extends modelo
{
    protected $table = 'plan';
    protected $fillable = ["nombre","desde","hasta","meses_pagar","empresa_id"];
    protected $campos = [
        'nombre' => [
            'type' => 'text',
            'label' => 'Nombre',
            'placeholder' => 'Nombre del Plan'
        ],
        'desde' => [
            'type' => 'text',
            'label' => 'Desde',
            'placeholder' => 'Desde del Plan'
        ],
        'hasta' => [
            'type' => 'text',
            'label' => 'Hasta',
            'placeholder' => 'Hasta del Plan'
        ],'meses_pagar' => [
            'type' => 'number',
            'label' => 'Meses Para pagar',
            'placeholder' => 'Meses Para pagar'
        ],
        'empresa_id' => [
            'type' => 'select',
            'label' => 'Empresa',
            'placeholder' => '- Seleccione un Empresa',
            'url' => 'empresa'
        ]
    ];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->campos['empresa_id']['options'] = Empresa::pluck('nombre', 'id');
    }

    public function empresa()
	{
		return $this->belongsTo('Modules\Empresa\Model\Empresa');
	}

	public function app_usuario_empresa()
	{
		return $this->hasMany('Modules\Base\Model\AppUsuarioEmpresa');
	}

	public function detalle()
    {
        return $this->hasMany('Modules\Ventas\Model\PlanDetalles');
    }

      //cargado
    public function setHastaAttribute($value)
    {
        // 2016-06-27
       
        $formato = 'd/m/Y';
        if (preg_match('/^\d{4}-\d{1,2}-\d{1,2}$/', $value)){
            $formato = 'Y-m-d';
        }
        
        $this->attributes['hasta'] = Carbon::createFromFormat($formato, $value);
    }
    
    public function getHastaAttribute($value){
        return Carbon::parse($value)->format('d/m/Y');
    }

    public function setDesdeAttribute($value)
    {
        // 2016-06-27
       
        $formato = 'd/m/Y';
        if (preg_match('/^\d{4}-\d{1,2}-\d{1,2}$/', $value)){
            $formato = 'Y-m-d';
        }
        
        $this->attributes['desde'] = Carbon::createFromFormat($formato, $value);
    }
    
    public function getDesteAttribute($value){
        return Carbon::parse($value)->format('d/m/Y');
    }
    
}