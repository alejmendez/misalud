<?php
namespace Modules\Ventas\Model;

use Modules\Base\Model\Modelo;

class PlanDetalles extends modelo
{
	protected $table = 'plan_detalles';
    protected $fillable = [
	    'plan_id',
	    'beneficiarios',
	    'porsertaje_descuento',
	    'contado',
	    'inicial',
	    'total'
    ];

    public function plan()
	{
		return $this->belongsTo('Modules\Ventas\Model\Plan');
	}
}