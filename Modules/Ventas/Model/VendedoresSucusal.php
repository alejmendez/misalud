<?php
namespace Modules\Ventas\Model;

use Illuminate\Database\Eloquent\Model;

class VendedoresSucusal extends Model
{
	protected $table = 'vendedores_sucusal';
    protected $fillable = ["vendedor_id","sucursal_id"];
    protected $campos = [];
    
    protected $primaryKey = null;
    public $incrementing = false;
    public $timestamps = false;

}