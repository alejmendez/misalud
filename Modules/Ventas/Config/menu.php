<?php

$menu['Venta'] = [
	[
		'nombre' 	=> 'Ventas',
		'direccion' => '#Ventas',
		'icono' 	=> 'fa fa-clipboard',
		'menu' 		=> [
			[
				'nombre' 	=> 'Inicio',
				'direccion' => 'ventas/escritorio/vendedor',
				'icono' 	=> 'fa fa-list-alt'
			],
			[
				'nombre' 	=> 'Escritorio Gerente',
				'direccion' => 'ventas/escritorio/gerente',
				'icono' 	=> 'fa fa-list-alt'
			],
			[
				'nombre' 	=> 'Planes',
				'direccion' => 'ventas/planes',
				'icono' 	=> 'fa fa-list-alt'
			],
			[
				'nombre' 	=> 'Reporte',
				'direccion' => 'ventas/reporte/imprimir',
				'icono' 	=> 'fa fa-file-pdf-o'
			]
		]
	]
];
