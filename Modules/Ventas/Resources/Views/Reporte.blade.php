@extends('base::layouts.default')
@section('content')
	@include('base::partials.ubicacion', ['ubicacion' => ['Reporte Planes']])
	
	<div id="botonera">
		<button id="buscar" class="btn green" title="{{ Lang::get('backend.btn_group.search.title') }}">
			<i class="fa fa-search"></i>
			<span class="visible-lg-inline visible-md-inline">{{ Lang::get('backend.btn_group.search.btn') }}</span>
		</button>
		<button id="imprimir" class="btn btn-info" title="{{ Lang::get('backend.btn_group.print.title') }}">
			<i class="fa fa-print"></i>
			<span class="visible-lg-inline visible-md-inline">{{ Lang::get('backend.btn_group.print.btn') }}</span>
	    </button>
    </div>
	<div class="row">
		<form id="formReporte" name="formReporte" method="get" action="{{url('ventas/reporte/pdf')}}" target="_blank">
			{{ csrf_field() }}
			<div>

			{{ Form::bsSelect('id', $controller->plan(), '',[
				'label' => 'Plan'
			]) }}

			{{ Form::bsSelect('meses_pagar', $controller->meses(), '',[
				'label' => 'Meses a Pagar'
			]) }}
			
			</div>

			
		</form>
	</div>
@endsection
