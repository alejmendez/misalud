@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Plan']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Plan.',
        'columnas' => [
            'Nombre'             => '50',
            'Desde'              => '25',
            'Hasta'              => '25'
        ]
    ])
@endsection

@push('css')
<style type="text/css">
#tabla-plan thead{
    background-color: white;
}

.tcentro{
    text-align: center;
    vertical-align: middle !important;
}

#tabla-plan input{
    width: 100%;
}
</style>
@endpush

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Plan->generate() !!}
            <div class="table-scrollable">
                <table id="tabla-plan" class="table table-striped table-hover table-bordered">
                    <thead>
                        <tr>
                            <th class="tcentro" rowspan="2">N</th>
                            <th class="tcentro" colspan="2">Contado</th>
                            <th class="tcentro" rowspan="2">Inicial Credito</th>
                       {{-- <th class="tcentro" rowspan="2">Cutotas Especiales (Bs)</th> --}}
                            <th class="tcentro" colspan="3">Total de Credito</th>
                        </tr>
                        <tr>
                            <th class="tcentro">(-) %</th>
                            <th class="tcentro">Bs</th>
                      {{--  <th class="tcentro">N. de Giros</th>
                            <th class="tcentro">Monto por Giros (Bs)</th> --}}
                            <th class="tcentro">Total (Bs)</th> 
                        </tr>
                    </thead>
                    <tbody>
                        @for($i = 0; $i <= 9; $i++)
                        <tr class="fila{{ $i + 1 }}">
                            <td class="tcentro">
                                {{ $i + 1 }}
                                <input name="plan_detalle_id[]" class="form-control id" type="hidden" value="0" />
                            </td>
                            <td>
                                <input name="porsertaje_descuento[]" placeholder="% de descuento" min="0" max="100" class="form-control porsertaje_descuento" type="number" />
                            </td>
                            <td><input name="contado[]" class="form-control contado" type="text" readonly="readonly" /></td>
                            <td><input name="inicial[]" class="form-control inicial" type="text" /></td>
                            {{--   <td><input name="precios_especial[]" class="form-control precios_especial" type="text" /></td>
                            <td>
                                <input name="giros[]" placeholder="Cantidad de Giros" min="0" max="52" class="form-control giros" type="number" />
                            </td>
                            <td><input name="precios_giros[]" class="form-control precios_giros" type="text" readonly="readonly" /></td> --}}
                            <td><input name="total[]" class="form-control total" type="text" /></td>
                        </tr>
                        @endfor
                    </tbody>
                </table>
            </div>
        {!! Form::close() !!}
    </div>
@endsection