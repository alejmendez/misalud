@extends('base::layouts.default')
@section('content')
	@include('base::partials.ubicacion', ['ubicacion' => ['Escritorio Vendedor']])
	<div class="row">
		<div class="col-md-3">	
			<div class="list-group">
				<a href="#" class="list-group-item active"><center>Opciones</center></a>
				<a href="{{ url('contratos/contrato') }}" class="list-group-item"><i class="fa fa-plus" aria-hidden="true"></i> Nuevo Contratos</a>
		
				<a href="{{ url('consulta/') }}" class="list-group-item "><i class="fa fa-search" aria-hidden="true"></i> Consultar Cliente</a>

				<a href="{{ url('ventas/reporte/imprimir') }}" class="list-group-item" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Planes Activos</a>
				
			</div>
		</div> 
		<div class="col-md-9">
			<div class="panel-group accordion" id="accordion3">
		        <div class="panel panel-primary">
		            <div class="panel-heading">
		                <h4 class="panel-title">
		                	<a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1"><center><h3 class="panel-title">Contratos En Espera De Confirmacion</h3></center> </a>
		                </h4>
		            </div>
		            <div id="collapse_3_1" class="panel-collapse in">
		                <div class="panel-body">
		            		<center><table id="tabla1" class="table table-striped table-hover table-bordered tables-text">
								<thead>
									<tr>
										<th style="width: 10%; text-align: center;">Planilla</th>
										<th style="width: 15%; text-align: center;">Fecha Cargado</th>
										<th style="width: 15%; text-align: center;">Fecha Contrato</th>
										<th style="width: 30%; text-align: center;">Titular</th>
										<th style="width: 15%; text-align: center;">Sucursal</th>
									</tr>
								</thead>
							</table></center>
		                </div>
		            </div>
		        </div>
		    </div>

			<div class="panel-group accordion" id="accordion3">
		        <div class="panel panel-danger">
		            <div class="panel-heading">
		                <h4 class="panel-title">
		                	<a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_2"><center><h3 class="panel-title">Contratos Rechazados</h3></center> </a>
		                </h4>
		            </div>
		            <div id="collapse_3_2" class="panel-collapse in">
		                <div class="panel-body">
		            		<center>
						<table id="tabla2" class="table table-striped table-hover table-bordered tables-text">
							<thead>
								<tr>
									<th style="width: 10%; text-align: center;">Planilla</th>
									<th style="width: 15%; text-align: center;">Fecha Cargado</th>
									<th style="width: 15%; text-align: center;">Fecha Contrato</th>
									<th style="width: 30%; text-align: center;">Titular</th>
									<th style="width: 15%; text-align: center;">Sucursal</th>
								</tr>
							</thead>
						</table>
					</center>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
	 




@endsection