<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlanDetalles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('plan_detalles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('plan_id')->unsigned();
            $table->integer('beneficiarios');
            
            $table->decimal('porsertaje_descuento', 15,2)->unsigned();
            $table->decimal('contado', 15,2)->unsigned();

            $table->decimal('inicial', 15,2)->unsigned();
            //$table->integer('precios_especial')->unsigned();
            //$table->integer('giros')->unsigned();
            //$table->integer('precios_giros')->unsigned();
            $table->decimal('total', 15,2)->unsigned();
            
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('plan_id')
                ->references('id')->on('plan')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plan_detalles');
    }
}
