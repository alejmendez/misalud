<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VendedoresSucusal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        schema::create('vendedores_sucusal', function (Blueprint $table) {
            //$table->increments('id');
            $table->integer('vendedor_id')->unsigned();
            $table->integer('sucursal_id')->unsigned();
        
            
            //$table->timestamps();
            //$table->softDeletes();
            
            $table->foreign('vendedor_id')
                  ->references('id')->on('vendedores')
                  ->onDelete('cascade')->onUpdate('cascade'); 
            
            $table->foreign('sucursal_id')
                  ->references('id')->on('sucursal')
                  ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendedores_sucusal');
    }
}
