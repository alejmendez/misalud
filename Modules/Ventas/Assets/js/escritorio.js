   var aplicacion, $form, tabla, $tablaPlan;
$(function() {
/*	aplicacion = new app('formulario', {
		'limpiar' : function(){
		}
	});*/
	confimacion();
	rechazados();
	rechazados_renovacion();
});


function confimacion() {

	solicitud_asignadas = $('#tabla1')
	.on("click", "tbody tr", function(){
		//procesarsolicitud(this.id);
		contrato(this.id, 2);
	})
	.dataTable({
		processing: true,
		serverSide: true,
		ajax: $url + "confirmacion",
		oLanguage: datatableEspanol,
		columns: [
			{ data: 'planilla', name: 'contratos.planilla' },
			{ data: 'cargado', name: 'contratos.cargado' },
			{ data: 'inicio', name: 'contratos.inicio' },
			{ data: 'nombres', name: 'personas.nombres' },
			{ data: 'nombre', name: 'sucursal.nombre' }
		]
	});
}
function rechazados() {

	solicitud_rechazadas = $('#tabla2')
	.on("click", "tbody tr", function(){
		//procesarsolicitud(this.id);
		contrato(this.id, 3);
	})
	.dataTable({
		processing: true,
		serverSide: true,
		ajax: $url + "rechazados",
		oLanguage: datatableEspanol,
		columns: [
			{ data: 'planilla', name: 'contratos.planilla' },
			{ data: 'cargado', name: 'contratos.cargado' },
			{ data: 'inicio', name: 'contratos.inicio' },
			{ data: 'nombres', name: 'personas.nombres' },
			{ data: 'nombre', name: 'sucursal.nombre' }
		]
	});
}
function rechazados_renovacion() {

	solicitud_rechazadas = $('#tabla3')
	.on("click", "tbody tr", function(){
		//procesarsolicitud(this.id);
		//contrato(this.id);
	})
	.dataTable({
		processing: true,
		serverSide: true,
		ajax: $url + "rechazadosrenovacion",
		oLanguage: datatableEspanol,
		columns: [
			{ data: 'planilla', name: 'contratos_temp.planilla' },
		
			{ data: 'inicio', name: 'contratos_temp.inicio' },
			{ data: 'nombres', name: 'personas.nombres' },
			{ data: 'nombre', name: 'sucursal.nombre' }
		]
	});
}

function contrato(id, tipo){
	var win = window.open(dire + '/contratos/contrato/consulta/' + id+'/'+tipo+'/0', 'consulta', 'height=500,width=1000,resizable=yes,scrollbars=yes');
	win.focus();
	return false
}

function rechazados_renovacion_popup(id, tipo){
	var win = window.open(dire + '/contratos/contrato/consulta/' + id+'/'+tipo+'/0', 'consulta', 'height=500,width=1000,resizable=yes,scrollbars=yes');
	win.focus();
	return false
}

