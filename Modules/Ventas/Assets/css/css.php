<style type="text/css">

.table-scrollable{
    width: 100%;
    overflow-y: hidden;
    border: 1px solid #333232;
    margin: 10px 0!important;
}

.form-control{
	
	width: 100%;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 2.42857143;
    color: #555;
    background-color: #fff;
    border: 1px solid #333232;
    margin-left: 5px;
    margin-top: 0px;
    font-family: "Arial", serif;
    font-size: 10pt !important;
}

.pagebreak{ 
	page-break-before: always; 
}

table { 
	page-break-inside:auto;
    font-family: "Arial", serif;
    font-size: 13pt;
}

tr{ 
	page-break-inside:avoid; page-break-after:auto;
}

td { 
	page-break-inside:avoid; page-break-after:auto;
}

#nombre {
    border-style: none;
    width: 200px; 
    margin-left:0px;
    margin-bottom:-3px;
}

#desde {
    border-style: none;
    width: 200px; 
    margin-bottom:-3px; 
}

#hasta {
    border-style: none;
    width: 200px; 
    margin-bottom:-3px;
}

#meses{
    border-style: none;
    width: 100px; 
    margin-bottom:-3px;
}

.innput{

    font-size: 12pt;

}
.labell{
    margin-bottom:50px; 
    font-size: 12pt;

}

body{
     font-family: "Arial", serif;

}

</style>