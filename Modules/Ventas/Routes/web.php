<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => Config::get('admin.prefix')], function() {
	Route::group(['prefix' => 'ventas'], function() {
		Route::group(['prefix' => 'planes'], function () {
		    Route::get('/', 				'PlanController@index');
			Route::get('nuevo', 			'PlanController@nuevo');
			Route::get('cambiar/{id}', 		'PlanController@cambiar');
			
			Route::get('buscar/{id}', 		'PlanController@buscar');

			Route::post('validar',			'PlanController@validar');
			Route::post('guardar',			'PlanController@guardar');
			Route::put('guardar/{id}', 		'PlanController@guardar');

			Route::delete('eliminar/{id}', 	'PlanController@eliminar');
			Route::post('restaurar/{id}', 	'PlanController@restaurar');
			Route::delete('destruir/{id}', 	'PlanController@destruir');

			Route::get('datatable', 		'PlanController@datatable');
		});

		Route::group(['prefix' => 'escritorio/vendedor'], function () {
		    Route::get('/', 					'EscritoriovendedorController@index');
		    Route::get('confirmacion',  		'EscritoriovendedorController@confirmacion');
		    Route::get('rechazados',    		'EscritoriovendedorController@rechazados');
		    Route::get('rechazadosrenovacion',	'EscritoriovendedorController@rechazadosrenovacion');
		});
		Route::group(['prefix' => 'escritorio/gerente'], function () {
		    Route::get('/', 'EscritoriogerenteventasController@index');
		});
		
		Route::group(['prefix' => 'reporte'], function () {
		    Route::get('/', 		  'ReporteController@index');
		    Route::get('pdf', 		  'ReporteController@imprimir');
			Route::get('imprimir',    'ReporteController@imprimir');
		});
	});
});