<?php

namespace Modules\Empresa\Database\Seeds;

use Illuminate\Database\Seeder;
use DB;
use Modules\Empresa\Model\Cargos;

class cargosseed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          $cargos = [ 	
			['Presidencia'],
			['Gerente de Ventas'],
			['Gerente Cobranza'],
			['Analista'],
			['Vendedor'],
			['Programador']
		];

		DB::beginTransaction();
		try{
			foreach ($cargos as $cargo) {
				Cargos::create([
					'nombre'  => $cargo[0]
				]);
			}
		}catch(Exception $e){
			DB::rollback();
			echo "Error ";
		}
		DB::commit();
	
    }
}
