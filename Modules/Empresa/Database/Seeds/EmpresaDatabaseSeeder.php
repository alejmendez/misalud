<?php namespace Modules\Empresa\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class EmpresaDatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call(EmpresaSeeder::class);
		//$this->call(cargosseed::class);
		$this->call(sucursales::class);

		Model::reguard();
	}
}
