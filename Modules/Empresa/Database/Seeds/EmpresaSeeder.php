<?php

namespace Modules\Empresa\Database\Seeds;

use DB;
use Illuminate\Database\Seeder;

use Modules\Empresa\Model\Empresa;

class EmpresaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();
        try{
            Empresa::create([
                'rif'         => 'J-12345678-0',
                'nombre'      => 'Empresa C.A.',
                'abreviatura' => 'E',
                'direccion'   => 'Ciudad Bolívar',
                'tlf'         => '0414-850-8123',
                'personas_id' => 1
            ]);
        }catch(Exception $e){
            DB::rollback();
            echo 'Error ';
        }
        DB::commit();
    }
}
