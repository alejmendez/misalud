<?php

namespace Modules\Empresa\Database\Seeds;

use Illuminate\Database\Seeder;
use DB;
use Modules\Empresa\Model\Sucursal;
class sucursales extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::beginTransaction();
        try{
            Sucursal::create([
	            "empresa_id" => 1,
	            "nombre" => 'Sucursal 1',
	            "abreviatura" => 'S-1',
	            "cod_sucursal"=> '1',
	            "correlativo" => 0,
	            "estados_id"  => 1,
	            "ciudades_id" => 1,
	            "municipios_id" => 1,
	            "parroquias_id" => 1,
	            
	            "direccion"=>'nose'
	        ]);
	        Sucursal::create([
	            "empresa_id" => 1,
	            "nombre" => 'Sucursal 2',
	            "abreviatura" => 'S-2',
	            "cod_sucursal"=> '2',
	            "correlativo" => 0,
	            "estados_id"  => 1,
	            "ciudades_id" => 1,
	            "municipios_id" => 1,
	            "parroquias_id" => 1,

	            "direccion"=>'nose'
	        ]);
        }catch(Exception $e){
            DB::rollback();
            echo 'Error ';
        }
        DB::commit();
    }
}
