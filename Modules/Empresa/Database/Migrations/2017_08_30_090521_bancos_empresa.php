<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BancosEmpresa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('bancos_empresa', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nombre',255); 
            $table->integer('empresa_id')->unsigned();
            $table->integer('bancos_id')->unsigned();
            //$table->integer('tipo_cuenta_id')->unsigned();
            
            $table->char('cuenta', 20)->unique();     

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('empresa_id')
                  ->references('id')->on('empresa')
                  ->onDelete('cascade')->onUpdate('cascade');
            
            $table->foreign('bancos_id')
                  ->references('id')->on('bancos')
                  ->onDelete('cascade')->onUpdate('cascade');
            
          /*   $table->foreign('tipo_cuenta_id')
                  ->references('id')->on('banco_tipo_cuenta')
                  ->onDelete('cascade')->onUpdate('cascade'); */

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bancos_empresa');
    }
}
