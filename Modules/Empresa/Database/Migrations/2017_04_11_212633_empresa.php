<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Empresa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresa', function (Blueprint $table) {
            $table->increments('id');

            $table->string('rif', 80)->unique();
            $table->string('nombre', 80)->unique();
            $table->string('abreviatura', 80)->unique();
            
            $table->string('direccion', 200);   
            $table->string('tlf', 200); 

            $table->integer('personas_id')->unsigned()->nullable();  

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('personas_id')
                ->references('id')->on('personas')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
