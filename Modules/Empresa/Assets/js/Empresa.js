var aplicacion, $form, tabla;
$(function() {
	aplicacion = new app('formulario', {
		'limpiar' : function(){
				tabla.fnDraw();
		}, 'buscar' : function(r){
			$('#personas_id').val(r.persona.dni);
			
			validar(r.persona.dni, 'dni','#dni');
		}
	});

	$form = aplicacion.form;

	tabla = datatable('#tabla', {
		ajax: $url + "datatable",
		columns: [{"data":"rif","name":"rif"},{"data":"nombre","name":"nombre"}]
	});
	
	$('#tabla').on("click", "tbody tr", function(){
		aplicacion.buscar(this.id);
	});

	$('#personas_id').blur(function(){
		if($(this).val() == ''){
			$('#nombres').val('');
			return false;
		}
		validar($(this).val(), 'dni','#dni');
	});
});


//valida DNI, RIF y CORREO
function validar($dato, $campo, $id){
	/*
	$dato = dato a validar
	$tipo = que campo validar
	
	*/
	$.ajax({
		url : $url + 'validar',
		type : 'POST',
		data:{ 
			'dato': $dato,
		},
		success : function(r){
			aviso(r);
			if(r.s == 'n'){
				$('#nombres').val('');
				return false
			}
			$('#nombres').val(r.persona[0].nombres).prop('disabled', true);
		}
	});
}