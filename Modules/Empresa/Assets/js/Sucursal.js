var aplicacion, $form, tabla;
$(function() {
	aplicacion = new app('formulario', {
		'limpiar' : function(){
			tabla.fnDraw();
		}
	});

	$form = aplicacion.form;

	tabla = datatable('#tabla', {
		ajax: $url + "datatable",
		columns: [{"data":"nombre","name":"nombre"},{"data":"abreviatura","name":"abreviatura"},{"data":"correlativo","name":"correlativo"}]
	});
	
	$('#tabla').on("click", "tbody tr", function(){
		aplicacion.buscar(this.id);
	});

	$('#estados_id').change(function(){
		aplicacion.selectCascada($(this).val(), 'ciudades_id','ciudades');
		aplicacion.selectCascada($(this).val(), 'municipio_id','municipios');
	});
	
	$('#municipio_id').change(function(){
		aplicacion.selectCascada($(this).val(), 'parroquia_id','parroquias');
	});

	$('#parroquia_id').change(function(){
		aplicacion.selectCascada($(this).val(), 'sector_id','sectores');
	});
});