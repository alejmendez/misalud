<?php

namespace Modules\Empresa\Http\Controllers;

//Controlador Padre
use Modules\Empresa\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use Modules\Empresa\Http\Requests\SucursalRequest;

//Modelos
use Modules\Empresa\Model\Sucursal;
use Modules\Base\Model\Estados;
use Modules\Base\Model\Municipio;
use Modules\Base\Model\Parroquia;
use Modules\Base\Model\Ciudades;
use Modules\Base\Model\Sector;

class SucursalController extends Controller
{
    protected $titulo = 'Sucursal';

    public $js = [
        'Sucursal'
    ];
    
    public $css = [
        'Sucursal'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('empresa::Sucursal', [
            'Sucursal' => new Sucursal()
        ]);
    }

    public function nuevo()
    {
        $Sucursal = new Sucursal();
        return $this->view('empresa::Sucursal', [
            'layouts' => 'base::layouts.popup',
            'Sucursal' => $Sucursal
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Sucursal = Sucursal::find($id);
        return $this->view('empresa::Sucursal', [
            'layouts' => 'base::layouts.popup',
            'Sucursal' => $Sucursal
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Sucursal = Sucursal::withTrashed()->find($id);
        } else {
            $Sucursal = Sucursal::find($id);
        }

        if ($Sucursal) {
            return array_merge($Sucursal->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(SucursalRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $Sucursal = $id == 0 ? new Sucursal() : Sucursal::find($id);

            $Sucursal->fill($request->all());
            $Sucursal->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $Sucursal->id,
            'texto' => $Sucursal->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Sucursal::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Sucursal::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Sucursal::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Sucursal::select([
            'id','nombre', 'abreviatura', 'correlativo',  'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
        public function ciudades(Request $request){
        $sql = Ciudades::where('estados_id', $request->id)
                    ->pluck('nombre','id')
                    ->toArray();

        $salida = ['s' => 'n' , 'msj'=> 'el estado no Contiene ciudades'];
        
        if($sql){
            $salida = ['s' => 's' , 'msj'=> 'Ciudades encontrados', 'ciudades_id'=> $sql];
        }               
        
        return $salida;
    } 
    
    public function municipios(Request $request){
        $sql = Municipio::where('estados_id', $request->id)
                    ->pluck('nombre','id')
                    ->toArray();

        $salida = ['s' => 'n' , 'msj'=> 'el estado no Contiene municipios'];
        
        if($sql){
            $salida = ['s' => 's' , 'msj'=> 'Municipios encontrados', 'municipio_id'=> $sql];
        }               
        
        return $salida;
    } 
    public function parroquias(Request $request){
        $sql = Parroquia::where('municipio_id', $request->id)
                    ->pluck('nombre','id')
                    ->toArray();

        $salida = ['s' => 'n' , 'msj'=> 'el municipio no Contiene parroquias'];
        
        if($sql){
            $salida = ['s' => 's' , 'msj'=> 'Paroquias encontrados', 'parroquia_id'=> $sql];
        }               
        
        return $salida;
    } 
    public function sectores(Request $request){
        $sql = Sector::where('parroquia_id', $request->id)
                    ->pluck('nombre','id')
                    ->toArray();

        $salida = ['s' => 'n' , 'msj'=> 'La parroquia no Contiene sectores'];
        
        if($sql){
            $salida = ['s' => 's' , 'msj'=> 'Sectores encontrados', 'sector_id'=> $sql];
        }               
        
        return $salida;
    }
}