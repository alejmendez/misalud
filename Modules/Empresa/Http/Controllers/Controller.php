<?php namespace Modules\Empresa\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController {
	public $app = 'admin';
	protected $patch_js = [
		'public/js',
		'public/plugins',
		'Modules/Empresa/Assets/js',
	];

	protected $patch_css = [
		'public/css',
		'public/plugins',
		'Modules/Empresa/Assets/css',
	];
}