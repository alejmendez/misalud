<?php

namespace Modules\Empresa\Http\Controllers;

//Controlador Padre
use Modules\Empresa\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use Modules\Empresa\Http\Requests\EmpresaRequest;

//Modelos
use Modules\Empresa\Model\Empresa;
use Modules\Base\Model\Personas;

class EmpresaController extends Controller
{
    protected $titulo = 'Empresa';

    public $js = [
        'Empresa'
    ];
    
    public $css = [
        'Empresa'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('empresa::Empresa', [
            'Empresa' => new Empresa()
        ]);
    }

    public function nuevo()
    {
        $Empresa = new Empresa();
        return $this->view('empresa::Empresa', [
            'layouts' => 'base::layouts.popup',
            'Empresa' => $Empresa
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Empresa = Empresa::find($id);
        return $this->view('empresa::Empresa', [
            'layouts' => 'base::layouts.popup',
            'Empresa' => $Empresa
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Empresa = Empresa::withTrashed()->find($id);
        } else {
            $Empresa = Empresa::find($id);
        }
        $persona = Personas::find($Empresa->personas_id);

        if ($Empresa) {
            return array_merge($Empresa->toArray(), [
                'persona' => $persona,
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(EmpresaRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
   

            $persona = Personas::where('dni',$request->personas_id)->get()->toArray();
            $datos = $request->all();
            $datos['personas_id'] = $persona[0]['id'];

            $Empresa = $id == 0 ? new Empresa() : Empresa::find($id);

            $Empresa->fill($datos);
            $Empresa->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $Empresa->id,
            'texto' => $Empresa->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Empresa::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Empresa::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Empresa::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Empresa::select([
            'id', 'rif', 'nombre'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }

    public function validar(Request $request){

        $persona = Personas::where('dni',$request->dato)->get();
        
        if($persona->count() == 0){

            return ['s' => 'n', 'msj' => 'El responsable no se encuentra registrado'];
        }   


        return [
            'persona' => $persona->toArray(),
            's' => 's', 
            'msj' =>  trans('controller.buscar')
        ];
    }
}