<?php

namespace Modules\Empresa\Http\Controllers;

//Controlador Padre
use Modules\Empresa\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use Modules\Empresa\Http\Requests\BancosEmpresaRequest;

//Modelos
use Modules\Empresa\Model\BancosEmpresa;

class BancosEmpresaController extends Controller
{
    protected $titulo = 'Bancos Empresa';

    public $js = [
        'BancosEmpresa'
    ];
    
    public $css = [
        'BancosEmpresa'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('empresa::BancosEmpresa', [
            'BancosEmpresa' => new BancosEmpresa()
        ]);
    }

    public function nuevo()
    {
        $BancosEmpresa = new BancosEmpresa();
        return $this->view('empresa::BancosEmpresa', [
            'layouts' => 'base::layouts.popup',
            'BancosEmpresa' => $BancosEmpresa
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $BancosEmpresa = BancosEmpresa::find($id);
        return $this->view('empresa::BancosEmpresa', [
            'layouts' => 'base::layouts.popup',
            'BancosEmpresa' => $BancosEmpresa
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $BancosEmpresa = BancosEmpresa::withTrashed()->find($id);
        } else {
            $BancosEmpresa = BancosEmpresa::find($id);
        }

        if ($BancosEmpresa) {
            return array_merge($BancosEmpresa->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(BancosEmpresaRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $BancosEmpresa = $id == 0 ? new BancosEmpresa() : BancosEmpresa::find($id);

            $BancosEmpresa->fill($request->all());
            $BancosEmpresa->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $BancosEmpresa->id,
            'texto' => $BancosEmpresa->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            BancosEmpresa::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            BancosEmpresa::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            BancosEmpresa::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = BancosEmpresa::select([
            'id', 'nombre'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}