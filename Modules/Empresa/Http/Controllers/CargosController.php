<?php

namespace Modules\Empresa\Http\Controllers;

//Controlador Padre
use Modules\Empresa\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use Modules\Empresa\Http\Requests\CargosRequest;

//Modelos
use Modules\Empresa\Model\Cargos;

class CargosController extends Controller
{
    protected $titulo = 'Cargos';

    public $js = [
        'Cargos'
    ];
    
    public $css = [
        'Cargos'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('empresa::Cargos', [
            'Cargos' => new Cargos()
        ]);
    }

    public function nuevo()
    {
        $Cargos = new Cargos();
        return $this->view('base::Cargos', [
            'layouts' => 'base::layouts.popup',
            'Cargos' => $Cargos
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Cargos = Cargos::find($id);
        return $this->view('base::Cargos', [
            'layouts' => 'base::layouts.popup',
            'Cargos' => $Cargos
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Cargos = Cargos::withTrashed()->find($id);
        } else {
            $Cargos = Cargos::find($id);
        }

        if ($Cargos) {
            return array_merge($Cargos->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(CargosRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $Cargos = $id == 0 ? new Cargos() : Cargos::find($id);

            $Cargos->fill($request->all());
            $Cargos->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $Cargos->id,
            'texto' => $Cargos->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Cargos::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Cargos::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Cargos::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Cargos::select([
            'id', 'nombre', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}