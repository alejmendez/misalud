<?php

namespace Modules\Empresa\Http\Requests;

use App\Http\Requests\Request;

class ProfesionRequest extends Request {
    protected $reglasArr = [
		'nombre' => ['required', 'min:3', 'max:100'], 
		'slug' => ['required', 'min:3', 'max:100', 'unique:profesion,slug']
	];
}