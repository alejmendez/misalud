<?php

namespace Modules\Empresa\Http\Requests;

use App\Http\Requests\Request;

class BancosEmpresaRequest extends Request {
    protected $reglasArr = [
		'nombre' => ['required', 'min:3', 'max:255'], 
		'empresa_id' => ['required', 'integer'], 
		'bancos_id' => ['integer'], 
		'cuenta' => ['required', 'min:3', 'max:20', 'unique:bancos_empresa,cuenta']
	];
}