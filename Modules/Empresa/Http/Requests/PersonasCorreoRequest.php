<?php

namespace Modules\Empresa\Http\Requests;

use App\Http\Requests\Request;

class PersonasCorreoRequest extends Request {
    protected $reglasArr = [
		'personas_id' => ['required', 'integer'], 
		'principal' => ['required'], 
		'correo' => ['required', 'min:3', 'max:200', 'unique:personas_correo,correo']
	];
}