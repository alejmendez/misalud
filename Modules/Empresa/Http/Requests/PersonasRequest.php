<?php

namespace Modules\Empresa\Http\Requests;

use App\Http\Requests\Request;

class PersonasRequest extends Request {
    protected $reglasArr = [
		'nacionalidad' => ['required', 'min:3', 'max:3', 'unique:personas,nacionalidad'], 
		'dni' => ['required', 'min:3', 'max:20'], 
		'nombres' => ['required', 'min:3', 'max:200']
	];
}