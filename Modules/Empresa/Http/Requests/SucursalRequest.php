<?php

namespace Modules\Empresa\Http\Requests;

use App\Http\Requests\Request;

class SucursalRequest extends Request {
    protected $reglasArr = [
		'empresa_id' => ['required', 'integer'], 
		'nombre' => ['required', 'min:3', 'max:80', 'unique:sucursal,nombre'], 
		'abreviatura' => ['required', 'min:3', 'max:80', 'unique:sucursal,abreviatura'], 
		'cod_sucursal' => ['required', 'integer'], 
		'correlativo' => ['required', 'integer'], 
		'estados_id' => ['required', 'integer'], 
		'ciudades_id' => ['required', 'integer'], 
		'municipios_id' => ['required', 'integer'], 
		'parroquias_id' => ['required', 'integer'], 
		'sectores_id' => ['required', 'integer'], 
		'direccion' => ['required', 'min:3', 'max:200']
	];
}