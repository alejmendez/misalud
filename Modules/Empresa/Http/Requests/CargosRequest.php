<?php

namespace Modules\Empresa\Http\Requests;

use App\Http\Requests\Request;

class CargosRequest extends Request {
    protected $reglasArr = [
		'nombre' => ['required', 'min:3', 'max:255']
	];
}