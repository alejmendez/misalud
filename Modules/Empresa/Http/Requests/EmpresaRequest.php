<?php

namespace Modules\Empresa\Http\Requests;

use App\Http\Requests\Request;

class EmpresaRequest extends Request {
    protected $reglasArr = [
		'rif' => ['required', 'min:3', 'max:80', 'unique:empresa,rif'], 
		'nombre' => ['required', 'min:3', 'max:80', 'unique:empresa,nombre'], 
		'abreviatura' => ['required', 'min:3', 'max:80', 'unique:empresa,abreviatura'], 
		'direccion' => ['required', 'min:3', 'max:200'], 
		'tlf' => ['required', 'min:3', 'max:200'], 
		'personas_id' => ['required', 'integer']
	];
}