<?php

namespace Modules\Empresa\Http\Requests;

use App\Http\Requests\Request;

class PersonasDireccionRequest extends Request {
    protected $reglasArr = [
		'personas_id' => ['required', 'integer', 'unique:personas_direccion,personas_id'], 
		'estados_id' => ['required', 'integer'], 
		'ciudade_id' => ['required', 'integer'], 
		'municipio_id' => ['required', 'integer'], 
		'parroquia_id' => ['required', 'integer'], 
		'sector_id' => ['required', 'integer'], 
		'direccion' => ['required', 'min:3', 'max:200']
	];
}