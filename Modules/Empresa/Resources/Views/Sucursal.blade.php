@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Sucursal']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Sucursal.',
        'columnas' => [
           
    		'Nombre' => '50',
    		'Abreviatura' => '25',
    		'correlativo' => '25'
    		
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Sucursal->generate(["empresa_id"]) !!}
            <div class="col-sm-12"></div>
            {!! $Sucursal->generate(["nombre","abreviatura","cod_sucursal","correlativo","estados_id","ciudades_id","municipios_id","parroquias_id","sectores_id","direccion"]) !!}
        {!! Form::close() !!}
    </div>
@endsection