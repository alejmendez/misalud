<?php

namespace Modules\Empresa\Model;

use Modules\Base\Model\Modelo;

use Modules\Base\Model\Estados;
use Modules\Base\Model\Municipio;
use Modules\Base\Model\Parroquia;
use Modules\Base\Model\Ciudades;
use Modules\Base\Model\Sector;

class Sucursal extends modelo
{
    protected $table = 'sucursal';
    protected $fillable = ["empresa_id","nombre","abreviatura","cod_sucursal","correlativo","estados_id","ciudades_id","municipios_id","parroquias_id","sectores_id","direccion"];
    protected $campos = [
        'empresa_id' => [
            'type'        => 'select',
            'label'       => 'Empresa',
            'placeholder' => '- Seleccione una Empresa',
            'url'         => 'empresa'
        ],
        'nombre' => [
            'type'        => 'text',
            'label'       => 'Nombre',
            'placeholder' => 'Nombre del Sucursal'
        ],
        'abreviatura' => [
            'type'        => 'text',
            'label'       => 'Abreviatura',
            'placeholder' => 'Abreviatura del Sucursal'
        ],
        'cod_sucursal' => [
            'type'        => 'number',
            'label'       => 'Cod Sucursal',
            'placeholder' => 'Cod Sucursal del Sucursal'
        ],
        'correlativo' => [
            'type'        => 'number',
            'label'       => 'Correlativo',
            'placeholder' => 'Correlativo del Sucursal'
        ],
        'estados_id' => [
            'type'        => 'select',
            'label'       => 'Estado',
            'placeholder' => '- Eeleccione un Estado',
            'url'         => 'estados'
        ],
        'ciudades_id' => [
            'type'        => 'select',
            'label'       => 'Ciudad',
            'placeholder' => '- Seleccione una Ciudad',
            'url'         => 'ciudades'
        ],
        'municipios_id' => [
            'type'        => 'select',
            'label'       => 'Municipios',
            'placeholder' => '- Seleccione un Municipio',
            'url'         => 'municipio'
        ],
        'parroquias_id' => [
            'type'        => 'select',
            'label'       => 'Parroquia',
            'placeholder' => '- Seleccione una Parroquia',
            'url'         => 'parroquia'
        ],
        'sectores_id' => [
            'type'        => 'select',
            'label'       => 'Sector',
            'placeholder' => '- Seleccione un Sector',
            'url'         => 'sector'
        ],
        'direccion' => [
            'type'        => 'textarea',
            'label'       => 'Direccion',
            'placeholder' => 'Direccion de la Sucursal',
            'cont_class'  => 'col-sm-12'
        ]
    ];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->campos['empresa_id']['options'] = Empresa::pluck('nombre', 'id');
        $this->campos['estados_id']['options'] = Estados::pluck('nombre', 'id');
        $this->campos['municipios_id']['options'] = Municipio::pluck('nombre', 'id');
        $this->campos['parroquias_id']['options'] = Parroquia::pluck('nombre', 'id');
        $this->campos['ciudades_id']['options'] = Ciudades::pluck('nombre', 'id');
        $this->campos['sectores_id']['options'] = Sector::pluck('nombre', 'id');
    }

     public function estado()
    {
        return $this->belongsTo('Modules\Base\Model\Estados', 'estados_id');
    }

    public function municipio()
    {
        return $this->belongsTo('Modules\Base\Model\Municipio', 'municipios_id');
    } 

    public function ciudad()
    {
        return $this->belongsTo('Modules\Base\Model\Ciudades', 'ciudades_id');
    }

    public function sector()
    {
        return $this->belongsTo('Modules\Base\Model\Sector', 'sectores_id');
    }

    public function parroquia()
    {
        return $this->belongsTo('Modules\Base\Model\Parroquia', 'parroquias_id');
    }
}