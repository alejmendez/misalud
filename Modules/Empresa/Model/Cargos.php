<?php

namespace Modules\Empresa\Model;

use Modules\Base\Model\Modelo;



class Cargos extends modelo
{
    protected $table = 'cargos';
    protected $fillable = ["nombre"];
    protected $campos = [
    'nombre' => [
        'type' => 'text',
        'label' => 'Nombre',
        'placeholder' => 'Nombre del Cargos'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    
}