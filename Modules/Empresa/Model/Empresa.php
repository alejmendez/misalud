<?php

namespace Modules\Empresa\Model;

use Modules\Base\Model\Modelo;

class Empresa extends modelo
{
    protected $table = 'empresa';
    protected $fillable = ["rif","nombre","abreviatura","direccion","tlf","personas_id"];
    protected $campos = [
        'rif' => [
            'type' => 'text',
            'label' => 'Rif',
            'placeholder' => 'Rif del Empresa'
        ],
        'nombre' => [
            'type' => 'text',
            'label' => 'Nombre',
            'placeholder' => 'Nombre del Empresa'
        ],
        'abreviatura' => [
            'type' => 'text',
            'label' => 'Abreviatura',
            'placeholder' => 'Abreviatura del Empresa'
        ], 
        'tlf' => [
            'type' => 'text',
            'label' => 'Tlf',
            'placeholder' => 'Tlf del Empresa'
        ],
        'personas_id' => [
            'type' => 'number',
            'label' => ' Dni del Responsable de la Empresa',
            'placeholder' => 'Dni del Responsable de la Empresa'
        ],
        'nombres' => [
            'type' => 'text',
            'label'       => 'Nombres y Apellidos del Responsable',
            'placeholder' => 'Nombres y Apellidos del Responsable',
            'required' => 'required',
            'disabled' => 'disabled',
            'cont_class'  => 'form-group col-md-6'
        ],
        'direccion' => [
            'type'        => 'textarea',
            'label'       => 'Direccion',
            'placeholder' => 'Direccion de la Empresa',
            'cont_class'  => 'col-sm-12'
        ]
    ];
}