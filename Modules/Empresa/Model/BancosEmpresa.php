<?php

namespace Modules\Empresa\Model;

use Modules\Base\Model\Modelo;
use Modules\Base\Model\Bancos;



class BancosEmpresa extends modelo
{
    protected $table = 'bancos_empresa';
    protected $fillable = ["nombre","empresa_id","bancos_id","cuenta"];
    protected $campos = [
    'nombre' => [
        'type' => 'text',
        'label' => 'Nombre',
        'placeholder' => 'Nombre del Bancos Empresa'
    ],
    'empresa_id' => [
        'type'        => 'select',
        'label'       => 'Empresa',
        'placeholder' => '- Seleccione un Empresa',
    ],
    'bancos_id' => [
        'type'        => 'select',
        'label'       => 'Bancos',
        'placeholder' => '- Seleccione un Bancos',
    ],
    'cuenta' => [
        'type' => 'text',
        'label' => 'Cuenta',
        'placeholder' => 'Cuenta del Bancos Empresa'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->campos['empresa_id']['options'] = Empresa::pluck('nombre', 'id');
        $this->campos['bancos_id']['options'] = Bancos::pluck('nombre', 'id');
    }

    
}