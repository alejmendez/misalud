<?php
$menu['empresa'] = [
	[
		'nombre' 	=> 'Empresa',
		'direccion' => '#Empresa',
		'icono' 	=> 'fa fa-building',
		'menu' 		=> [
			[
				'nombre' 	=> 'Empresa',
				'direccion' => 'empresa',
				'icono' 	=> 'fa fa-building'
			],
			[
				'nombre' 	=> 'Sucursales',
				'direccion' => 'empresa/sucursal',
				'icono' 	=> 'fa fa-building'
			],
			[
				'nombre' 	=> 'Bancos Empresa',
				'direccion' => 'empresa/bancos_empresa',
				'icono' 	=> 'fa fa-user-circle'
			]
		]
	]
];