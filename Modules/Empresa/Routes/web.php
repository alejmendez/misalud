<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::group(['prefix' => Config::get('admin.prefix')], function() {
	Route::group(['prefix' => 'empresa'], function () {   
		Route::get('/', 				'EmpresaController@index');
		Route::get('nuevo', 			'EmpresaController@nuevo');
		Route::get('cambiar/{id}', 		'EmpresaController@cambiar');
		
		Route::get('buscar/{id}', 		'EmpresaController@buscar');

		Route::post('validar',			'EmpresaController@validar');
		Route::post('guardar',			'EmpresaController@guardar');
		Route::put('guardar/{id}', 		'EmpresaController@guardar');

		Route::delete('eliminar/{id}', 	'EmpresaController@eliminar');
		Route::post('restaurar/{id}', 	'EmpresaController@restaurar');
		Route::delete('destruir/{id}', 	'EmpresaController@destruir');

		Route::get('datatable', 		'EmpresaController@datatable');

		Route::group(['prefix' => 'cargos'], function() {
			Route::get('/', 				'CargosController@index');
			Route::get('buscar/{id}', 		'CargosController@buscar');
			Route::get('nuevo', 		    'CargosController@nuevo');

			Route::get('cambiar/{id}', 		'CargosController@cambiar');
			Route::post('guardar',			'CargosController@guardar');
			Route::put('guardar/{id}', 		'CargosController@guardar');

			Route::delete('eliminar/{id}', 	'CargosController@eliminar');
			Route::post('restaurar/{id}', 	'CargosController@restaurar');
			Route::delete('destruir/{id}', 	'CargosController@destruir');

			Route::post('cambio', 			'CargosController@cambio');
			Route::get('datatable', 		'CargosController@datatable');
		});
		Route::group(['prefix' => 'sucursal'], function() {
			Route::get('/', 				'SucursalController@index');
			Route::get('nuevo', 			'SucursalController@nuevo');
			Route::get('cambiar/{id}', 		'SucursalController@cambiar');
			
			Route::get('buscar/{id}', 		'SucursalController@buscar');
			Route::get('ciudades/{id}',     'SucursalController@ciudades');
			Route::get('municipios/{id}',   'SucursalController@municipios');
			Route::get('parroquias/{id}',   'SucursalController@parroquias');
			Route::get('sectores/{id}',    	'SucursalController@sectores');
			Route::post('guardar',			'SucursalController@guardar');
			Route::put('guardar/{id}', 		'SucursalController@guardar');

			Route::delete('eliminar/{id}', 	'SucursalController@eliminar');
			Route::post('restaurar/{id}', 	'SucursalController@restaurar');
			Route::delete('destruir/{id}', 	'SucursalController@destruir');

			Route::get('datatable', 		'SucursalController@datatable');
		});


		Route::group(['prefix' => 'bancos_empresa'], function() {
			Route::get('/', 				'BancosEmpresaController@index');
			Route::get('nuevo', 			'BancosEmpresaController@nuevo');
			Route::get('cambiar/{id}', 		'BancosEmpresaController@cambiar');
			
			Route::get('buscar/{id}', 		'BancosEmpresaController@buscar');

			Route::post('guardar',			'BancosEmpresaController@guardar');
			Route::put('guardar/{id}', 		'BancosEmpresaController@guardar');

			Route::delete('eliminar/{id}', 	'BancosEmpresaController@eliminar');
			Route::post('restaurar/{id}', 	'BancosEmpresaController@restaurar');
			Route::delete('destruir/{id}', 	'BancosEmpresaController@destruir');

			Route::get('datatable', 		'BancosEmpresaController@datatable');
		});

		//{{route}}
	});
});