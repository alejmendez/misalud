<?php namespace Modules\Base\Database\Seeds;

use DB;
use Illuminate\Database\Seeder;
use Modules\Base\Model\Perfil;

class PerfilesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $perfiles = [
            'Desarrollador',
            'Administrador',
            'Analista',
            'Vendedor',
            'Precidencia',
            'Cordinadores',
            'consulta'
        ];

        DB::beginTransaction();
        try{
            foreach ($perfiles as $perfil) {
                Perfil::create([
                    'nombre' => $perfil
                ]);
            }
        }catch(Exception $e){
            DB::rollback();
            echo "Error ";
        }
        DB::commit();
    }
}
