<?php 

namespace Modules\Base\Database\Seeds;

use DB;
use Illuminate\Database\Seeder;

use Modules\Base\Model\TipoTelefono;

class TipoTelefonoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();
        try{
            TipoTelefono::create([
                'nombre'         => 'Movil'
            ]);
            TipoTelefono::create([
                'nombre'         => 'Casa'
            ]);
        }catch(Exception $e){
            DB::rollback();
            echo "Error ";
        }
        DB::commit();
    }
}
