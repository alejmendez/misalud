<?php

namespace Modules\Base\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Base\Model\BancoTipoCuenta;
use DB;
class bancotiposeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
    {
        DB::beginTransaction();
        try{
            BancoTipoCuenta::create([
                'nombre' => 'Ahorro'
            ]);
            BancoTipoCuenta::create([
                'nombre' => 'Corriente'
            ]);
        }catch(Exception $e){
            DB::rollback();
            echo "Error ";
        }
        DB::commit();
    }
}
