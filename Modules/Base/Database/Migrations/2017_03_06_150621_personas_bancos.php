<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PersonasBancos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas_bancos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('personas_id')->unsigned();
            $table->integer('bancos_id')->unsigned();
            $table->integer('tipo_cuenta_id')->unsigned();
            
            $table->char('digitos', 4)->nullable();
            $table->char('cuenta', 20)->unique();     

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('personas_id')
                  ->references('id')->on('personas')
                  ->onDelete('cascade')->onUpdate('cascade');
            
            $table->foreign('bancos_id')
                  ->references('id')->on('bancos')
                  ->onDelete('cascade')->onUpdate('cascade');
            
            $table->foreign('tipo_cuenta_id')
                  ->references('id')->on('banco_tipo_cuenta')
                  ->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personas_bancos');
    }
}
