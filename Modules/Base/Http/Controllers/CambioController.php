<?php namespace Modules\Base\Http\Controllers;

use Modules\Base\Http\Controllers\Controller;

use DB;
use Validator;

//Request
use App\Http\Requests\Request;

//Modelos
use Modules\Base\Model\Usuario;


class CambioController extends Controller {
	protected $titulo = 'Cambio de Contraseña';

	public $autenticar = false;
	public $css = [
		'cambio'
	];
	public $js = [
		'cambio'
	];

	public $librerias = [
        'alphanum',
        'maskedinput',
        'template'
    ];

	public function index(){
		return $this->view('base::Cambio');
	}
	
	public function clave(Request $request){
		$usuario = auth()->user();
		
		if ($usuario->super !== 's'){
			$validator = Validator::make($request->all(), [
				'password' => ['required', 'confirmed', 'password', 'min:8', 'max:50'],
			]);

			if ($validator->fails()) {
				return response($validator->errors(), 422);
			}
		}

		try {
			$usuario = usuario::find($usuario->id);
			$usuario->password = $request->password;
			$usuario->save();
		} catch (Exception $e) {
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}
   
} 