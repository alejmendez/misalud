<?php namespace Modules\Base\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController {

	public $app = 'admin';
	
	protected $patch_js = [
		'public/js',
		'public/plugins',
		'Modules/Base/Assets/js',
	];

	protected $patch_css = [
		'public/css',
		'public/plugins',
		'Modules/Base/Assets/css',
	];
}