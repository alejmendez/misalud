<?php

namespace Modules\Base\Http\Controllers;

//Controlador Padre
use Modules\Base\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use Modules\Base\Http\Requests\SectoresRequest;

//Modelos
use Modules\Base\Model\Sectores;
use Modules\Base\Model\Estados;
use Modules\Base\Model\Municipio;
use Modules\Base\Model\Parroquia;
use Modules\Base\Model\Ciudades;
use Modules\Base\Model\Sector;

class SectoresController extends Controller
{
    protected $titulo = 'Sectores';

    public $js = [
        'Sectores'
    ];
    
    public $css = [
        'Sectores'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('base::Sectores', [
            'Sectores' => new Sectores()
        ]);
    }

    public function nuevo()
    {
        $Sectores = new Sectores();
        return $this->view('base::Sectores', [
            'layouts' => 'base::layouts.popup',
            'Sectores' => $Sectores
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Sectores = Sectores::find($id);
        return $this->view('base::Sectores', [
            'layouts' => 'base::layouts.popup',
            'Sectores' => $Sectores
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Sectores = Sectores::withTrashed()->find($id);
        } else {
            $Sectores = Sectores::find($id);
        }

        if ($Sectores) {
            return array_merge($Sectores->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(SectoresRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $Sectores = $id == 0 ? new Sectores() : Sectores::find($id);

            $Sectores->fill($request->all());
            $Sectores->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $Sectores->id,
            'texto' => $Sectores->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Sectores::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Sectores::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Sectores::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Sectores::select([
            'id', 'nombre', 'slug', 'parroquias_id', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}