<?php

namespace Modules\Base\Http\Controllers\definiciones;

//Controlador Padre
use Modules\Base\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use Modules\Base\Http\Requests\SectorRequest;

//Modelos
use Modules\Base\Model\Sector;
use Modules\Base\Model\Sectores;
use Modules\Base\Model\Estados;
use Modules\Base\Model\Municipio;
use Modules\Base\Model\Parroquia;
use Modules\Base\Model\Ciudades;


class SectorController extends Controller
{
    protected $titulo = 'Sector';

    public $js = [
        'Sector'
    ];
    
    public $css = [
        'Sector'
    ];

    public $librerias = [
        'alphanum',
        'maskedinput',
        'datatables',
    ];

    public function index()
    {
        return $this->view('base::Sector', [
            'Sector' => new Sector()
        ]);
    }

    public function nuevo()
    {
        $Sector = new Sector();
        return $this->view('base::Sector', [
            'layouts' => 'base::layouts.popup',
            'Sector' => $Sector
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Sector = Sector::find($id);
        return $this->view('base::Sector', [
            'layouts' => 'base::layouts.popup',
            'Sector' => $Sector
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Sector = Sector::withTrashed()->find($id);
        } else {
            $Sector = Sector::find($id);
        }

        if ($Sector) {

            $municipio = Parroquia::select('municipios_id')
				->where('id', $Sector->parroquias_id)
				->first()
				->municipios_id;

            $estado = Municipio::select('estados_id')
				->where('id', $municipio)
				->first()
				->estados_id;
          
            $estados_id = Estados::pluck('nombre','id')
				->put('_', $estado);

            $municipios_id = Municipio::where('estados_id',  $estado)
				->pluck('nombre','id')
				->put('_', $municipio);

            $parroquias_id = Parroquia::where('municipios_id', $municipio)
				->pluck('nombre','id')
				->put('_', $Sector->parroquias_id);
                
            return array_merge($Sector->toArray(), [
                's' => 's',
                'estados_id'    => $estado,
                'municipios_id' => $municipios_id,
                'parroquias_id' => $parroquias_id,
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(SectorRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $Sector = $id == 0 ? new Sector() : Sector::find($id);
            $datos = $request->all();
            $datos['slug'] = str_slug($request->nombre, '-');
            $Sector->fill($datos);
            $Sector->save();
        } catch(QueryException $e) {
            DB::rollback();
            return $e->getMessage();
        } catch(Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }
        DB::commit();

        return [
            'id'    => $Sector->id,
            'texto' => $Sector->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Sector::destroy($id);
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Sector::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Sector::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Sector::select([
            'sectores.id', 'sectores.nombre', 'parroquias.nombre as parroquias', 'sectores.deleted_at'
        ])->join('parroquias', 'parroquias.id','=','sectores.parroquias_id');

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
     public function ciudades(Request $request){
        $sql = Ciudades::where('estados_id', $request->id)
                    ->pluck('nombre','id')
                    ->toArray();

        $salida = ['s' => 'n' , 'msj'=> 'el estado no Contiene ciudades'];
        
        if($sql){
            $salida = ['s' => 's' , 'msj'=> 'Ciudades encontrados', 'ciudades_id'=> $sql];
        }               
        
        return $salida;
    } 
    
    public function municipios(Request $request){
        $sql = Municipio::where('estados_id', $request->id)
                    ->pluck('nombre','id')
                    ->toArray();

        $salida = ['s' => 'n' , 'msj'=> 'el estado no Contiene municipios'];
        
        if($sql){
            $salida = ['s' => 's' , 'msj'=> 'Municipios encontrados', 'municipios_id'=> $sql];
        }               
        
        return $salida;
    } 
    public function parroquias(Request $request){
        $sql = Parroquia::where('municipios_id', $request->id)
                    ->pluck('nombre','id')
                    ->toArray();

        $salida = ['s' => 'n' , 'msj'=> 'el municipio no Contiene parroquias'];
        
        if($sql){
            $salida = ['s' => 's' , 'msj'=> 'Paroquias encontrados', 'parroquias_id'=> $sql];
        }               
        
        return $salida;
    } 
}