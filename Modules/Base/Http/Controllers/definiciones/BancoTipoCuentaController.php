<?php

namespace Modules\Base\Http\Controllers\definiciones;

//Controlador Padre
use Modules\Base\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use Modules\Base\Http\Requests\BancoTipoCuentaRequest;

//Modelos
use Modules\Base\Model\BancoTipoCuenta;

class BancoTipoCuentaController extends Controller
{
    protected $titulo = 'Banco Tipo Cuenta';

    public $js = [
        'BancoTipoCuenta'
    ];
    
    public $css = [
        'BancoTipoCuenta'
    ];

    public $librerias = [
        'alphanum',
        'maskedinput',
        'datatables',
    ];

    public function index()
    {
        return $this->view('base::BancoTipoCuenta', [
            'BancoTipoCuenta' => new BancoTipoCuenta()
        ]);
    }

    public function nuevo()
    {
        $BancoTipoCuenta = new BancoTipoCuenta();
        return $this->view('base::BancoTipoCuenta', [
            'layouts' => 'base::layouts.popup',
            'BancoTipoCuenta' => $BancoTipoCuenta
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $BancoTipoCuenta = BancoTipoCuenta::find($id);
        return $this->view('base::BancoTipoCuenta', [
            'layouts' => 'base::layouts.popup',
            'BancoTipoCuenta' => $BancoTipoCuenta
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $BancoTipoCuenta = BancoTipoCuenta::withTrashed()->find($id);
        } else {
            $BancoTipoCuenta = BancoTipoCuenta::find($id);
        }

        if ($BancoTipoCuenta) {
            return array_merge($BancoTipoCuenta->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(BancoTipoCuentaRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $BancoTipoCuenta = $id == 0 ? new BancoTipoCuenta() : BancoTipoCuenta::find($id);

            $BancoTipoCuenta->fill($request->all());
            $BancoTipoCuenta->save();
        } catch(QueryException $e) {
            DB::rollback();
            return $e->getMessage();
        } catch(Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }
        DB::commit();

        return [
            'id'    => $BancoTipoCuenta->id,
            'texto' => $BancoTipoCuenta->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            BancoTipoCuenta::destroy($id);
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            BancoTipoCuenta::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            BancoTipoCuenta::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = BancoTipoCuenta::select([
            'id', 'nombre', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}