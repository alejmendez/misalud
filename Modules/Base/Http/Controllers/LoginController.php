<?php

namespace Modules\Base\Http\Controllers;

//Dependencias
use Illuminate\Support\Facades\Auth;
use Session;

//Request
use Modules\Base\Http\Requests\LoginRequest;

//Controlador Padre
use Modules\Base\Http\Controllers\Controller;

//Modelos
use Modules\Base\Model\Historico;
use Modules\Empresa\Model\Empresa;
use Modules\Base\Model\AppUsuarioEmpresa;

class LoginController extends Controller {
	public $autenticar = false;

	protected $redirectTo = '/';   //antes era /escritorio
	protected $redirectPath = '/';   //antes era /escritorio
	protected $prefijo = '';

	public function __construct() {
		//$this->middleware('guest', ['except' => 'getSalir']);
		$this->prefijo = \Config::get('admin.prefix');

		$this->redirectTo = $this->prefijo . $this->redirectTo;
		$this->redirectPath = $this->prefijo . $this->redirectPath;

		if (Auth::check()) {
			return redirect($this->prefijo . '/');   //antes era /escritorio
		}
	}

	public function index() {
		if (Auth::check()) {
			return redirect($this->prefijo . '/');   //antes era /escritorio
		}

		$empresas = Empresa::all()->pluck('nombre', 'id');

		return $this->view('base::Login', ['empresas' => $empresas]);
	}

	public function bloquear() {
		Auth::logout();
		return $this->view('base::Bloquear');
	}

	public function salir() {
		Auth::logout();
		return redirect($this->prefijo . '/login');
	}

	public function validar(LoginRequest $request) {
		
		$data = $request->only('usuario', 'password');
		$data['usuario'] = strtolower($data['usuario']);
		$autenticado = Auth::attempt($data, $request->recordar());

		$idregistro = '';
		$login = $data['usuario'];

		if (!$autenticado) {
			$idregistro = 'Clave:' . $data['password'];
		}

		Historico::create([
			'tabla' => 'autenticacion',
			'concepto' => 'autenticacion',
			'idregistro' => $idregistro,
			'usuario' => $login,
		]);

		if ($autenticado) {
			if($request->empresa == '' && Auth::user()->super != 's'){
				Auth::logout();
				return ['s' => 'n', 'msj' => 'Debe Seleccionar una Empresa'];
			}
			if(Auth::user()->super == 'n'){ 
				$permisos = AppUsuarioEmpresa::where('usuario_id',Auth::user()->id)
				 			->where('empresa_id', $request->empresa)->get();
				if($permisos->count() == 0 ){
				 	Auth::logout();
				 	return ['s' => 'n', 'msj' => 'Error de acceso denegado'];
				}

				$permis = AppUsuarioEmpresa::select('empresa_id')->where('usuario_id',Auth::user()->id)->get();
				$datos = [];
				foreach ($permis as $permi) {
					$datos[]= $permi->empresa_id;
				}

				Session::put(['empresas_permisos' =>$datos]);	
				Session::put(['empresa' => $request->empresa]);
			}else{
				Session::put(['empresa' => 1]);
			}
			return ['s' => 's'];
		}

		return ['s' => 'n', 'msj' => 'La combinacion de Usuario y Clave no Concuerdan.'];
	}
}