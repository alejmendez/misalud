<?php

namespace Modules\Base\Http\Controllers;
use Session;
use Modules\Base\Http\Controllers\Controller;

class EscritorioController extends Controller {
	public $autenticar = false;

	protected $titulo = 'Escritorio';

	public function __construct() {
		parent::__construct();

		$this->middleware('Authenticate');
	}

	public function getIndex() {
		//dd(Session::get('empresa'));
		$permisos = [
			'ventas/planes',
		];
		$pase = 0;
		$ultimoPermiso = '';
		foreach ($permisos as $permiso) {
			
			if ($this->permisologia($permiso)) {

		
				$pase++;
				$ultimoPermiso = $permiso;
			}
		}
		
		if ($pase >= 1){
			return $this->view('base::Inicio');
		} else {
			return $this->view('base::Escritorio');
		}
		//return $this->view('base::Escritorio');
	}
}