@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
  
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Cambio']])
    
@endsection

@section('content')
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h3 class="panel-title">Cambio de Contraseña</h3>
                </div>
                <div class="panel-body">
                    <form id="formulario" name="formulario"  method="POST" autocomplete="off">
                        {{ Form::bsPassword('password', '', [
                            'label'         => 'Nueva Contrase&ntilde;a',
                            'placeholder'   => 'Nueva Contrase&ntilde;a',
                            'class_cont'    => 'col-sm-12'
                        ]) }}

                        {{ Form::bsPassword('password_confirmation', '', [
                            'label'         => 'Repita Nueva Contrase&ntilde;a',
                            'placeholder'   => 'Repita Nueva Contrase&ntilde;a',
                            'class_cont'    => 'col-sm-12'
                        ]) }}   
                        <center>
                            <div class="col-md-12 margiv-top-10">
                                <input type="submit"  class="btn green" id="guardar" value="Guardar">
                            </div>
                        </center>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4"></div>            
    </div>
@endsection