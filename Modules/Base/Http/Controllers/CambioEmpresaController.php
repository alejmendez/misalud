<?php

namespace Modules\Base\Http\Controllers;
use Session;
use Modules\Base\Http\Controllers\Controller;
use App\Http\Requests\Request;
use Auth;
class CambioEmpresaController extends Controller {
	public $autenticar = false;


	public function __construct() {
		parent::__construct();

		$this->middleware('Authenticate');
	}

	public function cambio(Request $request){
		$empresa = $request->id;
		if(Auth::user()->super != 's'){
			
		}

		Session::put(['empresa' => $empresa]);
		return ['s'=>'s'];
	}	
}