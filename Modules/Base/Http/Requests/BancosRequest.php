<?php

namespace Modules\Base\Http\Requests;

use App\Http\Requests\Request;

class BancosRequest extends Request {
    protected $reglasArr = [
		'nombre' => ['required', 'min:3', 'max:100']
	];
}