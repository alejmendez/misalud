<?php

namespace Modules\Base\Http\Requests;

use App\Http\Requests\Request;

class UsuariosRequest extends Request {
	protected $reglasArr = [
		'usuario'        => ['required', 'usuario', 'min:3', 'max:50', 'unique:app_usuario,usuario'],
		'password'       => ['required', 'password', 'min:8', 'max:50'],
		'dni'            => ['required', 'integer'],
		'nombre'         => ['nombre', 'min:3', 'max:50'],
		'apellido'       => ['nombre', 'min:3', 'max:100'],
		'correo'         => ['max:50', 'unique:personas_correo,correo'],
		
		'foto'           => ['mimes:jpeg,png,jpg'],
		'perfil_id'	     => ['required', 'integer'],
		//'autenticacion'  => ['required'],
		//'super'          => ['required'],
		
	];

	public function rules() {
		$rules = parent::rules();

		switch ($this->method()){
			case 'PUT':
			case 'PATCH':
				if ($this->input('password') == '') {
					unset($rules['password']);
				}

				break;
		}

		return $rules;
	}
}