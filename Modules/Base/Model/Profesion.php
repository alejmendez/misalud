<?php

namespace Modules\Base\Model;

use Modules\Base\Model\Modelo;

class Profesion extends modelo
{
    protected $table = 'profesion';
    protected $fillable = ["nombre","slug"];
    protected $campos = [
        'nombre' => [
            'type'        => 'text',
            'label'       => 'Nombre',
            'placeholder' => 'Nombre del Profesion'
        ]
    ];

    public function personadetalle()
    {
        return $this->hasOne('Modules\Base\Model\PersonasDetalles', 'profesion_id');
    }
}