<?php 

namespace Modules\Base\Model;
use Modules\Base\Model\Modelo;

class Perfil extends Modelo{
	protected $table = 'app_perfil';
	protected $fillable = ['nombre'];

	public function permisos()
	{
		return $this->hasMany('Modules\Base\Model\PerfilesPermisos', 'perfil_id');
	}

	public function usuarios()
	{
		return $this->hasMany('Modules\Base\Model\Usuario', 'perfil_id');
	}
}
