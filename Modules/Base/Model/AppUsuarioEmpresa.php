<?php
namespace Modules\Base\Model;

use Illuminate\Database\Eloquent\Model;

class AppUsuarioEmpresa extends Model
{
	protected $table = 'app_usuario_empresa';
    protected $fillable = ["empresa_id","usuario_id"];
    protected $campos = [];

    protected $primaryKey = null;
    public $incrementing = false;
    public $timestamps = false;

    protected $hidden = ['created_at', 'updated_at'];

    public function usuario()
    {
        return $this->belongsTo('Modules\Base\Model\Usurios', 'usuario_id');
    }


    public function __construct(array $attributes = array())
	{
		parent::__construct($attributes);
	}
}