<?php

namespace Modules\Base\Model;

use Modules\Base\Model\Modelo;

class TipoPersona extends modelo
{
    protected $table = 'tipo_persona';
    protected $fillable = ["nombre", "descripcion"];
    protected $campos = [
        'nombre' => [
            'type'        => 'text',
            'label'       => 'Nombre',
            'placeholder' => 'Nombre del Tipo Persona'
        ],
        'descripcion' => [
            'type'        => 'text',
            'label'       => 'Descripción',
            'placeholder' => 'Descripción del Tipo Persona'
        ]
    ];

    public function personas()
    {
        return $this->hasMany('Modules\Base\Model\Personas', 'tipo_persona_id');
    } 
}