<?php

namespace Modules\Base\Model;

use Modules\Base\Model\Modelo;

class Sector extends modelo
{
    protected $table = 'sectores';
    protected $fillable = ["nombre","slug","parroquias_id"];
    protected $campos = [
    'estados_id' => [
        'type'        => 'select',
        'label'       => 'Estado',
        'placeholder' => '- Eeleccione un Estado',
        'url'         => 'estados'
    ],
    'municipios_id' => [
        'type'        => 'select',
        'label'       => 'Municipios',
        'placeholder' => '- Seleccione un Municipio',
        'url'         => 'municipio'
    ],
    'parroquias_id' => [
        'type'        => 'select',
        'label'       => 'Parroquia',
        'placeholder' => '- Seleccione una Parroquia',
        'url'         => 'parroquia'
    ],
    'nombre' => [
        'type' => 'text',
        'label' => 'Nombre',
        'placeholder' => 'Nombre del Sectores'
    ],

];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
       $this->campos['estados_id']['options'] = Estados::pluck('nombre', 'id');
        
    }

    
}