<?php
namespace Modules\Base\Model;

use Illuminate\Database\Eloquent\Model;

class AppUsuarioEmpresaCargos extends Model
{
	protected $table = 'app_usuario_empresa_cargos';
    protected $fillable = ['cargo_id','usuario_id'];
    protected $campos = [];
	protected $primaryKey = null;
 	public $incrementing = false;
 	public $timestamps = false;
	
    public function __construct(array $attributes = array())
	{
		parent::__construct($attributes);
		
	}
}