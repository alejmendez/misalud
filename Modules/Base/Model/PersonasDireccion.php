<?php

namespace Modules\Base\Model;

use Modules\Base\Model\Modelo;
use Modules\Base\Model\Estados;
use Modules\Base\Model\Municipio;
use Modules\Base\Model\Parroquia;
use Modules\Base\Model\Ciudades;
use Modules\Base\Model\Sector;


class PersonasDireccion extends modelo
{
    protected $table = 'personas_direccion';
    protected $fillable = ["personas_id","estados_id","ciudades_id","municipios_id","parroquias_id","sectores_id","direccion"];
    protected $campos = [
        'estados_id' => [
            'type'        => 'select',
            'label'       => 'Estado',
            'placeholder' => '- Eeleccione un Estado',
            'url'         => 'estados'
        ],
        'ciudades_id' => [
            'type'        => 'select',
            'label'       => 'Ciudad',
            'placeholder' => '- Seleccione una Ciudad',
            'url'         => 'ciudades'
        ],
        'municipios_id' => [
            'type'        => 'select',
            'label'       => 'Municipios',
            'placeholder' => '- Seleccione un Municipio',
            'url'         => 'municipio'
        ],
        'parroquias_id' => [
            'type'        => 'select',
            'label'       => 'Parroquia',
            'placeholder' => '- Seleccione una Parroquia',
            'url'         => 'parroquia'
        ],
        'sectores_id' => [
            'type'        => 'select',
            'label'       => 'Sector',
            'placeholder' => '- Seleccione un Sector',
            'url'         => 'sector'
        ],
        'direccion' => [
            'type'        => 'textarea',
            'label'       => 'Direccion',
            'placeholder' => 'Direccion del Personas Direccion',
            'cont_class'  => 'col-sm-12'
        ]
    ];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->campos['estados_id']['options'] = Estados::pluck('nombre', 'id');
        $this->campos['municipios_id']['options'] = Municipio::pluck('nombre', 'id');
        $this->campos['parroquias_id']['options'] = Parroquia::pluck('nombre', 'id');
        $this->campos['ciudades_id']['options'] = Ciudades::pluck('nombre', 'id');
        $this->campos['sectores_id']['options'] = Sector::pluck('nombre', 'id');
    }

    public function persona()
    {
        return $this->belongsTo('Modules\Base\Model\Persona', 'personas_id');
    }

    public function estado()
    {
        return $this->belongsTo('Modules\Base\Model\Estados', 'estados_id');
    }

    public function municipio()
    {
        return $this->belongsTo('Modules\Base\Model\Municipio', 'municipios_id');
    } 

    public function ciudad()
    {
        return $this->belongsTo('Modules\Base\Model\Ciudades', 'ciudades_id');
    }

    public function sector()
    {
        return $this->belongsTo('Modules\Base\Model\Sector', 'sectores_id');
    }

    public function parroquia()
    {
        return $this->belongsTo('Modules\Base\Model\Parroquia', 'parroquias_id');
    }
}