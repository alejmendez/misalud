<?php

namespace Modules\Base\Model;

use Illuminate\Database\Eloquent\Model;

class PerfilesPermisos extends Model{
	protected $table = 'app_perfiles_permisos';
	protected $fillable = ['perfil_id', 'ruta'];

	protected $primaryKey = null;
    public $incrementing = false;

	protected $hidden = ['created_at', 'updated_at'];

	public function perfil()
	{
		return $this->belongsTo('Modules\Base\Model\Perfil', 'perfil_id');
	}
}
