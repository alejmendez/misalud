<?php
namespace Modules\Base\Model;

use Modules\Base\Model\Modelo;

class Bancos extends modelo
{
    protected $table = 'bancos';
    protected $fillable = ["nombre","codigo"];
    protected $campos = [
        'nombre' => [
            'type'        => 'text',
            'label'       => 'Nombre',
            'placeholder' => 'Nombre del Bancos'
        ], 
        'codigo' => [
            'type'        => 'text',
            'label'       => 'Codigo',
            'placeholder' => 'Bancos codigo'
        ]
    ];
}