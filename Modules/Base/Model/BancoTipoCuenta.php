<?php

namespace Modules\Base\Model;

use Modules\Base\Model\Modelo;

class BancoTipoCuenta extends modelo
{
    protected $table = 'banco_tipo_cuenta';
    protected $fillable = ["nombre"];
    protected $campos = [
        'nombre' => [
            'type' => 'text',
            'label' => 'Nombre',
            'placeholder' => 'Nombre del Banco Tipo Cuenta'
        ]
    ];
}