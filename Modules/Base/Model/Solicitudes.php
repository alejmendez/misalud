<?php
namespace Modules\Base\Model;

use Illuminate\Database\Eloquent\Model;

class Solicitudes extends Model
{
	protected $table = 'solicitudes';
    protected $fillable = ["tipo_solicitud","solicitante","aquien","indece","created_at"];
    protected $hidden = [ 'updated_at'];
    protected $campos = [
    'tipo_solicitud' => [
        'type' => 'number',
        'label' => 'Tipo Solicitud',
        'placeholder' => 'Tipo Solicitud del Solicitudes',
        'required' => true
    ],
    'solicitante' => [
        'type' => 'number',
        'label' => 'Solicitante',
        'placeholder' => 'Solicitante del Solicitudes',
        'required' => true
    ],
    'aquien' => [
        'type' => 'number',
        'label' => 'Aquien',
        'placeholder' => 'Aquien del Solicitudes',
        'required' => true
    ],
    'indece' => [
        'type' => 'number',
        'label' => 'Indece',
        'placeholder' => 'Indece del Solicitudes',
        'required' => true
    ]
];

    public function __construct(array $attributes = array())
	{
		parent::__construct($attributes);
		
	}
}