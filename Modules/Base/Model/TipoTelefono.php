<?php

namespace Modules\Base\Model;

use Modules\Base\Model\Modelo;

class TipoTelefono extends modelo
{
    protected $table = 'tipo_telefono';
    protected $fillable = ["nombre"];
    protected $campos = [
        'nombre' => [
            'type'        => 'text',
            'label'       => 'Nombre',
            'placeholder' => 'Nombre del Tipo Telefono'
        ]
    ];

    public function personatelefono()
    {
        return $this->hasMany('Modules\Base\Model\PersonasTelefono', 'tipo_telefono_id');
    }
}