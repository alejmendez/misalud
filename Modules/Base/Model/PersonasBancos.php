<?php

namespace Modules\Base\Model;

use Modules\Base\Model\Modelo;



class PersonasBancos extends modelo
{
    protected $table = 'personas_bancos';
    protected $fillable = ["personas_id","bancos_id","tipo_cuenta_id","cuenta",'digitos'];
    protected $campos = [
        'personas_id' => [
            'type'        => 'number',
            'label'       => 'Personas',
            'placeholder' => 'Personas del Personas Bancos'
        ],
        'bancos_id' => [
            'type'        => 'number',
            'label'       => 'Bancos',
            'placeholder' => 'Bancos del Personas Bancos'
        ],
        'tipo_cuenta_id' => [
            'type'        => 'number',
            'label'       => 'Tipo Cuenta',
            'placeholder' => 'Tipo Cuenta del Personas Bancos'
        ],
        'cuenta' => [
            'type'        => 'text',
            'label'       => 'Correo',
            'placeholder' => 'Correo del Personas Bancos'
        ],
        'digitos' => [
            'type'        => 'text',
            'label'       => 'digitos',
            'placeholder' => 'digitos del Personas Bancos'
        ]
    ];

    public function personas()
    {
        return $this->belongsTo('Modules\Base\Model\Personas', 'personas_id');
    }

    public function bancos()
    {
        return $this->belongsTo('Modules\Base\Model\Bancos', 'bancos_id');
    } 

    public function tipocuenta()
    {
        return $this->belongsTo('Modules\Base\Model\BancoTipoCuenta', 'tipo_cuenta_id');
    }
}