@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Sectores']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Sectores.',
        'columnas' => [
            'Nombre' => '33.333333333333',
		'Slug' => '33.333333333333',
		'Parroquias' => '33.333333333333'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Sectores->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection