@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Banco Tipo Cuenta']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar BancoTipoCuenta.',
        'columnas' => [
            'Nombre' => '100'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $BancoTipoCuenta->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection