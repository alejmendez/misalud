<?php
$controller = app('Modules\Base\Http\Controllers\Controller');
$controller->css[] = 'Escritorio.css';
$controller->js[] = 'Escritorio.js';

$data = $controller->_app();
extract($data);

$html['titulo'] = 'Inicio de Sesión';
?>
<!DOCTYPE html>
<!--[if IE 8]>    <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>    <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--><html lang="es"><!--<![endif]-->
	<head>
		@include('base::partials.head')
	</head>
	<body>
		@include('base::partials.page-header')
		<div class="container">
			<div class="row" style="padding-top: 20px;">
				@if ($controller->permisologia('ventas/escritorio/vendedor')) 
					<div class="col-md-4 text-center" style="padding-top: 20px;">
						<img class="img-responsive" style="height: 100px;" src="{{ asset('public/img/escritorio/ventas.png') }}" />
						<br />
						<span style="border-bottom: solid 2px #FF5900;font-size: 20px;">Ventas</span>
						<br /><br />
						<span>
							Creacion de Contratos, Clientes, renovación de Contratos, Inclusión y exclusión de beneficioarios 
						</span>
						<br />
						<br />
						<div class="btn-group" style="width: 100%;">
		                    <button type="button" class="btn dark" style="width: 50%; height: 50px;font-size: 10px;">Mi Salud Medicina Prepagada C.A.</button>
		                    <a class="btn red-thunderbird" style="width: 50%; height: 50px;font-size: 24px;" href="{{ url('ventas/escritorio/vendedor') }}" role="button">Ingresar</a>
		                </div>
						<!-- 
						<a class="btn btn-primary btn-lg" href="{{ url('ventas/escritorio/vendedor') }}" role="button">Inventario de Alimentos</a>
						-->
					</div>
				@endif 
				@if ($controller->permisologia('/consulta')) 
					<div class="col-md-4 text-center"  style="padding-top: 20px;">
						<img class="img-responsive" style="height: 100px;" src="{{ asset('public/img/escritorio/consulta.png') }}" />
						<br />
						<span style="border-bottom: solid 2px #FF5900;font-size: 20px;">Consulta</span>
						<br /><br />
						<span>
							Consulta de Cliente, Contrato, Beneficiarios y cobros  
						</span>
						<br />
						<br /><br />
						<div class="btn-group" style="width: 100%;">
							<button type="button" class="btn dark" style="width: 50%; height: 50px;font-size: 10px;">Mi Salud Medicina Prepagada C.A.</button>
							<a class="btn red-thunderbird" style="width: 50%; height: 50px;font-size: 24px;" href="{{ url('/consulta') }}" role="button">Ingresar</a>
						</div>
						<!-- 
						<a class="btn btn-primary btn-lg" href="{{ url('ventas/escritorio/vendedor') }}" role="button">Inventario de Alimentos</a>
						-->
					</div>
				@endif
				@if ($controller->permisologia('cobranza/escritorio/analista'))
					<div class="col-md-4 text-center"  style="padding-top: 20px;">
						<img class="img-responsive" style="height: 100px;" src="{{ asset('public/img/escritorio/icon_programa3.png') }}" />
						<br />
						<span style="border-bottom: solid 2px #FF5900;font-size: 20px;">Cobranza</span>
						<br /><br />
						<span>
							Consulta de Cliente, Contrato, Beneficiarios, cobros, domiciliaciones aceptacion de contratos    
						</span>
						<br />
						<br />
						<div class="btn-group" style="width: 100%;">
							<button type="button" class="btn dark" style="width: 50%; height: 50px;font-size: 10px;">Mi Salud Medicina Prepagada C.A.</button>
							<a class="btn red-thunderbird" style="width: 50%; height: 50px;font-size: 24px;" href="{{ url('cobranza/escritorio/analista') }}" role="button">Ingresar</a>
						</div>
						<!-- 
						<a class="btn btn-primary btn-lg" href="{{ url('ventas/escritorio/vendedor') }}" role="button">Inventario de Alimentos</a>
						-->
					</div>
				@endif 
				@if ($controller->permisologia('/facturacion'))
					<div class="col-md-4 text-center"  style="padding-top: 20px;">
						<img class="img-responsive" style="height: 100px;" src="{{ asset('public/img/escritorio/icon-itickets.png') }}" />
						<br />
						<span style="border-bottom: solid 2px #FF5900;font-size: 20px;">Facturacion</span>
						<br /><br />
						<span>
							Facturacion mensual, Libro de Venta    
						</span>
						<br />
						<br />
						<div class="btn-group" style="width: 100%;">
							<button type="button" class="btn dark" style="width: 50%; height: 50px;font-size: 10px;">Mi Salud Medicina Prepagada C.A.</button>
							<a class="btn red-thunderbird" style="width: 50%; height: 50px;font-size: 24px;" href="{{ url('facturacion') }}" role="button">Ingresar</a>
						</div>
						<!-- 
						<a class="btn btn-primary btn-lg" href="{{ url('ventas/escritorio/vendedor') }}" role="button">Inventario de Alimentos</a>
						-->
					</div>
				@endif 
			</div>
			
		</div>
		
		@include('base::partials.footer')

		<script type="text/javascript">
			$(".page-header-menu").remove();
			$(".dropdown-user").remove();
		</script>
	</body>
</html>