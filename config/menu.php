<?php

$menu = [];

$modules = ['app', 'modules.json'];
$modules = implode(DIRECTORY_SEPARATOR , $modules);
$modules = storage_path($modules);
$modules = file_get_contents($modules);
$modules = json_decode($modules);
$modules = collect($modules);

foreach ($modules->sortBy('order') as $module) {
	$file = base_path(implode(DIRECTORY_SEPARATOR , ['Modules', $module->name, 'Config', 'menu.php']));

	if (!is_file($file)) {
		continue;
	}
	
	$menu[$module->slug] = [];

	include($file);
}

$_menu = $menu;
$menu = [];
foreach ($_menu as $key => $value) {
	if (empty($value)) {
		continue;
	}

	$menu[] = $value;
}

//dd($menu);
return $menu;