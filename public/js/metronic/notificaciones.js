(function ($) {

	$( "#target" ).focus(function() {
 		 alert( "Handler for .focus() called." );
	});

	$( "#foo" ).one( "click", function() {
 		 alert( "This will be displayed only once." );
	});

}(jQuery));